@if ($paginator->hasPages())
    <ul class="pager">
        @if ($paginator->onFirstPage())
            <li class="disabled"><a class="disabled">←</a></li>
        @else
            <li><a href="{{ $paginator->previousPageUrl() }}" rel="prev">←</a></li>
        @endif
        @foreach ($elements as $element)
            @if (is_string($element))
                <li class="disabled"><span>{{ $element }}</span></li>
            @endif
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="active my-active">
                            <a class="current-page">@if($page > 9){{ $page }}@else{{ "0".$page }}@endif.</a></li>
                    @else
                        <li><a href="{{ $url }}">@if($page > 9){{ $page }}@else{{ "0".$page }}@endif.</a></li>
                    @endif
                @endforeach
            @endif
        @endforeach
        @if ($paginator->hasMorePages())
            <li><a href="{{ $paginator->nextPageUrl() }}" rel="next"><i class="fal fa-long-arrow-right"></i></a></li>
        @else
            <li class="disabled"><a class="disabled"><i class="fal fa-long-arrow-right"></i></a></li>
        @endif
    </ul>
@endif
