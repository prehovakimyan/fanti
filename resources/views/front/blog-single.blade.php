@extends('front.layouts.app')

@section('content')
    <!--content-->
    <div class="content">
        <!--fixed-column-wrap-->
        <div class="fixed-column-wrap">
            <!--fixed-column-wrap-content-->
            <div class="pr-bg"></div>
            <div class="fixed-column-wrap-content">
                <div class="scroll-nav-wrap color-bg">
                    <div class="carnival">{{ app('App\Http\Controllers\Front\DictionaryController')->getTranslate("scroll_down",$lang) }}</div>
                    <div class="snw-dec">
                        <div class="mousey">
                            <div class="scroller"></div>
                        </div>
                    </div>
                </div>
                <div class="bg" data-bg="/img/blog.jpg"></div>
                <div class="overlay"></div>
                <div class="progress-bar-wrap bot-element">
                    <div class="progress-bar"></div>
                </div>
                <!--fixed-column-wrap_title-->
                <div class="fixed-column-wrap_title first-tile_load">
                    <h2>{{ app('App\Http\Controllers\Front\DictionaryController')->getTranslate("our_last",$lang) }}<br> {{ app('App\Http\Controllers\Front\DictionaryController')->getTranslate("news",$lang) }}</h2>
                    <p>{{ app('App\Http\Controllers\Front\DictionaryController')->getTranslate("our_news_txt",$lang) }}</p>
                </div>
                <!--fixed-column-wrap_title end-->
                <div class="fixed-column-dec"></div>
            </div>
            <!--fixed-column-wrap-content end-->
        </div>
        <!--fixed-column-wrap end-->
        <!--column-wrap-->
        <div class="column-wrap">
            <!--column-wrap-container -->
            <div class="column-wrap-container fl-wrap">
                <div class="col-wc_dec">
                    <div class="pr-bg pr-bg-white"></div>
                </div>
                <div class="col-wc_dec col-wc_dec2">
                    <div class="pr-bg pr-bg-white"></div>
                </div>
                <section  class="small-padding article">
                    <div class="container">
                        <div class="split-sceen-content_title fl-wrap">
                            <div class="pr-bg pr-bg-white"></div>
                            <h3>{{ $blog->{"title_".$lang} }}</h3>
                            <div class="parallax-header"> <a href="#">{{ \Carbon\Carbon::parse($blog->created_at)->format('d M Y') }}</a><span>{{ app('App\Http\Controllers\Front\DictionaryController')->getTranslate("category",$lang) }}: </span><a>{{ app('App\Http\Controllers\Front\DictionaryController')->getTranslate("branding",$lang) }}</a></div>
                        </div>
                        <div class="column-wrap-content fl-wrap">
                            <div class="column-wrap-media fl-wrap">
                                <div class="pr-bg pr-bg-white"></div>
                                @if(count($blog->images) > 1)
                                    <div class="single-slider-wrap">
                                        <div class="single-slider fl-wrap">
                                            <div class="swiper-container">
                                                <div class="swiper-wrapper lightgallery">
                                                    @foreach($blog->images as $image)
                                                        <div class="swiper-slide hov_zoom"><img src="{{$image->image}}" alt=""><a href="{{$image->image}}" class="box-media-zoom   popup-image"><i class="fal fa-search"></i></a></div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                        <div class="ss-slider-controls">
                                            <div class="pr-bg pr-bg-white"></div>
                                            <div class="ss-slider-pagination"></div>
                                            <div class="ss-slider-cont ss-slider-cont-prev color-bg"><i class="fal fa-long-arrow-left"></i></div>
                                            <div class="ss-slider-cont ss-slider-cont-next color-bg"><i class="fal fa-long-arrow-right"></i></div>
                                        </div>
                                    </div>
                                @else
                                    @foreach($blog->images as $image)
                                        <img src="{{$image->image}}"  class="respimg" alt="">
                                    @endforeach
                                @endif
                            </div>
                            <div class="column-wrap-text">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="post-opt fl-wrap">
                                            <div class="pr-bg pr-bg-white"></div>
                                            <ul>
                                                <li><span><i class="fal fa-eye"></i> {{ $blog->views }} view</span></li>
                                            </ul>
                                        </div>
                                        <div class="pr-bg pr-bg-white"></div>
                                        {!! $blog->{"desc_".$lang} !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="section-number right_sn">
                            <div class="pr-bg pr-bg-white"></div>
                            <span>0</span>1.
                        </div>
                    </div>
                </section>
                <!--section end-->
            </div>
            <!--column-wrap-container end-->
        </div>
        <!--column-wrap end-->
        <div class="limit-box fl-wrap"></div>
    </div>
    <!--content end -->
@endsection
