@extends('front.layouts.app')
@section('content')
    <div class="content">
        <!--fixed-column-wrap-->
        <div class="fixed-column-wrap">
            <div class="pr-bg"></div>
            <!--fixed-column-wrap-content-->
            <div class="fixed-column-wrap-content">
                <div class="scroll-nav-wrap color-bg">
                    <div class="carnival">{{ app('App\Http\Controllers\Front\DictionaryController')->getTranslate("scroll_down",$lang) }}</div>
                    <div class="snw-dec">
                        <div class="mousey">
                            <div class="scroller"></div>
                        </div>
                    </div>
                </div>
                <div class="bg" data-bg="img/services.jpg"></div>
                <div class="overlay"></div>
                <div class="progress-bar-wrap bot-element">
                    <div class="progress-bar"></div>
                </div>
                <!--fixed-column-wrap_title-->
                <div class="fixed-column-wrap_title first-tile_load">
                    <h2>{{ app('App\Http\Controllers\Front\DictionaryController')->getTranslate("our",$lang) }}<br> {{ app('App\Http\Controllers\Front\DictionaryController')->getTranslate("services",$lang) }}</h2>
                    <p>{{ app('App\Http\Controllers\Front\DictionaryController')->getTranslate("our_services_txt",$lang) }}</p>
                </div>
                <!--fixed-column-wrap_title end-->
                <div class="fixed-column-dec"></div>
            </div>
            <!--fixed-column-wrap-content end-->
        </div>
        <!--fixed-column-wrap end-->
        <!--column-wrap-->
        <div class="column-wrap">
            <!--column-wrap-container -->
            <div class="column-wrap-container fl-wrap">
                <div class="col-wc_dec">
                    <div class="pr-bg pr-bg-white"></div>
                </div>
                <div class="col-wc_dec col-wc_dec2">
                    <div class="pr-bg pr-bg-white"></div>
                </div>

                @foreach($services as $key => $service)
                    <!--section-->
                        <section   class="small-padding">
                            <div class="container">
                                <div class="split-sceen-content_title fl-wrap">
                                    <div class="pr-bg pr-bg-white"></div>
                                    <h3>{{ $service->{"title_".$lang} }}</h3>
                                    <p>{{ $service->{"short_desc_".$lang} }}</p>
                                </div>
                                <div class="column-wrap-content fl-wrap">
                                    <div class="column-wrap-media fl-wrap">
                                        <div class="pr-bg pr-bg-white"></div>
                                        <img src="{{ $service->image  }}"  class="respimg" alt="">
                                        <div class="cont-det-wrap dark-bg">
                                            <div class="pr-bg pr-bg-white"></div>
                                            <ul>
                                                @if($service->{"person_1_".$lang} != '')
                                                    <li><strong>01.</strong><span>Cras lacinia: </span> <a>{{ $service->{"person_1_".$lang} }}</a></li>
                                                @endif
                                                @if($service->{"person_2_".$lang} != '')
                                                    <li><strong>02.</strong><span>molestie faucibus: </span> <a>{{ $service->{"person_2_".$lang} }}</a></li>
                                                @endif
                                                @if($service->{"person_3_".$lang} != '')
                                                    <li><strong>03.</strong><span>Donec auctor: </span> <a>{{ $service->{"person_3_".$lang} }}</a></li>
                                                @endif
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="serv-text fl-wrap">
                                        <div class="pr-bg pr-bg-white"></div>
                                        <div class="row">
                                            <div class="col-md-8">
                                                {!! $service->{"desc_".$lang} !!}
                                            </div>
                                            <div class="col-md-4">
                                                <div class="serv-price-wrap dark-bg"><span>Price</span>{{$service->price}}</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="section-number right_sn">
                                    <div class="pr-bg pr-bg-white"></div>
                                    <span>0</span>{{ $key + 1 }}.
                                </div>
                            </div>
                        </section>
                        <!--section end-->
                        <div class="section-separator"></div>
                @endforeach



                <div class="clearfix"></div>
                <div class="container">
                    <div class="order-wrap dark-bg fl-wrap">
                        <div class="pr-bg pr-bg-white"></div>
                        <h4>{{ app('App\Http\Controllers\Front\DictionaryController')->getTranslate("ready_to_order_project",$lang) }}</h4>
                        <a href="{{url('/')}}/contacts" class="ajax">{{ app('App\Http\Controllers\Front\DictionaryController')->getTranslate("get_in_touch",$lang) }} <i class="fal fa-envelope"></i></a>
                    </div>
                </div>
            </div>
            <!--column-wrap-container end-->
        </div>
        <!--column-wrap end-->
        <div class="limit-box fl-wrap"></div>
    </div>

    <!-- footer -->
    <div class="height-emulator fl-wrap"></div>
    <footer class="main-footer fixed-footer">
        <div class="pr-bg"></div>
        <div class="container">
            <div class="fl-wrap footer-inner">
                <div class="row">
                    <div class="col-md-4">
                        <div class="footer-logo">
                            <img src="images/logo.png" alt="">
                        </div>
                        <div class="footer_text  footer-box fl-wrap">
                            <p>Our team takes over everything, from an idea and concept development to realization. We believe in traditions and incorporate them within our innovations.Client is the soul of the project.</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="footer-header fl-wrap"><span>01.</span> Contacts</div>
                        <!-- footer-contacts-->
                        <div class="footer-contacts footer-box fl-wrap">
                            <ul>
                                <li><span>Call:</span><a href="#">+489756412322</a></li>
                                <li><span>Write  :</span><a href="#">yourmail@domain.com</a></li>
                                <li><span>Find us : </span><a href="#">USA 27TH Brooklyn NY</a></li>
                            </ul>
                        </div>
                        <!-- footer contacts end -->
                        <a href="contacts.html" class="ajax fc_button">Get In Touch <i class="fal fa-envelope"></i></a>
                    </div>
                    <div class="col-md-4">
                        <div class="footer-header fl-wrap"><span>02.</span> Subscribe</div>
                        <div class="footer-box fl-wrap">
                            <p>Want to be notified when we launch a new template or an udpate. Just sign up and we'll send you a notification by email.</p>
                            <div class="subcribe-form fl-wrap">
                                <form id="subscribe" class="fl-wrap">
                                    <input class="enteremail" name="email" id="subscribe-email" placeholder="Your Email" spellcheck="false" type="text">
                                    <button type="submit" id="subscribe-button" class="subscribe-button">  Send  <i class="fal fa-long-arrow-right"></i></button>
                                    <label for="subscribe-email" class="subscribe-message"></label>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="subfooter fl-wrap">
                <!-- policy-box-->
                <div class="policy-box">
                    <span>&#169; TheSide 2019  /  All rights reserved. </span>
                </div>
                <!-- policy-box end-->
                <a href="#" class="to-top-btn to-top">Back to top <i class="fal fa-long-arrow-up"></i></a>
            </div>
        </div>
        <div class="footer-canvas">
            <div class="dots gallery__dots" data-dots=""></div>
        </div>
    </footer>
    <!-- footer  end -->
@endsection
