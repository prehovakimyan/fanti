<!DOCTYPE HTML>
<html lang="en">
<head>
    <!--=============== basic  ===============-->
    <meta charset="UTF-8">
    <title>
        {{ app('App\Http\Controllers\Front\DictionaryController')->getTranslate("title_name",$lang) }}
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="robots" content="index, follow"/>
    <meta name="keywords" content=""/>
    <meta name="description" content=""/>

    <!--=============== css  ===============-->
    <link href="{{ asset('assets/front/css/reset.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/front/css/plugins.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/front/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/front/css/color.css') }}" rel="stylesheet">

    <!--=============== favicons ===============-->
    <link rel="shortcut icon" href="{{ asset('assets/front/images/favicon.ico') }}">
</head>
<body>
<!--Loader -->
<div class="loader2">
    <div class="loader loader_each"><span></span></div>
</div>
<!-- loader end  -->
<!-- main start  -->
<div id="main">
    <!-- header start  -->
    <header class="main-header">
        <a href="{{ url('/') }}" class="header-logo ajax">
            <img src="{{ asset('assets/front/images/logo_new.png') }}" alt="">
        </a>
        <!-- sidebar-button -->
        <!-- nav-button-wrap-->
        <div class="nav-button-wrap">
            <div class="nav-button">
                <span  class="nos"></span>
                <span class="ncs"></span>
                <span class="nbs"></span>
                <div class="menu-button-text">Menu</div>
            </div>
        </div>
        <!-- nav-button-wrap end-->
        <!-- sidebar-button end-->
        <!--  navigation -->
        <div class="header-contacts">
            <ul>
                <li><span> Call </span> <a href="tel:+4(897)56412322">+4(897)56412322</a></li>
                <li><span> Write </span> <a href="mailto:theside@domain.com">theside@domain.com</a></li>
            </ul>
        </div>
        <!-- navigation  end -->
    </header>
    <!-- header end -->
    <!-- left-panel -->
    <div class="left-panel">
        <div class="horizonral-subtitle"><span><strong></strong></span></div>
        <div class="left-panel_social">
            <ul >
                <li><a href="https://www.facebook.com/hrant.afrikyan.31" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                <li><a href="https://www.instagram.com/fanti.grand/" target="_blank"><i class="fab fa-instagram"></i></a></li>
            </ul>
        </div>
    </div>
    <!-- left-panel end -->
    <!-- share-button -->
    <div class="share-button showshare">
        <span>Share</span>
    </div>
    <!-- share-button end -->
    <!-- wrapper  -->
    <div id="wrapper">
        <!-- content-holder  -->
        <div class="content-holder" data-pagetitle="Our Services">
            <!-- nav-holder-->
            <div class="nav-holder but-hol">
                <div class="nav-scroll-bar-wrap fl-wrap">
                    <div class="nav-search">
                        <form action="#" class="searh-inner fl-wrap">
                            <input name="se" id="se" type="text" class="search fl-wrap" placeholder="Search.." value="Search..." />
                            <button class="search-submit color-bg" id="submit_btn"><i class="fal fa-search"></i> </button>
                        </form>
                    </div>
                    <!-- nav -->
                    <nav class="nav-inner" id="menu">
                        <ul>
                            <li>
                                <a href="#">Home</a>
                                <!--level 2 -->
                                <ul>
                                    <li><a href="index.html" class="ajax">Half Slider</a></li>
                                    <li><a href="index2.html" class="ajax">Multi Slideshow</a></li>
                                    <li><a href="index8.html" class="ajax">Carousel</a></li>
                                    <li><a href="index3.html" class="ajax">Impulse Image</a></li>
                                    <li><a href="index4.html" class="ajax">Fullscreen  Image</a></li>
                                    <li><a href="index5.html" class="ajax">Fullscreen  Slider</a></li>
                                    <li><a href="index6.html" class="ajax">Fullscreen  Slideshow</a></li>
                                    <li><a href="index7.html" class="ajax">Video</a></li>
                                </ul>
                                <!--level 2 end -->
                            </li>
                            <li><a href="{{ url('about') }}" class="ajax">About</a></li>
                            <li><a href="{{ url('portfolio') }}" class="ajax">Portfolio</a></li>
                            <li><a href="{{ url('/') }}/services" class="ajax">Services</a></li>
                            <li><a href="{{ url('/') }}/contacts" class="ajax">Contacts</a></li>
                            <li><a href="{{ url('/') }}/blog" class="ajax">Blog</a></li>
                        </ul>
                    </nav>
                    <!-- nav end-->
                    <!--lang-links-->
                    <div class="lang-links fl-wrap">
                        <a href="#" class="act-leng">EN</a><a href="#">FR</a>
                    </div>
                    <!--lang-links end-->
                </div>
                <!--nav-social-->
                <div class="nav-social">
                    <span class="nav-social_title">Follow us : </span>
                    <ul >
                        <li><a href="https://www.facebook.com/hrant.afrikyan.31" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="https://www.instagram.com/fanti.grand/" target="_blank"><i class="fab fa-instagram"></i></a></li>
                    </ul>
                </div>
                <!--nav-social end -->
            </div>
            <div class="nav-overlay">
                <div class="element"></div>
            </div>
            <!-- nav-holder end -->
            <!--content-->
            @yield('content')
            <!--content end -->
            <!-- footer -->
            <div class="height-emulator fl-wrap"></div>
            <footer class="main-footer fixed-footer">
                <div class="pr-bg"></div>
                <div class="container">
                    <div class="fl-wrap footer-inner">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="footer-logo">
                                    <img src="{{ asset('assets/front/images/logo_new.png') }}" alt="">
                                </div>
                                <div class="footer_text  footer-box fl-wrap">
                                    <p>
                                        {{ app('App\Http\Controllers\Front\DictionaryController')->getTranslate("footer_short_desc",$lang) }}
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="footer-header fl-wrap">
                                    <span>01.</span>
                                    {{ app('App\Http\Controllers\Front\DictionaryController')->getTranslate("contacts",$lang) }}
                                </div>
                                <!-- footer-contacts-->
                                <div class="footer-contacts footer-box fl-wrap">
                                    <ul>
                                        <li>
                                            <span>
                                                {{ app('App\Http\Controllers\Front\DictionaryController')->getTranslate("call",$lang) }}
                                            </span>
                                            <a href="#">
                                                {{ app('App\Http\Controllers\Front\DictionaryController')->getTranslate("phone",$lang) }}
                                            </a>
                                        </li>
                                        <li>
                                            <span>
                                                {{ app('App\Http\Controllers\Front\DictionaryController')->getTranslate("email_us",$lang) }}
                                            </span>
                                            <a href="#">
                                                {{ app('App\Http\Controllers\Front\DictionaryController')->getTranslate("email",$lang) }}
                                            </a>
                                        </li>
                                        <li>
                                            <span>
                                                {{ app('App\Http\Controllers\Front\DictionaryController')->getTranslate("find_us",$lang) }}
                                            </span>
                                            <a href="#">
                                                {{ app('App\Http\Controllers\Front\DictionaryController')->getTranslate("address",$lang) }}
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <!-- footer contacts end -->
                            </div>
                            <div class="col-md-4">
                                <div class="footer-header fl-wrap">
                                    <span>02.</span>
                                    {{ app('App\Http\Controllers\Front\DictionaryController')->getTranslate("subscribe",$lang) }}
                                </div>
                                <div class="footer-box fl-wrap">
                                    <p>
                                        {{ app('App\Http\Controllers\Front\DictionaryController')->getTranslate("subscription_desc",$lang) }}
                                    </p>
                                    <div class="subcribe-form fl-wrap">
                                        <form id="subscribe" class="fl-wrap">
                                            <input class="enteremail" name="email" id="subscribe-email" placeholder="{{ app('App\Http\Controllers\Front\DictionaryController')->getTranslate("your_email",$lang) }}" spellcheck="false" type="text">
                                            <button type="submit" id="subscribe-button" class="subscribe-button">
                                                {{ app('App\Http\Controllers\Front\DictionaryController')->getTranslate("send",$lang) }}
                                                <i class="fal fa-long-arrow-right"></i>
                                            </button>
                                            <label for="subscribe-email" class="subscribe-message"></label>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="subfooter fl-wrap">
                        <!-- policy-box-->
                        <div class="policy-box">
                            <span>&#169;
                                {{ app('App\Http\Controllers\Front\DictionaryController')->getTranslate("copyright",$lang) }}
                            </span>
                        </div>
                        <!-- policy-box end-->
                        <a href="#" class="to-top-btn to-top">
                            {{ app('App\Http\Controllers\Front\DictionaryController')->getTranslate("back_to_top",$lang) }}
                            <i class="fal fa-long-arrow-up"></i>
                        </a>
                    </div>
                </div>
                <div class="footer-canvas">
                    <div class="dots gallery__dots" data-dots=""></div>
                </div>
            </footer>
            <!-- footer  end -->
            <!-- share-wrapper-->
            <div class="share-wrapper">
                <div class="close-share-btn"><i class="fal fa-long-arrow-left"></i> Close</div>
                <div class="share-container fl-wrap  isShare"></div>
            </div>
            <!-- share-wrapper  end -->
        </div>
        <!-- content-holder end -->
    </div>
    <!-- wrapper end -->
</div>
<!-- Main end -->

<!--=============== scripts  ===============-->
<script type="text/javascript" src="{{ asset('assets/front/js/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/front/js/plugins.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/front/js/core.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/front/js/scripts.js') }}"></script>
</body>
</html>
