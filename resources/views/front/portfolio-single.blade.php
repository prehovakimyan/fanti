@extends('front.layouts.app')

@section('content')
    <!-- content-holder  -->
    <div class="content-holder" data-pagetitle="Portfolio single 1">
        <!-- content -->
        <div class="content">
            <!-- flat-project_title -->
            <div class="flat-project_title fl-wrap dark-bg">
                <div class="hero-canvas-wrap fs-canvas first-tile_load">
                    <div class="dots gallery__dots" data-dots=""></div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="pr-bg"></div>
                            <h2>{{ $portfolio->{"title_$lang"} }}</h2>
                        </div>
                        <div class="col-md-4 first-tile_load">
                            <span class="flat-project_title_dec color-bg"></span>
                            <div class="clearfix"></div>
                            <p>{{ $portfolio->{"short_desc_$lang"} }}</p>
                            <a href="#sec2" class="flat-project_title_link custom-scroll-link">{{ app('App\Http\Controllers\Front\DictionaryController')->getTranslate("details",$lang) }} <i class="fal fa-long-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- flat-project_title end-->
            <!-- fw-carousel-wrap -->
            <div class="fw-carousel-wrap show-case-slider-wrap dark-bg fl-wrap">
                <!-- fw-carousel  -->
                <div class="fw-carousel  fs-gallery-wrap fl-wrap full-height lightgallery" data-mousecontrol="0">
                    <div class="swiper-container">
                        <div class="swiper-wrapper">
                            @foreach($portfolio->images as $image)
                                <!-- swiper-slide-->
                                <div class="swiper-slide hov_zoom">
                                    <img  src="{{$image->image}}"   alt="">
                                    <a href="{{$image->image}}" class="box-media-zoom   popup-image"><i class="fal fa-search"></i></a>
                                    <div class="pr-bg"></div>
                                </div>
                                <!-- swiper-slide end-->
                            @endforeach
                        </div>
                    </div>
                </div>
                <!-- fw-carousel end -->
            </div>
            <!-- fw-carousel-wrap end -->
            <!-- fw-carousel-control -->
            <div class="fw-carousel-control dark-bg fl-wrap">
                <div class="fw-carousel-control_container">
                    <div class="fw-carousel-counter"></div>
                    <div class="fw_cb fw-carousel-button-next"><i class="fal fa-long-arrow-right"></i></div>
                    <div class="fw_cb fw-carousel-button-prev"><i class="fal fa-long-arrow-left"></i></div>
                </div>
                <div class="half-scrollbar">
                    <div class="hs_init"></div>
                </div>
            </div>
            <!-- fw-carousel-control end -->
            <!-- section-->
            <section class="no-padding-bottom" id="sec2">
                <!-- container-->
                <div class="container">
                    <!-- det-wrap-->
                    <div class="fl-wrap det-wrap">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="fixed-column fl-wrap">
                                    <div class="pr-bg pr-bg-white"></div>
                                    <div class="pr-title">
                                        {{ app('App\Http\Controllers\Front\DictionaryController')->getTranslate("details",$lang) }}
                                        <span>
                                            {{ app('App\Http\Controllers\Front\DictionaryController')->getTranslate("single_project_text",$lang) }}
                                        </span>
                                    </div>
                                    <ul class="pr-list dark-bg">
                                        <li><span>{{ app('App\Http\Controllers\Front\DictionaryController')->getTranslate("date",$lang) }} :</span>{{ \Carbon\Carbon::parse($portfolio->project_date)->format('d M Y') }} </li>
                                        <li><span>{{ app('App\Http\Controllers\Front\DictionaryController')->getTranslate("status",$lang) }} :</span> {{ app('App\Http\Controllers\Front\DictionaryController')->getTranslate("completed",$lang) }} </li>
                                        <li><span>{{ app('App\Http\Controllers\Front\DictionaryController')->getTranslate("location",$lang) }} : </span>  <a href="https://goo.gl/maps/UzN5m" target="_blank"> {{ $portfolio->{"location_".$lang} }}  </a></li>
                                    </ul>
                                    <div class="fixed-column-top"><i class="fal fa-long-arrow-up"></i></div>
                                </div>
                            </div>
                            <div class="col-md-8 first-tile_load">

                                <!-- tabs-container-->
                                <div id="tabs-container">
                                    <div class="tabs-counter">
                                        <span>0</span>
                                        <div class="tc_item">1</div>
                                    </div>
                                    <ul class="tabs-menu fl-wrap">
                                        <li class="selectedLava">
                                            <a href="#tab-1" data-tabnum="1">
                                                <span>01.</span>
                                                {{ app('App\Http\Controllers\Front\DictionaryController')->getTranslate("details",$lang) }}
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#tab-2" data-tabnum="2">
                                                <span>02.</span>
                                                {{ app('App\Http\Controllers\Front\DictionaryController')->getTranslate("video_presentation",$lang) }}
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#tab-3" data-tabnum="3">
                                                <span>03.</span>
                                                {{ app('App\Http\Controllers\Front\DictionaryController')->getTranslate("plans",$lang) }}
                                            </a>
                                        </li>
                                    </ul>
                                    <!-- tab-content-->
                                    <div id="tab-1" class="tab-content">
                                        <h3 class="pr-subtitle">
                                            {{ app('App\Http\Controllers\Front\DictionaryController')->getTranslate("info",$lang) }}
                                        </h3>
                                        <p>
                                            {!! $portfolio->{"desc_".$lang} !!}
                                        </p>
                                    </div>
                                    <!-- tab-content end-->
                                    @if($portfolio->video_link)
                                        <!-- tab-content-->
                                        <div id="tab-2" class="tab-content">
                                            <h3 class="pr-subtitle">
                                                {{ app('App\Http\Controllers\Front\DictionaryController')->getTranslate("presentation",$lang) }}
                                            </h3>
                                            <p>
                                                {!! $portfolio->{"video_desc_".$lang} !!}
                                            </p>
                                            <div class="video-box fl-wrap">
                                                <img src="{{$portfolio->image}}" class="respimg" alt="">
                                                <a class="video-box-btn image-popup" href="{{$portfolio->video_link}}">
                                                    <i class="fas fa-play"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <!-- tab-content end-->
                                    @endif

                                    @if(!empty($portfolio->planImages))
                                        <!-- tab-content-->
                                        <div id="tab-3" class="tab-content">
                                            <h3 class="pr-subtitle">
                                                {{ app('App\Http\Controllers\Front\DictionaryController')->getTranslate("plans_and_makets",$lang) }}
                                            </h3>
                                            <div class="palns-gal fl-wrap lightgallery">
                                                @foreach($portfolio->planImages as $planImage)
                                                    <!-- plans-gal_item-->
                                                    <div class="plans-gal_item hov_zoom">
                                                        <img src="{{ $planImage->image }}" alt="" class="respimg">
                                                        <a href="{{ $planImage->image }}" class="box-media-zoom   popup-image"><i class="fal fa-search"></i></a>
                                                    </div>
                                                    <!-- plans-gal_item end-->
                                                @endforeach
                                            </div>
                                        </div>
                                        <!-- tab-content end-->
                                    @endif
                                </div>
                                <!-- tabs-container end-->
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <!-- det-wrap end-->
                </div>
                <!-- container end -->
                <div class="clearfix"></div>
                <div class="limit-box fl-wrap"></div>
                <!--content-nav_holder-->
                <div class="content-nav_holder fl-wrap first-tile_load">
                    <div class="container">
                    </div>
                </div>
                <!-- content-nav_holder end-->
            </section>
            <!-- section end-->
        </div>
        <!-- content end -->
    </div>
    <!-- content-holder end -->
@endsection
