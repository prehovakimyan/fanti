@extends('front.layouts.home')

@section('content')
    <!-- content-holder  -->
    <div class="content-holder" data-pagetitle="Portfolio Horizontal 2">
        <!-- fixed-filter-panel -->
        <div class="fixed-filter-panel bot-element fw-panel hor-mob-filter">
            <div class="fixed-filter-panel_title color-bg hor-mob-filter_btn">
                Works Filter <i class="fal fa-long-arrow-right"></i>
            </div>
            <div class="gallery-filters">
                <a href="#" class="gallery-filter gallery-filter-active" data-filter="*">All Works</a>
                <a href="#" class="gallery-filter " data-filter=".houses">Houses</a>
                <a href="#" class="gallery-filter" data-filter=".apartments">Apartments</a>
                <a href="#" class="gallery-filter" data-filter=".interior">Interior</a>
                <a href="#" class="gallery-filter" data-filter=".design">Design</a>
            </div>
            <div class="count-folio round-counter">
                <div class="num-album"></div>
                <div class="all-album"></div>
            </div>
            <div class="scroll-down-wrap">
                <div class="mousey">
                    <div class="scroller"></div>
                </div>
                <span>Scroll or  Swipe</span>
            </div>
        </div>
        <!-- fixed-filter-panelend -->
        <!--horizontal-grid   -->
        <div class="horizontal-grid-wrap  fl-wrap  ">
            <!-- portfolio start -->
            <div id="portfolio_horizontal_container" class="two-ver-columns">

                @foreach($portfolios as $portfolio)
                    <!-- portfolio_item-->
                        <div class="portfolio_item interior">
                            <div class="grid-item-holder">
                                <img src="{{$portfolio->image}}" alt="">
                                <div class="grid-det">
                                    <div class="grid-det_category"><a href="#">Architecture</a> <a href="#">Design</a></div>
                                    <div class="grid-det-item">
                                        <a href="{{'portfolio/'.$portfolio->slug}}" class="ajax grid-det_link">
                                            {{ $portfolio->{"title_".$lang} }}
                                            <i class="fal fa-long-arrow-right"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="pr-bg"></div>
                        </div>
                        <!-- portfolio_item end-->
                @endforeach

                <!-- portfolio_item-->
                <div class="portfolio_item houses design">
                    <div class="grid-item-holder">
                        <img src="images/folio/1.jpg" alt="">
                        <div class="grid-det">
                            <div class="grid-det_category"><a href="#">Architecture</a> <a href="#">Design</a></div>
                            <div class="grid-det-item">
                                <a href="portfolio-single.html" class="ajax grid-det_link">Wood River Ships Center<i
                                        class="fal fa-long-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="pr-bg"></div>
                </div>
                <!-- portfolio_item end-->
            </div>
            <!-- portfolio end -->
        </div>
        <!--horizontal-grid end -->
    </div>
    <!-- content-holder end -->
@endsection
