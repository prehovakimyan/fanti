@extends('cellar.layouts.app')
<link href="{{ asset('assets/css/admin/admin.css') }}" rel="stylesheet">
<link href="{{ asset('assets/css/admin/jquery.switch.css') }}" rel="stylesheet">
@section('content')

    <div class="container services_block">
        <div class="col-md-8 page_show_content">
            <div class="page_title_box parent_page_title">
                <h4>Portfolio Category</h4>
                <div class="page_actions_block">
                    <a href="{{ url('/') }}/admin/portfolio-category/create" class="sub_page_add btn_2">
                        <i class="icon-docs"></i>
                        Add
                    </a>
                </div>
            </div>
            <div class="child_pages">
                @foreach($categories as $category)
                    <div class="page_title_box child_page_title">
                        <p>{{ $category->title }}</p>
                        <div class="page_actions_block">
                            <a href="{{ url('/') }}/admin/portfolio-category/{{ $category->id }}/edit" class="parent_page_edit btn_2">
                                <i class="icon-edit-3"></i>
                                Edit
                            </a>
                            <a data-id="{{ $category->id }}" class="page_del_btn portfolio_category_delete btn_delete btn-danger">
                                <i class="icon-trash"></i>
                                Delete
                            </a>
                        </div>

                    </div>
                @endforeach
            </div>
        </div>
    </div>




    </div>
@endsection
