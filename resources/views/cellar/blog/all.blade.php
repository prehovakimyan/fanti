@extends('cellar.layouts.app')
<link href="{{ asset('assets/css/admin/admin.css') }}" rel="stylesheet">
<link href="{{ asset('assets/css/admin/jquery.switch.css') }}" rel="stylesheet">
@section('content')

    <div class="container services_block">
        <div class="col-md-8 page_show_content">
            <div class="page_title_box parent_page_title">
                <h4>Blog Posts</h4>
                <div class="page_actions_block">
                    <a href="{{ url('/') }}/admin/blog/create" class="sub_page_add btn_2">
                        <i class="icon-docs"></i>
                        Add
                    </a>
                </div>
            </div>
            <div class="child_pages">
                @foreach($blogs as $blog)
                    <div class="page_title_box child_page_title">
                        @if($blog->image)
                            <div class="project_img_box col-md-2 col-sm-2">
                                <img src="{{$blog->image->image}}" >
                            </div>
                        @endif
                        <p>{{ $blog->title }}</p>
                        <div class="page_actions_block">
                            <a href="{{ url('/') }}/admin/blog/{{ $blog->id }}/edit" class="parent_page_edit btn_2">
                                <i class="icon-edit-3"></i>
                                Edit
                            </a>
                            <a data-id="{{ $blog->id }}" class="page_del_btn blog_delete btn_delete btn-danger">
                                <i class="icon-trash"></i>
                                Delete
                            </a>
                        </div>

                    </div>
                @endforeach
            </div>
        </div>
    </div>




    </div>
@endsection
