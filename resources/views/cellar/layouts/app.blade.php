<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Admin panel</title>
    <!-- Google web fonts -->
    <link href="https://fonts.googleapis.com/css?family=Gochi+Hand|Lato:300,400|Montserrat:400,400i,700,700i"
          rel="stylesheet">

    <!-- BASE CSS -->
    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.min.css">
    <link rel="stylesheet" href="{{asset('/assets/sweetalert/lib/sweet-alert.css')}}"/>
    <link href="{{ asset('/assets/css/admin/base.css?v=1.1.1') }}" rel="stylesheet">
    <link href="{{ asset('/assets/css/admin/bootstrap-datepicker3.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/tinymce/skins/lightgray/skin.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/dropzone/dropzone.css') }}" type="text/css" rel="stylesheet"/>
    <link href="{{ asset('assets/gridster/gridster.css') }}" type="text/css" rel="stylesheet"/>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
          integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link href="{{ asset('/assets/css/admin/custom_css.css') }}" rel="stylesheet">

</head>
<body class="nav-md">

<!--[if lte IE 8]>
<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a>.</p>
<![endif]-->

<div id="preloader">
    <div class="sk-spinner sk-spinner-wave">
        <div class="sk-rect1"></div>
        <div class="sk-rect2"></div>
        <div class="sk-rect3"></div>
        <div class="sk-rect4"></div>
        <div class="sk-rect5"></div>
    </div>
</div>

<div class="layer"></div>


<div class="container body">
    <div class="main_container">
        <div class="col-12 mobile-header">
            <div class="btn-burger">
                <i class="fa fa-bars" aria-hidden="true"></i>
            </div>
            <div class="profile_pic">
                Fanti
            </div>
        </div>
        <div class="col-md-3 left_col sidebar-menu">
            <div class="admin_logo">
                <div class="profile_pic">
                    Fanti
                </div>
            </div>
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                <ul class="nav side-menu">
                    <li class="@if(\Request::route()->getName() == "dashboard") {{ "active_menu_tab" }} @endif">
                        <a href="{{ url('/') }}/admin/dashboard">
                            <i class="fa fa-home" aria-hidden="true"></i>
                            Dashboard
                        </a>
                    </li>
                    <li class="submenu @if(\Request::route()->getName() == "pages.index" || \Request::route()->getName() == "pages.create" || \Request::route()->getName() == "pages.edit") {{ "active_menu_tab " }} @endif">
                        <a href="{{ url('/') }}/admin/pages">Pages</a>
                    </li>
                    <li class="submenu @if(\Request::route()->getName() == "blog.index" || \Request::route()->getName() == "blog.create" || \Request::route()->getName() == "blog.edit") {{ "active_menu_tab " }} @endif">
                        <a href="{{ url('/') }}/admin/blog">Blog</a>
                    </li>
                    <li class="submenu @if(\Request::route()->getName() == "services.index" || \Request::route()->getName() == "services.create" || \Request::route()->getName() == "services.edit") {{ "active_menu_tab " }} @endif">
                        <a href="{{ url('/') }}/admin/services">Services</a>
                    </li>
                    <li class="submenu @if(\Request::route()->getName() == "portfolio.index" || \Request::route()->getName() == "portfolio.create" || \Request::route()->getName() == "portfolio.edit") {{ "active_menu_tab " }} @endif">
                        <a href="{{ url('/') }}/admin/portfolio">Portfolio</a>
                    </li>
                    {{--{!! app('App\Http\Controllers\Admin\DashboardController')->adminMenu() !!}--}}
                </ul>
                 <ul class="nav side-menu">
                     <li class="menu_section_title"><h4>Shop pages</h4></li>

                     <li class="submenu @if(\Request::route()->getName() == "orders.index" || \Request::route()->getName() == "orders.create" || \Request::route()->getName() == "orders.edit") {{ "active_menu_tab " }} @endif">
                         <a href="{{ url('/') }}/admin/orders">Orders @if(app('App\Http\Controllers\Cellar\Products\OrderController')->newOrder() > 0) <span class="new_order">{{ "(".app('App\Http\Controllers\Cellar\Products\OrderController')->newOrder() .")"}}</span> @endif</a>
                     </li>

                     <li class="@if(\Request::route()->getName() == "prtype.create" || \Request::route()->getName() == "prtype.index" || \Request::route()->getName() == "prtype.edit" || \Request::route()->getName() == "prcats.create" || \Request::route()->getName() == "prcats.index" || \Request::route()->getName() == "prcats.edit" || \Request::route()->getName() == "prcats") {{ "active_menu_tab " }} @endif submenu">
                         <a class="show-submenu">Products <span class="fa arrow"></span></a>
                         <ul class="submenu_coach @if(\Request::route()->getName() == "prcats.create" || \Request::route()->getName() == "prcats.index" || \Request::route()->getName() == "prcats.edit" || \Request::route()->getName() == "prcats") {{ "show_normal " }} @endif">
                             <li><a href="{{ url('/') }}/admin/products" class="show-submenu">All</a></li>
                             <li><a href="{{ url('/') }}/admin/product-category" class="show-submenu">Categories</a></li>
                             <li><a href="{{ url('/') }}/admin/product-attribute" class="show-submenu">Attributes</a></li>
                         </ul>
                     </li>

                     <li class="submenu">
                         <a href="{{ url('/') }}/admin/delivery-cities">Cities</a>
                     </li>
                     <li class="submenu @if(\Request::route()->getName() == "brands.index" || \Request::route()->getName() == "brands.create" || \Request::route()->getName() == "brands.edit") {{ "active_menu_tab " }} @endif">
                         <a href="{{ url('/') }}/admin/brands">Brands</a>
                     </li>


                     {{--<li class="@if(\Request::route()->getName() == "contact_page") {{ "active_menu_tab " }} @endif submenu">
                         <a href="{{ url('/') }}/admin/contact">Contact us</a>
                     </li>

                     <li class="submenu @if(\Request::route()->getName() == "teams.index" || \Request::route()->getName() == "teams.create" || \Request::route()->getName() == "partners.edit") {{ "active_menu_tab " }} @endif">
                         <a href="{{ url('/') }}/admin/teams">Our Team</a>
                     </li>
                     <li class="submenu @if(\Request::route()->getName() == "career.index" || \Request::route()->getName() == "career.create" || \Request::route()->getName() == "partners.edit") {{ "active_menu_tab " }} @endif">
                         <a href="{{ url('/') }}/admin/career">Career</a>
                     </li>
                     <li class="submenu @if(\Request::route()->getName() == "partners.index" || \Request::route()->getName() == "partners.create" || \Request::route()->getName() == "partners.edit") {{ "active_menu_tab " }} @endif">
                         <a href="{{ url('/') }}/admin/partners">Partners</a>
                     </li>
                     <li class="submenu @if(\Request::route()->getName() == "reviews.index" || \Request::route()->getName() == "reviews.create" || \Request::route()->getName() == "reviews.edit") {{ "active_menu_tab " }} @endif">
                         <a href="{{ url('/') }}/admin/reviews">Reviews</a>
                     </li>--}}

                 </ul>
                 <ul class="nav side-menu">
                     <li class="menu_section_title"><h4>Additional pages</h4></li>

                    {{-- <li class="@if(\Request::route()->getName() == "prtype.create" || \Request::route()->getName() == "prtype.index" || \Request::route()->getName() == "prtype.edit" || \Request::route()->getName() == "prcats.create" || \Request::route()->getName() == "prcats.index" || \Request::route()->getName() == "prcats.edit" || \Request::route()->getName() == "prcats") {{ "active_menu_tab " }} @endif submenu">
                         <a class="show-submenu">Products <span class="fa arrow"></span></a>
                         <ul class="submenu_coach @if(\Request::route()->getName() == "prcats.create" || \Request::route()->getName() == "prcats.index" || \Request::route()->getName() == "prcats.edit" || \Request::route()->getName() == "prcats") {{ "show_normal " }} @endif">
                             <li><a href="{{ url('/') }}/admin/products" class="show-submenu">All</a></li>
                             <li><a href="{{ url('/') }}/admin/prcats" class="show-submenu">Categories</a></li>
                             <li><a href="{{ url('/') }}/admin/prcats?type=1" class="show-submenu">Countries</a></li>
                             <li><a href="{{ url('/') }}/admin/product-attribute" class="show-submenu">Attributes</a></li>
                         </ul>
                     </li>--}}


                   {{--  <li class="submenu @if(\Request::route()->getName() == "brands.index" || \Request::route()->getName() == "brands.create" || \Request::route()->getName() == "brands.edit") {{ "active_menu_tab " }} @endif">
                         <a href="{{ url('/') }}/admin/brands">Brands</a>
                     </li>--}}
                     <li class="@if(\Request::route()->getName() == "slider.edit" || \Request::route()->getName() == "slide_edit") {{ "active_menu_tab " }} @endif submenu">
                         <a href="{{ url('/') }}/admin/slider/" class="show-submenu">Slider</a>
                     </li>
                     <li class="submenu @if(\Request::route()->getName() == "dictionary.index") {{ "active_menu_tab " }} @endif">
                         <a href="{{ url('/') }}/admin/dictionary">Dictionary</a>
                     </li>
                     <li>
                         <a href="{{ url('admin/logout') }}">
                             LogOut
                         </a>
                     </li>
                     {{--<li class="@if(\Request::route()->getName() == "contact_page") {{ "active_menu_tab " }} @endif submenu">
                         <a href="{{ url('/') }}/admin/contact">Contact us</a>
                     </li>

                     <li class="submenu @if(\Request::route()->getName() == "teams.index" || \Request::route()->getName() == "teams.create" || \Request::route()->getName() == "partners.edit") {{ "active_menu_tab " }} @endif">
                         <a href="{{ url('/') }}/admin/teams">Our Team</a>
                     </li>
                     <li class="submenu @if(\Request::route()->getName() == "career.index" || \Request::route()->getName() == "career.create" || \Request::route()->getName() == "partners.edit") {{ "active_menu_tab " }} @endif">
                         <a href="{{ url('/') }}/admin/career">Career</a>
                     </li>
                     <li class="submenu @if(\Request::route()->getName() == "partners.index" || \Request::route()->getName() == "partners.create" || \Request::route()->getName() == "partners.edit") {{ "active_menu_tab " }} @endif">
                         <a href="{{ url('/') }}/admin/partners">Partners</a>
                     </li>
                     <li class="submenu @if(\Request::route()->getName() == "reviews.index" || \Request::route()->getName() == "reviews.create" || \Request::route()->getName() == "reviews.edit") {{ "active_menu_tab " }} @endif">
                         <a href="{{ url('/') }}/admin/reviews">Reviews</a>
                     </li>--}}

                 </ul>
                 {{--<ul class="nav side-menu">
                     <li class="menu_section_title"><h4>Admin</h4></li>
                     <li class="submenu">
                         <a href="{{ url('/') }}/admin/orders">Orders @if(app('App\Http\Controllers\Admin\DashboardController')->newOrder() > 0) <span class="new_order">{{ "(".app('App\Http\Controllers\Admin\DashboardController')->newOrder() .")"}}</span> @endif</a>
                     </li>
                     @if(session('super_admin'))
                         <li class="submenu @if(\Request::route()->getName() == "pages.index" || \Request::route()->getName() == "pages.create" || \Request::route()->getName() == "pages.edit") {{ "active_menu_tab " }} @endif">
                             <a href="{{ url('/') }}/admin/pages">Pages</a>
                         </li>
                         <li class="submenu @if(\Request::route()->getName() == "admin_users") {{"active_menu_tab"}} @endif ">
                             <a href="{{ url('/') }}/admin/admin_users">USERS</a>
                         </li>
                         <li class="submenu @if(\Request::route()->getName() == "settings") {{ "active_menu_tab " }} @endif">
                             <a href="{{ url('/') }}/admin/settings">Settings</a>
                         </li>
                         <li class="submenu @if(\Request::route()->getName() == "glossary.index") {{ "active_menu_tab " }} @endif">
                             <a href="{{ url('/') }}/admin/glossary">Translations</a>
                         </li>
                     @endif
                     <li>
                         <a href="{{ url('admin/logout') }}">
                             Logout
                         </a>
                     </li>

                 </ul>--}}

            </div>
            <!-- End main-menu -->
            {{-- <ul id="top_tools">
                 <li class="user_logaut dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                         Admin <span class="caret"></span>
                     </a>

                     <ul class="user_logout_drowp dropdown-menu" role="menu">
                         <li>
                             <a href="{{ url('admin/logout') }}">
                                 Դուրս գալ
                             </a>
                         </li>
                         <li>
                             <a href="{{ url('admin/settings') }}">Settings</a>
                         </li>
                     </ul>
                 </li>
             </ul>--}}
        </div>
        <div class="right_col">
            @yield('content')
        </div>
    </div>
    <div class="sub_pdf_append_popup">
        <div class="sub_pdf_append_block show_block">
            <div id="sub_pdf_append_head" class="sub_pdf_append_head">
                <div class="sub_pdf_append_title">Upload pdf</div>
                <div class="sub_pdf_append_dragh"></div>
                <button type="button" class="sub_pdf_append_close" aria-hidden="true"><i class="mce-ico mce-i-remove"></i></button>
            </div>
            <div class="sub_pdf_append_head_lang">

            </div>
            <div class="form-group ">
                <label for="title">Title</label>
                <input id="about_title" type="text" class="append_file_title form-control" name="pdf_title" value=""
                       autofocus>
            </div>
            <div class="form-group">
                <label for="Description">Short Description</label>
                <textarea class="form-control append_file_desc short_desc" name="pdf_desc"></textarea>
            </div>
            <div class="sub_pdf_append_box form-group">
                <div class="">
                    <label>Upload PDF</label>
                </div>
                <div class="pdf_file_title">
                    <input type="text" name="old_pdf_title" readonly="" class="append_pdf_change old_pdf_title" value="">
                </div>
                <div class="file-upload btn btn_1 green" style="float:left;">
                    <span>Choose file</span>
                    <input type="file" name="pdf_file" id="myFile" class="image upload news_sub_pdf_file">
                </div>
            </div>
            <input type="hidden" class="news_sub_pdf_parent" name="parent_id" value="">
            <div class="sub_pdf_append_footer">
                <button type="button" class="sub_pdf_append_save">Save</button>
            </div>
        </div>
    </div>

    <div class="edit_sub_pdf_append_popup">

    </div>






    <div class="edit_sub_pages_with_pdf_append_popup">

    </div>

</div>
<div id="toTop"></div>

<script src="{{ asset('/assets/js/jquery-2.2.4.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.min.js"></script>
<script src="{{ asset('assets/js/common_scripts_min.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('assets/js/functions.js') }}"></script>
{{--<script src="{{ asset('tinymce/tinymce.min.js') }}"></script>--}}
<script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=1gy4lpyq9463q61q1asiaiau3e5eke4gl3y1lcp1lq4v9t8o"></script>
<script src="{{ asset('assets/sweetalert/lib/sweet-alert.js')}}"></script>
<script type="text/javascript" src="{{ asset('assets/dropzone/dropzone.js') }}"></script>
<script src="{{ asset('assets/gridster/gridster.js')}}"></script>
<script src="{{ asset('assets/js/actions.js')}}"></script>
<script>
  $("document").ready(function () {
    $(document).on("click", ".lang_tab", function () {
      var lang_short = $(this).attr("data-tab");
      $(".lang_tab").removeClass("active_tab");
      if(lang_short == 'ir') {
        $('.lang_field_ir').attr('dir', "rtl");
      } else {
        $('.lang_field_ir').attr('dir', "ltr");
      }

      if (!($("." + lang_short + "_tab").hasClass("active_tab"))) {
        $("." + lang_short + "_tab").addClass("active_tab");
      }
      $(".lang_field").removeClass("active_field").addClass("hidden_field");
      $(".lang_field_" + lang_short).addClass("active_field").removeClass("hidden_field");

    });
  });
  $("document").ready(function () {

    $(".sub_pdf_append_save").click(function () {
      var result = true;
      var current_page_id = $(".current_page_id").val();

      if($(".append_file_title").val() != ''){
        $(".append_file_title").removeClass("error_empty");
        var title = $(".append_file_title").val();
      }else{
        result = false;
        $(".append_file_title").addClass("error_empty");
      }

      var desc = $(".append_file_desc").val();

      var file_data = $('#myFile').prop('files')[0];
      var form_data = new FormData();
      form_data.append('file', file_data);
      form_data.append('title', title);
      form_data.append('desc', desc);
      form_data.append('parent_id', current_page_id);

      console.log(file_data);
      if(result){
        $.ajax({
          type: "POST",
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          url: '/admin/news_sub_page_pdf/upload_pdf',
          data: form_data,
          type: 'POST',
          contentType: false, // The content type used when sending data to the server.
          cache: false, // To unable request pages to be cached
          processData: false,
          success: function (msg) {
            $(".append_file_title").val('');
            $(".append_file_desc").val('');
            $(".old_pdf_title ").val('');
            $("#myFile ").val('');

            $(".page_pdf_plus_block").prepend(msg);
            $(".sub_pdf_append_popup").toggle(200);
          }
        });
      }




    });

    $(".page_with_pdf_append_save").click(function () {

      var result = true;
      var current_page_id = $(".current_page_id").val();
      var site_langs = $(".site_langs").val();
      var res = site_langs.split(",");
      var form_data = new FormData();
      form_data.append('parent_id', current_page_id);
      res.forEach(function(entry) {
        if(entry !=''){
          var lang = entry;
          var title = $(".append_page_with_pdf_file_title_" + entry).val();
          var desc = $(".append_page_with_pdf_file_desc").val();
          form_data.append('title_' + lang, title);
          form_data.append('desc_' + lang, desc);
          var file_data = $('#page_with_pdf_file_' + lang).prop('files')[0];
          form_data.append('file_' + lang, file_data);
        }

      });

      if(result){
        $.ajax({
          type: "POST",
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          url: '/admin/sub_pages_with_pdf/upload_pdf',
          data: form_data,
          type: 'POST',
          contentType: false, // The content type used when sending data to the server.
          cache: false, // To unable request pages to be cached
          processData: false,
          success: function (msg) {
            if(msg == 0){
              alert('Please choose file');
            }else{
              $(".append_page_with_pdf_file_title").val('');
              $(".append_page_with_pdf_file_desc").val('');
              $(".old_pdf_title ").val('');
              $("#page_with_pdf_file ").val('');

              $(".page_pdf_plus_block").prepend(msg);
              $(".sub_pages_with_pdf_append_popup").toggle(200);
            }

          }
        });
      }




    });


    $(document).on("click", ".open_sub_pdf_block p", function(){
      var id = $(this).attr("data-id");

      $.ajax({
        type: "POST",
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '/admin/news_sub_page_pdf/edit_sub_pdf_block',
        data: {"id": id},
        success: function (msg) {
          $(".edit_sub_pdf_append_popup").append(msg);
          $(".edit_sub_pdf_append_popup").toggle(200);
        }
      });
    });





    $(document).on("click", ".open_sub_pages_with_block p", function(){
      var id = $(this).attr("data-id");

      $.ajax({
        type: "POST",
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '/admin/sub_pages_with_pdf/edit_sub_pdf_block',
        data: {"id": id},
        success: function (msg) {
          $(".edit_sub_pages_with_pdf_append_popup").append(msg);
          $(".edit_sub_pages_with_pdf_append_popup").toggle(200);
        }
      });
    });



    $(".news_sub_pdf_file").change(function () {
      var file_data = $('#myFile').prop('files')[0];
      $(".append_pdf_change").val(file_data.name);
    });
    $(".page_with_pdf_file").change(function () {
      var  lang = $(this).data("lang");
      var file_data = $('#page_with_pdf_file_' + lang).prop('files')[0];
      $(".append_page_with_pdf_change_" + lang).val(file_data.name);
    });


    $(document).on("change", "#editMyFile", function () {
      var file_data = $('#editMyFile').prop('files')[0];
      $(".append_pdf_change").val(file_data.name);
    });

    $(document).on("change", "#editpage_with_pdf", function () {
      var file_data = $('#editpage_with_pdf').prop('files')[0];
      $(".append_pdf_change_edit").val(file_data.name);
    });


    $(".sub_pdf_append_close").click(function () {
      $(".sub_pdf_append_popup").toggle(200);
    });
    $(".sub_with_pdf_append_close").click(function () {
      $(".sub_pages_with_pdf_append_popup").toggle(200);
    });
    $(document).on("click", ".sub_with_pdf_append_close_edit", function () {
      $(".edit_sub_pages_with_pdf_append_popup").toggle(200);
      $(".edit_sub_pages_with_pdf_append_popup .sub_pdf_append_block ").remove();
    });

    $(".option_hidden").change(function () {
      var update_hidden = $(this).val();
      var id = $(this).attr("data-id");
      var class_Id = ".option_hidden" + id;
      $.ajax({
        type: "POST",
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '/admin/services/changeHidden',
        data: {"update_hidden": update_hidden, "id": id},
        success: function (msg) {
          $(class_Id).val(msg);
        }
      });
    });
    $(".category_hidden").change(function () {
      var update_hidden = $(this).val();
      var id = $(this).attr("data-id");
      var class_Id = ".option_hidden" + id;
      $.ajax({
        type: "POST",
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '/admin/categories/changeHidden',
        data: {"update_hidden": update_hidden, "id": id},
        success: function (msg) {
          $(class_Id).val(msg);
        }
      });
    });

    // Comments Hidden
    $(".comment_hidden").change(function () {
      var update_hidden = $(this).val();
      var id = $(this).attr("data-id");
      var class_Id = ".comment_hidden" + id;
      $.ajax({
        type: "POST",
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '/admin/comments/changeHidden',
        data: {"update_hidden": update_hidden, "id": id},
        success: function (msg) {
          $(class_Id).val(msg);
        }
      });
    });
    $(".project_hidden").change(function () {
      var update_hidden = $(this).val();
      var id = $(this).attr("data-id");
      var class_Id = ".option_hidden" + id;
      $.ajax({
        type: "POST",
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '/admin/projects/changeHidden',
        data: {"update_hidden": update_hidden, "id": id},
        success: function (msg) {
          $(class_Id).val(msg);
        }
      });
    });
    $(".partner_hidden").change(function () {
      var update_hidden = $(this).val();
      var id = $(this).attr("data-id");
      var class_Id = ".option_hidden" + id;
      $.ajax({
        type: "POST",
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '/admin/partners/changeHidden',
        data: {"update_hidden": update_hidden, "id": id},
        success: function (msg) {
          $(class_Id).val(msg);
        }
      });
    });
    $(".company_hidden").change(function () {
      var company_hidden = $(this).val();
      var id = $(this).attr("data-id");
      var class_Id = ".option_hidden" + id;
      $.ajax({
        type: "POST",
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '/admin/companies/changeHidden',
        data: {"update_hidden": company_hidden, "id": id},
        success: function (msg) {
          $(class_Id).val(msg);
        }
      });
    });
    $(".slide_hidden").change(function () {
      var slide_hidden = $(this).val();
      var id = $(this).attr("data-id");
      var class_Id = ".option_hidden" + id;
      $.ajax({
        type: "POST",
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '/admin/slider/changeHidden',
        data: {"update_hidden": slide_hidden, "id": id},
        success: function (msg) {
          $(class_Id).val(msg);
        }
      });
    });

    $(".user_logaut").click(function () {
      $(".user_logout_drowp").toggle(200)
    })

  });
</script>
<script type="text/javascript">

  $(".add_slider_button").click(function () {
    $(".add_silder").toggleClass("hidden");
  });


  var gridster;
  $(function () {

    gridster = $(".gridster > ul").gridster({
      widget_margins: [5, 5],
      widget_base_dimensions: [40, 40],
      resize: {
        enabled: false
      }
    }).data('gridster');
  });

  /*$(document).ready(function(){
      $(".childe").each(function(){
          $(this).attr("data-col","2");
      })
  });*/
  function remove_from_menu(elm) {
    var erd = $(elm).parent();
    gridster.remove_widget(erd);
    var numItems = $('.gs-w').length;
    if (numItems > 0) {
      $(".menu_item_not_null").addClass("hidden");
      $(".menu_item_null").removeClass("hidden");
    }
  }

  function remove_from_authors(elm) {
    var erd = $(elm).parent();
    $(erd).remove();

  }

  $(".add_to_menu_pages").click(function () {

    $(this).prev("div").children(".one_row").each(function () {
      if ($(this).children(".page_add").is(":checked")) {
        var page_id = $(this).children(".page_add").val();
        var page_title = $(this).children("label").text();
        var i = 0;
        $(".gs-w").each(function () {
          i++;
        });
        gridster.add_widget('<li class="gs-w" data-id_p="' + page_id + '">' + page_title + '<a onclick="remove_from_menu(this)"><i class="icon-cancel-2"></i></a></li>', 8, 1, 1, i + 1);
        $(this).children(".page_add").prop("checked", false);
        var numItems = $('.gs-w').length;
        if (numItems > 0) {
          $(".menu_item_not_null").removeClass("hidden");
          $(".menu_item_null").addClass("hidden");
        }
      }

    })
  });
  $(".add_to_menu_cats").click(function () {
    $(this).prev("div").children(".one_row").each(function () {
      if ($(this).children(".page_add").is(":checked")) {
        var page_id = $(this).children(".page_add").val();
        var page_title = $(this).children("label").text().replace(/ -/g, '');
        var i = 0;
        $(".gs-w").each(function () {
          i++;
        });
        gridster.add_widget('<li class="gs-w" data-id_c="' + page_id + '">' + page_title + '<a onclick="remove_from_menu(this)"><i class="icon-cancel-2"></i></a></li>', 8, 1, 1, i + 1);
        $(this).children(".page_add").prop("checked", false);

      }
    })
  });
  $(".add_to_menu_link").click(function () {
    var parent = $(this).prev();
    var link_name = parent.children(".link_name").val();
    var link_href = parent.children(".link_href").val();

    var link_name_ru = parent.children(".link_name_ru").val();
    var link_href_ru = parent.children(".link_href_ru").val();

    var link_name_en = parent.children(".link_name_en").val();
    var link_href_en = parent.children(".link_href_en").val();

    $.ajax({
      type: "POST",
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      url: '/admin/menu/add_link_to_menu',
      data: {
        "link_name": link_name,
        "link_href": link_href,
        "link_name_ru": link_name_ru,
        "link_href_ru": link_href_ru,
        "link_name_en": link_name_en,
        "link_href_en": link_href_en
      },
      success: function (msg) {
        var i = 0;
        $(".gs-w").each(function () {
          i++;
        });
        gridster.add_widget('<li class="gs-w" data-id_l="' + msg + '">' + link_name + '<a onclick="remove_from_menu(this)"><i class="icon-cancel-2"></i></a></li>', 8, 1, 1, i + 1);
        parent.children(".link_name").val('');
        var link_ru_href = parent.children(".link_href").val('');
      }
    });
  });
  $(".save_menu").click(function () {
    var json = {};
    var i = 0;
    $(".gs-w").each(function () {
      json[i] = {};
      if ($(this).data("id_m") !== undefined) {
        json[i]['type'] = 'old';
        json[i]['id'] = $(this).data("id_m");
        json[i]['row'] = $(this).data("row");
        json[i]['col'] = $(this).data("col");
      }
      if ($(this).data("id_p") !== undefined) {
        json[i]['type'] = 'page';
        json[i]['id'] = $(this).data("id_p");
        json[i]['row'] = $(this).data("row");
        json[i]['col'] = $(this).data("col");
      }
      if ($(this).data("id_c") !== undefined) {
        json[i]['type'] = 'category';
        json[i]['id'] = $(this).data("id_c");
        json[i]['row'] = $(this).data("row");
        json[i]['col'] = $(this).data("col");
      }
      if ($(this).data("id_l") !== undefined) {
        json[i]['type'] = 'link';
        json[i]['id'] = $(this).data("id_l");
        json[i]['row'] = $(this).data("row");
        json[i]['col'] = $(this).data("col");
      }
      i++;
    });
    var a = JSON.stringify(json);
    var current_menu_id = $(".current_menu_id").val();
    var menu_title = $(".new_menu_title_input").val();

    $.ajax({
      type: "POST",
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      url: '/admin/menu/save_menu',
      data: {"json": a, "menu_title": menu_title, "menu_id": current_menu_id},
      success: function (msg) {
        document.location.href = "/admin/menu/" + current_menu_id + "/edit";
      }
    });
  });


  var numItems = $('.gs-w').length;
  if (numItems > 0) {
    $(".menu_item_not_null").removeClass("hidden");
    $(".menu_item_null").addClass("hidden");
  }
</script>
<script>

</script>
<script type="text/javascript">
  $(document).on('change', '#uploadBtn1', function () {
    var val = $(this).val();
    $("#uploadFile1").val(val);
    $("#file1_ch").val("1");
  });


  function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
        $('#uploadPreview').attr('src', e.target.result);
        $(".profile-image-preview").css("display", "block");
      };
      reader.readAsDataURL(input.files[0]);
    }
  }

  function readURL2(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
        $('#uploadPreview2').attr('src', e.target.result);
        $(".profile-image-preview2").css("display", "block");
      };
      reader.readAsDataURL(input.files[0]);
    }
  }

  function readURLLogo(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
        $('#uploadPreviewLogo').attr('src', e.target.result);
        $(".profile-image-preview_logo").css("display", "block");
      };
      reader.readAsDataURL(input.files[0]);
    }
  }

  function readURLBackImage(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
        $('#uploadPreviewBackImage').attr('src', e.target.result);
        $(".profile-image-preview_back_image").css("display", "block");
      };
      reader.readAsDataURL(input.files[0]);
    }
  }

  function readHomeBanner1(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
        $('#uploadPreviewBanner1').attr('src', e.target.result);
        $(".profile-image-preview_banner1").css("display", "block");
      };
      reader.readAsDataURL(input.files[0]);
    }
  }
  function readHomeBanner2(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
        $('#uploadPreviewBanner2').attr('src', e.target.result);
        $(".profile-image-preview_banner2").css("display", "block");
      };
      reader.readAsDataURL(input.files[0]);
    }
  }


  function readURLLogo_ru(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
        $('#uploadPreviewLogo_ru').attr('src', e.target.result);
        $(".profile-image-preview_logo_ru").css("display", "block");
      };
      reader.readAsDataURL(input.files[0]);
    }
  }

  function readURLLogo_en(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
        $('#uploadPreviewLogo_en').attr('src', e.target.result);
        $(".profile-image-preview_logo_en").css("display", "block");
      };
      reader.readAsDataURL(input.files[0]);
    }
  }

  function readURLBg(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
        $('#uploadPreviewBg').attr('src', e.target.result);
        $(".profile-image-preview_bg").css("display", "block");
      };
      reader.readAsDataURL(input.files[0]);
    }
  }


  //Edit Company images
  function readURLEdit(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
        $('#uploadPreview').attr('src', e.target.result);
        $(".profile-image-preview").css("display", "block");
        $(".no_img").val('0');
      };
      reader.readAsDataURL(input.files[0]);
    }
  }

  function readURLEditBg(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
        $('#uploadPreviewBg').attr('src', e.target.result);
        $(".profile-image-preview_bg").css("display", "block");
      };
      reader.readAsDataURL(input.files[0]);
    }
  }

  function readURLEditLogo(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
        $('#uploadPreviewLogo').attr('src', e.target.result);
        $(".profile-image-preview_logo").css("display", "block");
      };
      reader.readAsDataURL(input.files[0]);
    }
  }

  function readURLPdf(input) {
    var file_name = input.files[0].name;
    var className = input.getAttribute("data-lang");
    $("." + className).val(file_name);
  }


  $(".img_text_position").click(function () {
    $(".img_text_position").removeClass("avtive_position");
    $(this).addClass("avtive_position");
    var type = $(this).attr("data-type");
    $(".img_position_type").val(type);
  });

  function deleteimg(id) {
    $('#uploadPreview').attr('src', '');
    $(".profile-image-preview").css("display", "none");
    $(".image").val('');
  }
  function deleteimg2(id) {
    $('#uploadPreview2').attr('src', '');
    $(".profile-image-preview2").css("display", "none");
    $(".image2").val('');
  }

  function deleteimgBg(id) {
    $('#uploadPreviewBg').attr('src', '');
    $(".profile-image-preview_bg").css("display", "none");
    $(".uploadBtnBg").val('');
  }

  function deleteimgBackImage(id) {
    $('#uploadPreviewBackImage').attr('src', '');
    $(".profile-image-preview_back_image").css("display", "none");
    $(".uploadBtnBackImage").val('');
  }

  function deleteimgLogo(id) {
    $('#uploadPreviewLogo').attr('src', '');
    $(".profile-image-preview_logo").css("display", "none");
    $(".old_image_logo").val('');
  }

  function deleteimgLogo_en(id) {
    $('#uploadPreviewLogo_en').attr('src', '');
    $(".profile-image-preview_logo_en").css("display", "none");
    $(".uploadBtnLogo_en").val('');
  }

  function deleteimgLogoru(id) {
    $('#uploadPreviewLogo_ru').attr('src', '');
    $(".profile-image-preview_logo_ru").css("display", "none");
    $(".uploadBtnLogo_ru").val('');
  }

  //Rdit delete images
  function deleteimgedit(id) {
    $('#uploadPreview').attr('src', '');
    $(".profile-image-preview").css("display", "none");
    $(".image").val('');
    $(".old_image").val('');
  }

  function deleteimgeditBg(id) {
    $('#uploadPreviewBg').attr('src', '');
    $(".profile-image-preview_bg").css("display", "none");
    $(".image_bg").val('');
    $(".old_image_bg").val('');
  }

  function deleteimgeditLogo(id) {
    $('#uploadPreviewLogo').attr('src', '');
    $(".profile-image-preview_logo").css("display", "none");
    $(".image_logo").val('');
    $(".old_image_logo").val('');
  }
</script>
<script>
  $('#datapicker2').datepicker({
    format: 'dd-mm-yyyy'
  });
  $('#datapicker3').datepicker({
    format: 'dd-mm-yyyy'
  });
  $(".menus").change(function () {
    var a = $(this).val();
    document.location.href = "/admin/menu/" + a + "/edit";
  });
  $(".remove_pdf").click(function () {
    $(".isset_pdf").remove();
    $(".old_pdf").val('');
  });
  //Authors plus
  $(".author_plus").on("click", function () {
    $.ajax({
      type: "POST",
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      url: '/admin/authors/add_author',
      success: function (msg) {
        $(".article_authors_block").append(msg);
      }
    });
  });


  //Service plus
  $(".service_sub_plus").on("click", function () {
    $.ajax({
      type: "POST",
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      url: '/admin/services/add_sub_pages',
      success: function (msg) {
        $(".service_sub_block").append(msg);
      }
    });
  });


  //Article pdf plus
  $(".pdf_plus").on("click", function () {
    $.ajax({
      type: "POST",
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      url: '/admin/articles/add_pdf',
      success: function (msg) {
        $(".article_pdf_block").append(msg);
      }
    });
  });

  $("#cat_id").change(function () {
    var cat_id = $(this).val();
    if (cat_id == 1) {
      $(".not_media_box").addClass("not_media");
      $(".media_link").css("display", "block");
    } else {
      $(".not_media_box").removeClass("not_media");
      $(".media_link").css("display", "none");
    }
  });
  $(".open_close_meta").click(function () {
    $(".meta_box").slideToggle(200);
  });
  $('#datapicker2').datepicker({
    format: 'yyyy-dd-mm'
  });
</script>
<script>

  tinymce.init({
    selector: 'textarea.description',
    height: 500,
    setup: function (editor) {
      editor.on('init change', function () {
        editor.save();
      });
    },
    plugins: [
      "advlist autolink lists link image charmap print preview anchor",
      "searchreplace visualblocks code fullscreen",
      "insertdatetime media table contextmenu paste"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
    image_title: true,
    automatic_uploads: true,
//        images_upload_url: 'http://energy.mycard.am/admin/upload_content',
    images_upload_url: 'https://imp-ex.am/admin/upload_content',
    file_picker_types: 'image',
    file_picker_callback: function(cb, value, meta) {
      var input = document.createElement('input');
      input.setAttribute('type', 'file');
      input.setAttribute('accept', 'image/*');
      input.onchange = function() {
        var file = this.files[0];
        console.log(file);
        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function () {

          console.log();

          /* var id = 'blobid' + (new Date()).getTime();
           var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
           var base64 = reader.result.split(',')[1];
           var blobInfo = blobCache.create(id, file, base64);
           blobCache.add(blobInfo);
           cb(blobInfo.blobUri(), { title: file.name });*/
        };
      };
      input.click();
    },
    directionality: 'ltr'
  })
  tinymce.init({
    selector: 'textarea.description_ir',
    height: 500,
    setup: function (editor) {
      editor.on('init change', function () {
        editor.save();
      });
    },
    plugins: [
      "advlist autolink lists link image charmap print preview anchor",
      "searchreplace visualblocks code fullscreen",
      "insertdatetime media table contextmenu paste"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
    image_title: true,
    automatic_uploads: true,
//        images_upload_url: 'http://energy.mycard.am/admin/upload_content',
    images_upload_url: 'https://imp-ex.am/admin/upload_content',
    file_picker_types: 'image',
    file_picker_callback: function(cb, value, meta) {
      var input = document.createElement('input');
      input.setAttribute('type', 'file');
      input.setAttribute('accept', 'image/*');
      input.onchange = function() {
        var file = this.files[0];
        console.log(file);
        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function () {

          console.log();

          /* var id = 'blobid' + (new Date()).getTime();
           var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
           var base64 = reader.result.split(',')[1];
           var blobInfo = blobCache.create(id, file, base64);
           blobCache.add(blobInfo);
           cb(blobInfo.blobUri(), { title: file.name });*/
        };
      };
      input.click();
    },
    directionality: 'rtl'
  })

  tinymce.init({
    selector: 'textarea.description_pr_ir',
    height: 250,
    setup: function (editor) {
      editor.on('init change', function () {
        editor.save();
      });
    },
    plugins: [
      "advlist autolink lists link image charmap print preview anchor",
      "searchreplace visualblocks code fullscreen",
      "insertdatetime media table contextmenu paste"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
    image_title: true,
    automatic_uploads: true,
//        images_upload_url: 'http://energy.mycard.am/admin/upload_content',
    images_upload_url: 'https://imp-ex.am/admin/upload_content',
    file_picker_types: 'image',
    file_picker_callback: function(cb, value, meta) {
      var input = document.createElement('input');
      input.setAttribute('type', 'file');
      input.setAttribute('accept', 'image/*');
      input.onchange = function() {
        var file = this.files[0];
        console.log(file);
        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function () {

          console.log();

          /* var id = 'blobid' + (new Date()).getTime();
           var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
           var base64 = reader.result.split(',')[1];
           var blobInfo = blobCache.create(id, file, base64);
           blobCache.add(blobInfo);
           cb(blobInfo.blobUri(), { title: file.name });*/
        };
      };
      input.click();
    },
    directionality: 'rtl'
  })
  tinymce.init({
    selector: 'textarea.description_pr',
    height: 250,
    setup: function (editor) {
      editor.on('init change', function () {
        editor.save();
      });
    },
    plugins: [
      "advlist autolink lists link image charmap print preview anchor",
      "searchreplace visualblocks code fullscreen",
      "insertdatetime media table contextmenu paste"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
    image_title: true,
    automatic_uploads: true,
//        images_upload_url: 'http://energy.mycard.am/admin/upload_content',
    images_upload_url: 'https://imp-ex.am/admin/upload_content',
    file_picker_types: 'image',
    file_picker_callback: function(cb, value, meta) {
      var input = document.createElement('input');
      input.setAttribute('type', 'file');
      input.setAttribute('accept', 'image/*');
      input.onchange = function() {
        var file = this.files[0];
        console.log(file);
        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function () {

          console.log();

          /* var id = 'blobid' + (new Date()).getTime();
           var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
           var base64 = reader.result.split(',')[1];
           var blobInfo = blobCache.create(id, file, base64);
           blobCache.add(blobInfo);
           cb(blobInfo.blobUri(), { title: file.name });*/
        };
      };
      input.click();
    },
    directionality: 'ltr'
  })



  $(document).ready(function () {
    $(".checked_all").click(function () {
      var checked_delete_disable = $(".checked_delete");
      var checkBoxes = $(".checked_one_input");
      var checkBoxesType = $(".checked_one_input").next(".check_type");
      checked_delete_disable.prop("disabled", !checked_delete_disable.prop("disabled"));
      checkBoxes.prop("checked", !checkBoxes.prop("checked"));
      checkBoxesType.prop("checked", !checkBoxesType.prop("checked"));
    });
  });
  $(document).ready(function () {
    $(".checked_one_input").change(function () {
      $(".checked_delete").prop("disabled", false);
      var checkBoxesType = $(this).next(".check_type");
      checkBoxesType.prop("checked", !checkBoxesType.prop("checked"));
    });
  });

  $(document).ready(function () {
    $(".visible").click(function () {
      var id = $(this).attr("data-id");
      var type = $(this).attr("data-type");
      var hidden = $(this).attr("data-hidden");
      $.ajax({
        type: "POST",
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '/admin/hidden_check',
        data: {"hidden": hidden, "id": id, "type": type},
        success: function (msg) {
          location.reload();
        }
      });
    });
    $(".news_sub_page_pdf_add").click(function () {
      $(".sub_pdf_append_popup").slideToggle(200);
    });
    $(".sub_page_with_pdf_add").click(function () {
      $(".sub_pages_with_pdf_append_popup").slideToggle(200);
    });
  });



  $(document).on("click", '.add_charecter', function () {
    var row = parseInt($(this).attr('data-row'));
    var nextRow = row + 1;
    $(this).attr('data-row', nextRow);
    var char_block = ' <div class="charecter_group"><div class="form-group char_block lang_field lang_field_am">\n' +
        '                                           <input type="hidden" class="form-control" name="child_product_id[]" value="">\n' +
        '                                           <input id="additional_size_am" type="text" class="additional_size_am_'+ row +' additional_size_am form-control" placeholder="Size" name="additional_size_am[]" value=""\n' +
        '                                                   autofocus>\n' +
        '                                            <input id="additional_price_am" type="text" class="additional_price_am_'+ row +' additional_price_am form-control" placeholder="Price" name="additional_price_am[]" value=""\n' +
        '                                                   autofocus>\n' +
        '                                        </div>\n' +
        '                                        <div class="form-group char_block lang_field lang_field_ru hidden_field">\n' +
        '                                            <input id="additional_size_ru" type="text" class="additional_size_ru_'+ row +' additional_size_ru form-control" placeholder="Size" name="additional_size_ru[]" value=""\n' +
        '                                                   autofocus>\n' +
        '                                            <input id="additional_price_ru" type="text" class="additional_price_ru_'+ row +' additional_price_ru form-control" placeholder="Price" name="additional_price_ru[]" value=""\n' +
        '                                                   autofocus>\n' +
        '                                        </div>\n' +
        '                                        <div class="form-group char_block lang_field lang_field_en hidden_field">\n' +
        '                                            <input id="additional_size_en" type="text" class="additional_size_en_'+ row +' additional_size_en form-control" placeholder="Size" name="additional_size_en[]" value=""\n' +
        '                                                   autofocus>\n' +
        '                                            <input id="additional_price_en" type="text" class="additional_price_en_'+ row +' additional_price_en form-control" placeholder="Price" name="additional_price_en[]" value=""\n' +
        '                                                   autofocus>\n' +
        '                                        </div>\n' +
        '                                        <div class="form-group char_block lang_field lang_field_ir hidden_field">\n' +
        '                                            <input id="additional_size_ir" type="text" class="additional_size_ir_'+ row +' additional_size_ir form-control" placeholder="Size" name="additional_size_ir[]" value=""\n' +
        '                                                   autofocus>\n' +
        '                                            <input id="additional_price_ir" type="text" class="additional_price_ir_'+ row +' additional_price_ir form-control" placeholder="Price" name="additional_price_ir[]" value=""\n' +
        '                                                   autofocus>\n' +
        '                                        </div> ' +
        '<a data-id="0" class="remove_charecter_group"><i class="icon-minus"></i></a>' +
        '</div>';
    $(".product_charecters").append(char_block);
  });

  $(document).on("click", ".remove_charecter_group", function () {
    var id = parseInt($(this).attr("data-id"));
    if(id > 0) {
      $.ajax({
        type: "POST",
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '/admin/products/' + id,
        data: {"id": id, "_method": "DELETE"},
        success: function (msg) {

        }
      });
    }
    $(this).parent(".charecter_group").remove();
  })

  $(document).on('click', '.pdf_append_block_delete', function () {
    var div_class = $(this).parent(".open_sub_pdf_block");
    var id = $(this).attr("data-id");
    $.ajax({
      type: "POST",
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      url: '/admin/news_sub_page_pdf/delete_sub_pdf',
      data: {"id": id},
      success: function (msg) {
        div_class.remove();
      }
    });
  });
  $(document).on('click', '.delete_box_attr', function () {
    $(this).parent().remove();
  });
  $(document).on('click', '.package_service_add', function () {
    $.ajax({
      type: "POST",
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      url: '/admin/package_add',
      success: function (msg) {
        $(".page_pdf_plus_block").append(msg);
      }
    });
  });

  $(document).on('click', '.page_pdf_append_block_delete', function () {
    var div_class = $(this).parent(".open_sub_pages_with_block");
    var id = $(this).attr("data-id");
    $.ajax({
      type: "POST",
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      url: '/admin/sub_pages_with_pdf/delete_sub_pdf',
      data: {"id": id},
      success: function (msg) {
        console.log(msg);
        div_class.remove();
      }
    });
  });

  $(document).ready(function () {
    $(".checked_delete").click(function () {
      swal({
            title: "Are you sure you want to delete?",
            text: "Your will not be able to recover checked!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Delete",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: false
          },
          function (isConfirm) {
            if (isConfirm) {
              $(".checked_form").submit();
            } else {
              $(".sweet-overlay").css("display", "none");$(".sweet-alert").css("display", "none");
            }
          });
    });
  })

  $('.check_chars').keyup('paste', function(e) {
    var sub_title_length = $(this).val().length;
    var lang = $(this).data("lang");
    var count = 300 - sub_title_length;
    $(".slider_sub_title_length_" + lang).html("(" + count + " chars)");

  });


  $(document).ready(function () {
    $(document).on("click", ".close_choose_calendar_field", function () {
      $(".choose_calendar_field").css("display","none");
    })
  })

</script>
<script>
  $(document).ready(function () {
    $(document).on("click", ".add_column_info", function () {
      var data_day = $(this).attr("data-day");
      var data_index = $(this).attr("data-index");
      var data_table = $(this).attr("data-table");
      $(".save_calendar_field").attr("data-day",data_day);
      $(".save_calendar_field").attr("data-index",data_index);
      $(".save_calendar_field").attr("data-table",data_table);
      $(".choose_calendar_field").css("display","block");
    });

    $(document).on("click", ".edit_column_info", function () {
      var data_day = $(this).attr("data-day");
      var data_index = $(this).attr("data-index");
      var table = $(this).attr("data-table");
      $(".save_calendar_field").attr("data-day",data_day);
      $(".save_calendar_field").attr("data-index",data_index);
      $(".save_calendar_field").attr("data-table",table);

      var coachcatclass = "." + data_day + "_caochcat_" + data_index + '_'+table;
      var coachclass = "." + data_day + "_caoch_" + data_index + '_'+table;

      var curr_cat = $(coachcatclass).val();
      var curr_coach = $(coachclass).val();

      $(".choose_caochcat_pop").val(curr_cat);
      $(".choose_caoch_pop").val(curr_coach);

      $(".choose_calendar_field").css("display","block");
    });





    $(document).on("click", ".save_calendar_field", function () {
      var coachcat = $(".choose_caochcat_pop").val();
      var coach = $(".choose_caoch_pop").val();

      var data_cocat =  $(".choose_caochcat_pop option:selected").attr("data-cocat");
      var data_coname =  $(".choose_caoch_pop option:selected").attr("data-coname");

      var index =  $(this).attr("data-index");
      var day =  $(this).attr("data-day");
      var table =  $(this).attr("data-table");

      var coachcatclass = "." + day + "_caochcat_" + index + '_'+table;
      var coachclass = "." + day + "_caoch_" + index + '_'+table;

      var caochcat_info = "." + day + "_caochcat_info_" + index + '_'+table;
      var caoch_info = "." + day + "_caoch_info_" + index + '_'+table;

      $(coachclass).val(coach);
      $(coachcatclass).val(coachcat);

      $(caochcat_info).html(data_cocat);
      $(caoch_info).html(data_coname);
      $(".choose_calendar_field").css("display","none");
      $(".add_column_info_" + day+ "_" + index + "_" + table).addClass("edit_column_info");
      $(".add_column_info_" + day+ "_" + index + "_" + table).removeClass("add_column_info");
    });


    $(document).on("click", ".new_calendar_block_add", function () {

      var calendar_count = parseInt($(this).attr("data-calendar"));
      var next_calendar = calendar_count + 1;
      $(this).attr("data-calendar", next_calendar);
      var table = '<div class="form-group">\n' +
          '                                        <label>Calendar '+ (calendar_count +1) +'</label>\n' +
          '<div class="form-group lang_field lang_field_am active_field">\n' +
          ' <label>Հայ Calendar Title:</label>\n' +
          '<input type="text" placeholder="Calendar Title" class="form-control" id="meta_title" name="calendar_title_am[]"/>\n' +
          '</div>\n' +
          '<div class="form-group lang_field lang_field_ru hidden_field">\n' +
          ' <label>Рус Calendar Title:</label>\n' +
          '<input type="text" placeholder="Calendar Title" class="form-control" id="meta_title" name="calendar_title_ru[]"/>\n' +
          '</div>\n' +
          '<div class="form-group lang_field lang_field_en hidden_field">\n' +
          ' <label>Eng Calendar Title:</label>\n' +
          '<input type="text" placeholder="Calendar Title" class="form-control" id="meta_title" name="calendar_title_en[]"/>\n' +
          '</div>\n' +
          '                                        <table class="calendar_table calendar calendar_table_'+calendar_count+'">\n' +
          '                                            <tr>\n' +
          '                                                <th class="jamer_td">Ժամերը</th>\n' +
          '                                                <th>Երկ.</th>\n' +
          '                                                <th>Երեք.</th>\n' +
          '                                                <th>Չորեք.</th>\n' +
          '                                                <th>Հինգ.</th>\n' +
          '                                                <th>Ուրբ.</th>\n' +
          '                                                <th>Շաբ.</th>\n' +
          '                                                <th>Կիր.</th>\n' +
          '                                                <th class="delete_calendar_tr_td"></th>\n' +
          '                                            </tr>\n' +
          '                                                <tr>\n' +
          '                                                    <td class="jamer_td"><input type="text" name="jamer_'+ calendar_count +'[]" class="form-control"></td>\n' +
          '                                                    <td  data-day="erku" data-index="0" data-table="'+ calendar_count +'"  class="add_column_info add_column_info_erku_0_'+ calendar_count +'">\n' +
          '                                                        <small class="calendar_small erku_caochcat_info_0_'+ calendar_count +'"></small>\n' +
          '                                                        <small class="calendar_small erku_caoch_info_0_'+ calendar_count +'"></small>\n' +
          '                                                        <input type="hidden" name="erku_caochcat_'+ calendar_count +'[]" class="erku_caochcat_0_'+ calendar_count +'">\n' +
          '                                                        <input type="hidden" name="erku_caoch_'+ calendar_count +'[]" class="erku_caoch_0_'+ calendar_count +'">\n' +
          '                                                    </td>\n' +
          '                                                    <td  data-day="ereq" data-index="0" data-table="'+ calendar_count +'" class="add_column_info add_column_info_ereq_0_'+ calendar_count +'">\n' +
          '\n' +
          '                                                        <small class="calendar_small ereq_caochcat_info_0_'+ calendar_count +'"></small>\n' +
          '                                                        <small class="calendar_small ereq_caoch_info_0_'+ calendar_count +'"></small>\n' +
          '                                                        <input type="hidden" name="ereq_caochcat_'+ calendar_count +'[]" class="ereq_caochcat_0_'+ calendar_count +'">\n' +
          '                                                        <input type="hidden" name="ereq_caoch_'+ calendar_count +'[]" class="ereq_caoch_0_'+ calendar_count +'">\n' +
          '                                                    </td>\n' +
          '                                                    <td data-day="choreq" data-index="0" data-table="'+ calendar_count +'" class="add_column_info add_column_info_choreq_0_'+ calendar_count +'">\n' +
          '                                                        <small class="calendar_small choreq_caochcat_info_0_'+ calendar_count +'"></small>\n' +
          '                                                        <small class="calendar_small choreq_caoch_info_0_'+ calendar_count +'"></small>\n' +
          '                                                        <input type="hidden" name="choreq_caochcat_'+ calendar_count +'[]" class="choreq_caochcat_0_'+ calendar_count +'">\n' +
          '                                                        <input type="hidden" name="choreq_caoch_'+ calendar_count +'[]" class="choreq_caoch_0_'+ calendar_count +'">\n' +
          '                                                    </td>\n' +
          '                                                    <td data-day="hing" data-index="0" data-table="'+ calendar_count +'" class="add_column_info add_column_info_hing_0_'+ calendar_count +'">\n' +
          '                                                        <small class="calendar_small hing_caochcat_info_0_'+ calendar_count +'"></small>\n' +
          '                                                        <small class="calendar_small hing_caoch_info_0_'+ calendar_count +'"></small>\n' +
          '                                                        <input type="hidden" name="hing_caochcat_'+ calendar_count +'[]" class="hing_caochcat_0_'+ calendar_count +'">\n' +
          '                                                        <input type="hidden" name="hing_caoch_'+ calendar_count +'[]" class="hing_caoch_0_'+ calendar_count +'">\n' +
          '\n' +
          '                                                    </td>\n' +
          '                                                    <td data-day="urbat" data-index="0" data-table="'+ calendar_count +'" class="add_column_info add_column_info_urbat_0_'+ calendar_count +'">\n' +
          '                                                        <small class="calendar_small urbat_caochcat_info_0_'+ calendar_count +'"></small>\n' +
          '                                                        <small class="calendar_small urbat_caoch_info_0_'+ calendar_count +'"></small>\n' +
          '                                                        <input type="hidden" name="urbat_caochcat_'+ calendar_count +'[]" class="urbat_caochcat_0_'+ calendar_count +'">\n' +
          '                                                        <input type="hidden" name="urbat_caoch_'+ calendar_count +'[]" class="urbat_caoch_0_'+ calendar_count +'">\n' +
          '                                                    </td>\n' +
          '                                                    <td  data-day="shabat" data-index="0" data-table="'+ calendar_count +'" class="add_column_info add_column_info_shabat_0_'+ calendar_count +'">\n' +
          '                                                        <small class="calendar_small shabat_caochcat_info_0_'+ calendar_count +'"></small>\n' +
          '                                                        <small class="calendar_small shabat_caoch_info_0_'+ calendar_count +'"></small>\n' +
          '                                                        <input type="hidden" name="shabat_caochcat_'+ calendar_count +'[]" class="shabat_caochcat_0_'+ calendar_count +'">\n' +
          '                                                        <input type="hidden" name="shabat_caoch_'+ calendar_count +'[]" class="shabat_caoch_0_'+ calendar_count +'">\n' +
          '                                                    </td>\n' +
          '                                                    <td  data-day="kiraki" data-index="0" data-table="'+ calendar_count +'" class="add_column_info add_column_info_kiraki_0_'+ calendar_count +'">\n' +
          '                                                        <small class="calendar_small kiraki_caochcat_info_0_'+ calendar_count +'"></small>\n' +
          '                                                        <small class="calendar_small kiraki_caoch_info_0_'+ calendar_count +'"></small>\n' +
          '                                                        <input type="hidden" name="kiraki_caochcat_'+ calendar_count +'[]" class="kiraki_caochcat_0_'+ calendar_count +'">\n' +
          '                                                        <input type="hidden" name="kiraki_caoch_'+ calendar_count +'[]" class="kiraki_caoch_0_'+ calendar_count +'">\n' +
          '                                                    </td>\n' +
          '                                                    <td class="delete_calendar_tr_td"><a class="delete_calendar_tr"><i class="fa fa-times" aria-hidden="true"></i></a></td>\n' +
          '                                                </tr>\n' +
          '\n' +
          '                                        </table>\n' +
          '                                        <div data-rowcount="1" data-table="'+ calendar_count +'" class="add_calendar_row add_calendar_row_'+calendar_count+'" colspan="7">Ավելացնել</div>\n' +
          '                                    </div>'

      $(".calendars_block").append(table);
      $(".calendar_table_counts").val(calendar_count +1);
    })
  })
  $(document).ready(function () {
    $(document).on("click", ".add_calendar_row", function () {
      var rowcount = parseInt($(this).attr("data-rowcount"));

      var table = parseInt($(this).attr("data-table"));
      var nextrow = rowcount + 1;
      var class_tbl = '.calendar_table_' + table;
      var class_tbl_add = '.add_calendar_row_' + table;
      $.ajax({
        type: "POST",
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '/admin/pages/addCalendarRow',
        data: {"rowcount": rowcount,"table": table},
        success: function (msg) {
          $(class_tbl).append(msg)
          $(class_tbl_add).attr("data-rowcount", nextrow);
        }
      });
    })
  })
  $(document).on("click",".delete_calendar_tr_td",function () {
    $(this).parent().remove();
  })
  $(document).on("click",".show_hidden_info_block",function () {
    var row_id = $(this).attr("data-id");
    $(".hidden_info_block_" + row_id).slideToggle(200);
  })
  $(document).on("click",".btn_copy_past",function () {
    $(".additional_size_am").each(function (index, size) {
      var row = index + 1;
      $(".additional_size_ru_" + row).val(size.value);
      $(".additional_size_en_" + row).val(size.value);
      $(".additional_size_ir_" + row).val(size.value);

      var price = $(".additional_price_am_" + row).val();

      $(".additional_price_ru_" + row).val(price);
      $(".additional_price_en_" + row).val(price);
      $(".additional_price_ir_" + row).val(price);

    });
  })
</script>
</body>
</html>

