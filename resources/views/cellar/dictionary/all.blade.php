@extends('cellar.layouts.app')

@section('content')
    <div class="container services_block">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3>Add glossary</h3>
                    </div>
                    <div class="col--md-12">
                        <div class="about_form">
                            <form class="form-horizontal" method="POST" action="{{ url('/') }}/admin/dictionary">
                                {{ csrf_field() }}
                                <div class="glossary_col">
                                    <div class="form-group">
                                        <label>Short Code</label>
                                        @if(isset($edit_dictionary->short_code) && $edit_dictionary->short_code !='')
                                             <input type="text" class="form-control" value="{{ $edit_dictionary->short_code }}" name="short_code" required>
                                        @else
                                            <input type="text" class="form-control" name="short_code" required>
                                        @endif
                                    </div>
                                </div>

                                @foreach($languages as $language)
                                    <div class="glossary_col">
                                        <div class="form-group">
                                            <label>{{ $language->title }}</label>
                                            @if(isset($edit_dictionary->{"value_".$language->short} ) && $edit_dictionary->{"value_".$language->short} !='')
                                                <input type="text" class="form-control" value="{{ $edit_dictionary->{"value_".$language->short} }}" name="value_{{$language->short}}" required>
                                            @else
                                                <input type="text" class="form-control" name="value_{{$language->short}}" required>
                                            @endif

                                        </div>
                                    </div>
                                @endforeach
                                <div class="glossary_col">
                                    <div class="form-group">
                                        <input type="hidden" name="save_glossary" value="ok">
                                        @if(isset($edit_dictionary->id) && $edit_dictionary->id !='')
                                            <input type="hidden" name="dictionary_id" value="{{ $edit_dictionary->id }}">
                                        @else
                                            <input type="hidden" name="dictionary_id" value="">
                                        @endif
                                        <input type="submit" class="btn_1 green pull-right add_glossary_submit form-control" name="add_glossary_submit" value="Save">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <table class="glossary_table">
                            <tr>
                                <th>Short code</th>

                                @foreach($languages as $language)
                                    <th>{{ $language->title }}</th>
                                @endforeach

                                <th></th>
                            </tr>
                            @foreach($dictionaries as $glossary)
                                <tr>
                                    <td>{{$glossary->short_code}}</td>
                                    @foreach($languages as $language)
                                        <td>{{$glossary->{"value_".$language->short} }}</td>
                                    @endforeach
                                    <td>
                                        <div class="actions">
                                            <a href="{{ url('/') }}/admin/dictionary?id={{$glossary->id}}" class="edit_glossary btn_2">Edit</a>
                                            <a data-id="{{ $glossary->id }}" class="dictionary_del_btn dictionary_delete btn_delete btn-danger">
                                                <i class="icon-trash"></i>
                                                Delete
                                            </a>
                                        </div>

                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
