@extends('cellar.layouts.app')

@section('content')

    <div class="container">
        <div class="form_box">
            @if( count($languages) > 1)
                <ul class="languages_tabs">
                    @foreach($languages as $language)
                        <li data-tab="{{ $language->short }}" class="lang_tab {{ $language->short."_tab" }} @if($language->first == 1) {{"active_tab"}} @endif">
                            <p><span>{{ $language->title }}</span></p>
                        </li>
                    @endforeach
                </ul>
            @endif

            <div class="about_form">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form class="form-horizontal" method="POST" action="{{ url('/') }}/admin/delivery-cities" enctype='multipart/form-data'>
                    {{ csrf_field() }}
                    <div class="col-md-8 page_left">

                        @foreach($languages as $language)
                            <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                <label for="title">{{ $language->title }} City Title</label>
                                <input id="about_title" type="text" class="form-control" name="title_{{ $language->short }}" value="{{old("title_".$language->short)}}"
                                       autofocus>
                            </div>
                        @endforeach

                            <div class="article_cat author_fields form-group">
                                <label for="pr_code">Delivery Price</label>
                                <input id="pr_code" type="text" class="author-combobox-wrap article_author_name form-control" name="cost" value="{{old("cost")}}"
                                       autofocus>
                            </div>
                        <div class="col-md-12">
                            <button type="submit" class="btn_1 green pull-right">
                                Save
                            </button>
                        </div>
                    </div>


                </form>
            </div>
        </div>


    </div>
@endsection
