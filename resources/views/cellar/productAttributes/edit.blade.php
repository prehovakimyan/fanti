@extends('cellar.layouts.app')

@section('content')

    <div class="container">
        <div class="form_box">
            @if( count($languages) > 1)
                <ul class="languages_tabs">
                    @foreach($languages as $language)
                        <li data-tab="{{ $language->short }}" class="lang_tab {{ $language->short."_tab" }} @if($language->first == 1) {{"active_tab"}} @endif">
                            <p><span>{{ $language->title }}</span></p>
                        </li>
                    @endforeach
                </ul>
            @endif
            <div class="about_form">
                <form class="form-horizontal" method="POST" action="{{ url('/') }}/admin/product-attribute/{{ $attribute->id }}" enctype='multipart/form-data'>
                    {{method_field('PUT')}}
                    {{ csrf_field() }}
                    <div class="col-md-8 page_left">

                        @foreach($languages as $language)
                            <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                <label for="title">{{ $language->title }} Title</label>
                                <input id="about_title" type="text" class="form-control" name="title_{{ $language->short }}" value="{{ $attribute->{"title_".$language->short} }}"
                                       autofocus>
                            </div>
                        @endforeach
                            <div class="article_cat author_fields form-group">
                                <label>Category</label>
                                <div class="recomed_cats">
                                    @foreach($categories as $country)
                                        <div class="check_block_parent">
                                            <div class="check_block_child">
                                                <input class="pr_category" type="checkbox" @if(in_array($country->id ,$attribute->cats )) checked @endif value="{{ $country->id }}" name="category_id[]">
                                                <span>{{ $country->{"title_".$default_lang} }}</span>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>

                            </div>
                        <div class="col-md-12">
                            <input type="hidden" name="add_gallery" value="ok">
                            <button type="submit" class="btn_1 green pull-right">
                                Save
                            </button>
                        </div>
                    </div>


                </form>
            </div>
        </div>


    </div>
@endsection
