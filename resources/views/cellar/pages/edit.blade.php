@extends('cellar.layouts.app')
<link href="{{ asset('assets/css/admin/admin.css') }}" rel="stylesheet">
<link href="{{ asset('assets/css/admin/jquery.switch.css') }}" rel="stylesheet">
@if(isset($data['edit_successful']))
    <div class="edit_successful">
        <p>The page updated</p>
    </div>
@endif

@section('content')

    <div class="container edit_container">

            <div class="form_box">
                <div class="page_title_box">
                    <h4>@if(!empty($page->parent_title)) {!! $page->parent_title !!} | @endif {{ $page->{"title_".$default_lang} }}</h4>
                </div>
                @if( count($languages) > 1)
                <ul class="languages_tabs">
                    @foreach($languages as $language)
                        <li data-tab="{{ $language->short }}" class="lang_tab {{ $language->short."_tab" }} @if($language->first == 1) {{"active_tab"}} @endif">
                            <p><span>{{ $language->title }}</span></p>
                        </li>
                    @endforeach
                </ul>
                @endif
                <div class="about_form">

                        <form class="form-horizontal" method="POST" action="{{ url('/') }}/admin/pages/{{ $page->id }}" enctype="multipart/form-data">
                        {{method_field('PUT')}}
                        {{ csrf_field() }}
                        <!--Armenian-->
                                <div class="col-md-9 page_left">
                                    @foreach($languages as $language)
                                        <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                            <label for="title">{{ $language->title }} Title</label>
                                            <input id="about_title" type="text" class="form-control" name="title_{{ $language->short }}" value="{{ $page->{"title_".$language->short} }}"
                                                   autofocus>
                                        </div>

                                        <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                            <label for="Description">{{ $language->title }} Short Description</label>
                                            <textarea class="form-control short_desc" name="short_desc_{{ $language->short }}">{{ $page->{"short_desc_".$language->short} }}</textarea>
                                        </div>
                                        <div  class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                            <label for="Description">{{ $language->title }} Description</label>
                                            <textarea class="form-control @if($language->short == 'ir') description_ir @else description @endif" name="description_{{ $language->short }}">{{ $page->{"description_".$language->short} }}</textarea>
                                        </div>

                                    @endforeach

                                        <div class="row page_image">
                                            <div class="company_logo">
                                                <div class="">
                                                    <label for="Description">Image</label>
                                                </div>
                                                <div class="avatar_img">
                                                    <div class="file-upload btn btn_1 green">
                                                        <span>Choose image</span>
                                                        <input type="file" name="image" id="uploadBtnBackImage" class="image upload"
                                                               onchange="readURLBackImage(this)">
                                                    </div>
                                                </div>
                                                <div class="profile-image-preview_back_image @if($page->image !='') {{ "active" }} @endif">
                                                    <div class="ct-media--left">
                                                        <a>
                                                            <img id="uploadPreviewBackImage"  src="@if($page->image !='') {{ $page->image }} @endif">
                                                        </a>
                                                    </div>
                                                    <input type="hidden" name="old_image" class="old_image" value="{{ $page->image }}">
                                                    <div class="ct-media--content">
                                                        <span></span>
                                                        <a class="cross" onclick="deleteimgBackImage();" style="cursor:pointer;">
                                                            <i class="fa fa-trash-o"></i> Delete
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>


                                    <div class="form-group">
                                        <label>Slug</label>
                                        <input type="text" class="form-control slug_input" name="slug" value="{{ $page->slug }}">
                                    </div>


                                    <div class="form-group meta_block">
                                        <div class="open_close_meta">
                                            <p>Meta <i class="icon-arrow-combo"></i></p>
                                        </div>
                                        <div class="meta_box">
                                            <div class="col-md-6 col-sm-6 pull-left">
                                                @foreach($languages as $language)
                                                    <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                                        <label>{{ $language->title }} Meta Title:</label>
                                                        <input type="text" placeholder='Meta Title' class="form-control"
                                                               value='{{ $page->{"meta_title_".$language->short} }}' id='meta_title' name='meta_title_{{ $language->short }}'/>
                                                    </div>
                                                @endforeach
                                            </div>
                                            <div class="col-md-6 col-sm-6 pull-left">
                                                @foreach($languages as $language)
                                                    <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                                        <label>{{ $language->title }} Meta Keyword:</label>
                                                        <input type="text" placeholder='Meta Keyword' id='meta_key' name='meta_key_{{ $language->short }}'
                                                               class="form-control"
                                                               value="{{ $page->{"meta_key_".$language->short} }}"/>
                                                    </div>
                                                @endforeach
                                            </div>
                                            <div class="col-md-12 col-sm-12 pull-left">
                                                @foreach($languages as $language)
                                                    <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                                        <label>{{ $language->title }} Meta Description</label>
                                                        <textarea id="meta_desc" class="form-control" name="meta_desc_{{ $language->short }}">{{ $page->{"meta_desc_".$language->short} }}</textarea>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>

                                    </div>
                                    <input type="hidden" name="edit_page" value="ok">
                                    <button type="submit" class="btn_1 save_btn green pull-right">
                                        Save
                                    </button>
                                </div>



                        </form>
                    </div>
            </div>


    </div>
@endsection





