@extends('admin.layouts.page')
<link href="{{ asset('public/css/admin/admin.css') }}" rel="stylesheet">
<link href="{{ asset('public/css/admin/jquery.switch.css') }}" rel="stylesheet">
@section('content')
    <div class="container services_block">
        <div class="col-md-12">
            <div class="page_title_box parent_page_title">
                <div class="col-md-8">
                <h4>{{ $page->title }}</h4>
                <div class="page_actions_block">
                    @if($page->config_cat == 1)
                        <a href="{{ url('/') }}/admin/news/create?parent={{ $page->id }}" class="sub_page_add btn_2">
                            <i class="icon-docs"></i>
                            Add
                        </a>
                    @elseif($page->sub_pages == 1)
                        <a href="{{ url('/') }}/admin/sub_pages/{{ $page->id }}/create" class="sub_page_add btn_2">
                            <i class="icon-docs"></i>
                            Add
                        </a>
                    @elseif($page->config_project == 1)
                        <a href="{{ url('/') }}/admin/sub_pages_with_pdf/{{ $page->id }}/create" class="sub_page_add btn_2">
                            <i class="icon-docs"></i>
                            Add
                        </a>
                    @endif
                    <a href="{{ url('/') }}/admin/pages/{{ $page->id }}/edit" class="parent_page_edit btn_2">
                        <i class="icon-edit-3"></i>
                        Edit
                    </a>
                </div>
                </div>
                <div class="col-md-4">
                    @if($page->config_cat == 1)
                        <a href="{{ url('/') }}/admin/categories?parent={{ $page->id }}" class="categories_add btn_2">
                            <i class="icon-th-list-3"></i>
                            Categories
                        </a>
                    @endif
                        @if($page->config_project == 1)
                            <a href="{{ url('/') }}/admin/project_categories?parent={{ $page->id }}" class="categories_add btn_2">
                                <i class="icon-th-list-3"></i>
                                Categories
                            </a>
                        @endif
                        @if($page->page_with_pdf == 1)
                            <div class="page_additional_block">
                                <a href="{{ url('/') }}/admin/sub_pages_with_pdf/{{ $page->id }}/create" class="sub_pages_with_pdf btn_2">
                                    <i class="icon-folder-empty"></i>
                                    Page | pdf
                                </a>
                            </div>

                        @endif
                        <div class="page_additional_block">
                            @if(isset($page_gallery) && $page_gallery->id > 0)
                                <a href="{{ url('/') }}/admin/sub_page_gallery/edit/5" class="parent_page_edit btn_2">
                                    <i class="icon-edit-3"></i>
                                    Gallery
                                </a>
                                @else
                                <a style="    width: 100px;" href="{{ url('/') }}/admin/sub_page_gallery/add/{{ $page->id }}" class="gallery_pdf_btn sub_pages_with_pdf btn_2">
                                    <i class="icon-picture-1"></i>
                                    Gallery
                                </a>
                                @endif


                        </div>

                </div>
            </div>
        </div>

        <div class="col-md-12 page_show_content">


            <form class="checked_form" action="{{ url('/') }}/admin/checked_delete" method="post">
                {{ csrf_field() }}

            <div class="select_all_block col-md-8">
                <input type="checkbox" class="checked_all" name="checked_all" value="1">
                <label>Check all</label>
                <button class="checked_delete" type="button" disabled><i class="icon-trash"></i></button>
            </div>
            @if(!empty($page->sub_pages_with_pdf))
                    <div class="child_pages">
                        @foreach($page->sub_pages_with_pdf as $sub_page_with_pdf)
                            <div class="col-md-8">
                                <div class="page_title_box child_page_title">
                                    <div class="check_one">
                                        <input type="checkbox" class="checked_one_input" name="checked_one_input[]" value="{{ $sub_page_with_pdf->id }}">
                                        <input type="checkbox" class="hidden check_type" name="check_type[]" value="sub_page_with_pdf" >
                                    </div>
                                    <div class="page_hidden">
                                        <i class="icon-folder-empty"></i>
                                        @if($sub_page_with_pdf->hidden == 0)
                                            <a alt="Show" title="Show" class="visible" data-hidden="1" data-id="{{ $sub_page_with_pdf->id }}" data-type="sub_page_with_pdf">
                                                <i class="icon-eye"></i>
                                            </a>

                                        @else
                                            <a alt="Hide" title="Hide" class="visible" data-hidden="0" data-id="{{ $sub_page_with_pdf->id }}" data-type="sub_page_with_pdf">
                                                <i class="icon-eye-off"></i>
                                            </a>
                                        @endif
                                    </div>
                                    <p>{{ $sub_page_with_pdf->title }}</p>
                                    <div class="page_actions_block">

                                        <a href="{{ url('/') }}/admin/sub_pages_with_pdf/{{$page->id}}/edit/{{ $sub_page_with_pdf->id }}" class="parent_page_edit btn_2">
                                            <i class="icon-edit-3"></i>
                                            Edit
                                        </a>
                                        <a data-id="{{ $sub_page_with_pdf->id }}" class="page_del_btn page_with_pdf_delete btn_delete btn-danger">
                                            <i class="icon-trash"></i>
                                            Delete
                                        </a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @endif
            @if(!empty($page->sub_pages_with_projects))
                    <div class="child_pages">
                        @foreach($page->sub_pages_with_projects as $sub_pages_with_project)
                            <div class="col-md-8">
                                <div class="page_title_box child_page_title">
                                    <div class="check_one">
                                        <input type="checkbox" class="checked_one_input" name="checked_one_input[]" value="{{ $sub_pages_with_project->id }}">
                                        <input type="checkbox" class="hidden check_type" name="check_type[]" value="sub_page_with_pdf" >
                                    </div>
                                    <div class="page_hidden">
                                        <i class="icon-folder-empty"></i>
                                        @if($sub_pages_with_project->hidden == 0)
                                            <a alt="Show" title="Show" class="visible" data-hidden="1" data-id="{{ $sub_pages_with_project->id }}" data-type="sub_page_with_pdf">
                                                <i class="icon-eye"></i>
                                            </a>

                                        @else
                                            <a alt="Hide" title="Hide" class="visible" data-hidden="0" data-id="{{ $sub_pages_with_project->id }}" data-type="sub_page_with_pdf">
                                                <i class="icon-eye-off"></i>
                                            </a>
                                        @endif
                                    </div>
                                    <p>{{ $sub_pages_with_project->title }}</p>
                                    <div class="page_actions_block">

                                        <a href="{{ url('/') }}/admin/sub_pages_with_pdf/{{$page->id}}/edit/{{ $sub_pages_with_project->id }}" class="parent_page_edit btn_2">
                                            <i class="icon-edit-3"></i>
                                            Edit
                                        </a>
                                        <a data-id="{{ $sub_pages_with_project->id }}" class="page_del_btn page_with_pdf_delete btn_delete btn-danger">
                                            <i class="icon-trash"></i>
                                            Delete
                                        </a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @endif
            @if(!empty($page->child_pages))
                <div class="child_pages">
                    @foreach($page->child_pages as $child_page)
                        <div class="col-md-8">
                            <div class="page_title_box child_page_title">
                                <div class="check_one">
                                    <input type="checkbox" class="checked_one_input" name="checked_one_input[]" value="{{ $child_page->id }}">
                                    <input type="checkbox" class="hidden check_type" name="check_type[]" value="page" >
                                </div>
                                <div class="page_hidden">
                                    @if($child_page->hidden == 0)
                                        <a alt="Show" title="Show" class="visible" data-hidden="1" data-id="{{ $child_page->id }}" data-type="page">
                                            <i class="icon-eye"></i>
                                        </a>

                                    @else
                                        <a alt="Hide" title="Hide" class="visible" data-hidden="0" data-id="{{ $child_page->id }}" data-type="page">
                                            <i class="icon-eye-off"></i>
                                        </a>
                                    @endif
                                </div>
                                <p>{{ $child_page->title }}</p>
                                <div class="page_actions_block">

                                    <a href="{{ url('/') }}/admin/pages/{{ $child_page->id }}/edit" class="parent_page_edit btn_2">
                                        <i class="icon-edit-3"></i>
                                        Edit
                                    </a>
                                    <a data-id="{{ $child_page->id }}" class="page_del_btn page_delete btn_delete btn-danger">
                                        <i class="icon-trash"></i>
                                        Delete
                                    </a>
                                </div>
                            </div>

                            @if(isset($child_page->child_page_pdfs) && !empty($child_page->child_page_pdfs))
                                <div class="child_page_pdfs">
                                    @foreach($child_page->child_page_pdfs as $child_page_pdf)
                                        <div class="page_title_box child_page_title">
                                            <div class="check_one">
                                                <input type="checkbox" class="checked_one_input" name="checked_one_input[]" value="{{ $child_page_pdf->id }}">
                                                <input type="checkbox" class="hidden check_type" name="check_type[]" value="pdf" >
                                            </div>
                                            <div class="page_hidden">
                                                <i class="icon-doc-add"></i>
                                                @if($child_page_pdf->hidden == 0)
                                                    <a alt="Show" title="Show" class="visible" data-hidden="1" data-id="{{ $child_page_pdf->id }}" data-type="page_pdf">
                                                        <i class="icon-eye"></i>
                                                    </a>
                                                @else
                                                    <a alt="Hide" title="Hide" class="visible" data-hidden="0" data-id="{{ $child_page_pdf->id }}" data-type="page_pdf">
                                                        <i class="icon-eye-off"></i>
                                                    </a>
                                                @endif
                                            </div>
                                            <p>{{ $child_page_pdf->title }}</p>
                                            <div class="page_actions_block">
                                                <a href="{{ url('/') }}/admin/sub_pdf/{{ $child_page->id }}/edit/{{ $child_page_pdf->id }}" class="parent_page_edit btn_2">
                                                    <i class="icon-edit-3"></i>
                                                    Edit
                                                </a>
                                                <a data-id="{{ $child_page_pdf->id }}" class="page_del_btn page_pdf_delete btn_delete btn-danger">
                                                    <i class="icon-trash"></i>
                                                    Delete
                                                </a>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>

                            @endif
                            @if(isset($child_page->child_page_galleries) && !empty($child_page->child_page_galleries))
                                <div class="child_page_pdfs">
                                    @foreach($child_page->child_page_galleries as $child_page_gallery)
                                        <div class="page_title_box child_page_title">
                                            <div class="check_one">
                                                <input type="checkbox" class="checked_one_input" name="checked_one_input[]" value="{{ $child_page_gallery->id }}">
                                                <input type="checkbox" class="check_type hidden" name="check_type[]" value="gallery" >
                                            </div>
                                            <div class="page_hidden">
                                                <i class="icon-picture-1"></i>
                                                @if($child_page_gallery->hidden == 0)
                                                    <a alt="Show" title="Show" class="visible" data-hidden="1" data-id="{{ $child_page_gallery->id }}" data-type="page_gallery">
                                                        <i class="icon-eye"></i>
                                                    </a>

                                                @else
                                                    <a alt="Hide" title="Hide" class="visible" data-hidden="0" data-id="{{ $child_page_gallery->id }}" data-type="page_gallery">
                                                        <i class="icon-eye-off"></i>
                                                    </a>
                                                @endif
                                            </div>
                                            <p>{{ $child_page_gallery->title }}</p>
                                            <div class="page_actions_block">
                                                <a href="{{ url('/') }}/admin/page_gallery/edit/{{ $child_page_gallery->id }}" class="parent_page_edit btn_2">
                                                    <i class="icon-edit-3"></i>
                                                    Edit
                                                </a>
                                                <a data-count="{{ $child_page->page_gallery_count }}" data-id="{{ $child_page_gallery->id }}" class="page_del_btn page_gallery_delete btn_delete btn-danger">
                                                    <i class="icon-trash"></i>
                                                    Delete
                                                </a>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            @endif
                            @if(isset($child_page->child_sub_pages_with_pdf) && !empty($child_page->child_sub_pages_with_pdf))
                                <div class="child_page_pdfs">
                                    @foreach($child_page->child_sub_pages_with_pdf as $child_sub_page_with_pdf)
                                        <div class="page_title_box child_page_title">
                                            <div class="check_one">
                                                <input type="checkbox" class="checked_one_input" name="checked_one_input[]" value="{{ $child_sub_page_with_pdf->id }}">
                                                <input type="checkbox" class="check_type hidden" name="check_type[]" value="sub_page_with_pdf" >
                                            </div>
                                            <div class="page_hidden">
                                                <i class="icon-folder-empty"></i>
                                                @if($child_sub_page_with_pdf->hidden == 0)
                                                    <a alt="Show" title="Show" class="visible" data-hidden="1" data-id="{{ $child_sub_page_with_pdf->id }}" data-type="sub_page_with_pdf">
                                                        <i class="icon-eye"></i>
                                                    </a>

                                                @else
                                                    <a alt="Hide" title="Hide" class="visible" data-hidden="0" data-id="{{ $child_sub_page_with_pdf->id }}" data-type="sub_page_with_pdf">
                                                        <i class="icon-eye-off"></i>
                                                    </a>
                                                @endif
                                            </div>
                                            <p>{{ $child_sub_page_with_pdf->title }}</p>
                                            <div class="page_actions_block">
                                                    <div class="sub_page_actions_block">
                                                        <a style="    width: 100px;" href="{{ url('/') }}/admin/sub_pages_with_pdf/{{ $child_page->id }}/create?sub_id={{ $child_sub_page_with_pdf->id }}" class="sub_pages_with_pdf btn_2">
                                                            <i class="icon-edit-3"></i>
                                                            Folders
                                                        </a>
                                                        <a style="    width: 100px;" href="{{ url('/') }}/admin/sub_page_gallery/add/{{ $child_sub_page_with_pdf->id }}" class="gallery_pdf_btn sub_pages_with_pdf btn_2">
                                                            <i class="icon-picture-1"></i>
                                                            Gallery
                                                        </a>

                                                    </div>

                                                <a href="{{ url('/') }}/admin/sub_pages_with_pdf/{{ $child_page->id }}/edit/{{ $child_sub_page_with_pdf->id }}" class="parent_page_edit btn_2">
                                                    <i class="icon-edit-3"></i>
                                                    Edit
                                                </a>
                                                <a data-id="{{ $child_sub_page_with_pdf->id }}" class="page_del_btn page_with_pdf_delete btn_delete btn-danger">
                                                    <i class="icon-trash"></i>
                                                    Delete
                                                </a>
                                            </div>
                                        </div>

                                        @if(isset($child_sub_page_with_pdf->pdf_folders) && !empty($child_sub_page_with_pdf->pdf_folders))

                                        <div class="child_page_pdfs">
                                            @foreach($child_sub_page_with_pdf->pdf_folders as $pdf_folder)
                                                <div class="page_title_box child_page_title">
                                                    <div class="check_one">
                                                        <input type="checkbox" class="checked_one_input" name="checked_one_input[]" value="{{ $pdf_folder->id }}">
                                                        <input type="checkbox" class="check_type hidden" name="check_type[]" value="sub_page_with_pdf" >
                                                    </div>
                                                    <div class="page_hidden">
                                                        <i class="icon-folder-empty"></i>
                                                        @if($child_sub_page_with_pdf->hidden == 0)
                                                            <a alt="Show" title="Show" class="visible" data-hidden="1" data-id="{{ $pdf_folder->id }}" data-type="sub_page_with_pdf">
                                                                <i class="icon-eye"></i>
                                                            </a>

                                                        @else
                                                            <a alt="Hide" title="Hide" class="visible" data-hidden="0" data-id="{{ $pdf_folder->id }}" data-type="sub_page_with_pdf">
                                                                <i class="icon-eye-off"></i>
                                                            </a>
                                                        @endif
                                                    </div>
                                                    <p>{{ $pdf_folder->title }}</p>
                                                    <div class="page_actions_block">
                                                        <a href="{{ url('/') }}/admin/sub_pages_with_pdf/{{ $child_page->id }}/edit/{{ $pdf_folder->id }}" class="parent_page_edit btn_2">
                                                            <i class="icon-edit-3"></i>
                                                            Edit
                                                        </a>
                                                        <a data-id="{{ $pdf_folder->id }}" class="page_del_btn page_with_pdf_delete btn_delete btn-danger">
                                                            <i class="icon-trash"></i>
                                                            Delete
                                                        </a>
                                                    </div>
                                                </div>


                                            @endforeach
                                        </div>
                                        @endif




                                        @if(isset($child_sub_page_with_pdf->gallery_folders) && !empty($child_sub_page_with_pdf->gallery_folders))

                                            <div class="child_page_pdfs">
                                                @foreach($child_sub_page_with_pdf->gallery_folders as $pdf_folder)
                                                    <div class="page_title_box child_page_title">
                                                        <div class="check_one">
                                                            <input type="checkbox" class="checked_one_input" name="checked_one_input[]" value="{{ $pdf_folder->id }}">
                                                            <input type="checkbox" class="check_type hidden" name="check_type[]" value="page_gallery" >
                                                        </div>
                                                        <div class="page_hidden">
                                                            <i class="icon-picture-1"></i>
                                                            @if($child_sub_page_with_pdf->hidden == 0)
                                                                <a alt="Show" title="Show" class="visible" data-hidden="1" data-id="{{ $pdf_folder->id }}" data-type="page_gallery">
                                                                    <i class="icon-eye"></i>
                                                                </a>

                                                            @else
                                                                <a alt="Hide" title="Hide" class="visible" data-hidden="0" data-id="{{ $pdf_folder->id }}" data-type="page_gallery">
                                                                    <i class="icon-eye-off"></i>
                                                                </a>
                                                            @endif
                                                        </div>
                                                        <p>{{ $pdf_folder->title }}</p>
                                                        <div class="page_actions_block">
                                                            <a href="{{ url('/') }}/admin/sub_page_gallery/edit/{{ $pdf_folder->id }}" class="parent_page_edit btn_2">
                                                                <i class="icon-edit-3"></i>
                                                                Edit
                                                            </a>
                                                            <a data-id="{{ $pdf_folder->id }}" class="page_del_btn page_gallery_delete btn_delete btn-danger">
                                                                <i class="icon-trash"></i>
                                                                Delete
                                                            </a>
                                                        </div>
                                                    </div>


                                                @endforeach
                                            </div>
                                        @endif


                                    @endforeach
                                </div>

                            @endif

                        </div>
                    <div class="col-md-4">
                        <div class="page_additional_block">
                           {{-- @if($child_page->pdf_uploder == 1)
                                <a href="{{ url('/') }}/admin/sub_pdf/{{ $child_page->id }}/create" class="page_pdf_btn btn_2">
                                    <i class="icon-doc-add"></i>Pdf</a>
                            @endif
                            @if($child_page->gallery == 1)
                                <a  href="{{ url('/') }}/admin/page_gallery/add/{{ $child_page->id }}" class="gallery_pdf_btn btn_2">
                                    <i class="icon-picture-1"></i>
                                    Gallery
                                </a>
                            @endif--}}
                            @if($child_page->page_with_pdf == 1)
                                    <a href="{{ url('/') }}/admin/sub_pages_with_pdf/{{ $child_page->id }}/create" class="sub_pages_with_pdf btn_2">
                                        <i class="icon-folder-empty"></i>
                                        Sub page
                                    </a>
                            @endif
                        </div>
                    </div>
                       @endforeach
                </div>
            @endif

            @if(!empty($page->newses))
                <div class="child_pages">
                    @foreach($page->newses as $news)
                        <div class="col-md-8">
                            <div class="page_title_box child_page_title">
                                <div class="check_one">
                                    <input type="checkbox" class="checked_one_input" name="checked_one_input[]" value="{{ $news->id }}">
                                    <input type="checkbox" class="hidden check_type" name="check_type[]" value="news" >
                                </div>
                                <div class="page_hidden">
                                    @if($news->hidden == 0)
                                        <a alt="Show" title="Show" class="visible" data-hidden="1" data-id="{{ $news->id }}" data-type="news">
                                            <i class="icon-eye"></i>
                                        </a>

                                    @else
                                        <a alt="Hide" title="Hide" class="visible" data-hidden="0" data-id="{{ $news->id }}" data-type="news">
                                            <i class="icon-eye-off"></i>
                                        </a>
                                    @endif

                                    @if($news->cat_id == 14)
                                            <i class="fa fa-video-camera" aria-hidden="true"></i>
                                    @endif
                                </div>
                                <div class="news_img">
                                    @if($news->image !='')
                                        <img src="{{ url('/') }}/uploads/news/{{$news->image}}" >
                                    @else
                                        <div class="date">
                                            <span class="month">{{ date("M", strtotime($news->created_at)) }}</span>
                                            <span class="day"><strong>{{ date("d", strtotime($news->created_at)) }}</strong>{{ date("D", strtotime($news->created_at)) }}</span>
                                        </div>
                                    @endif
                                </div>
                                <p class="news_title">{{ $news->title }}</p>
                                <div class="page_actions_block">
                                    <a href="{{ url('/') }}/admin/news/{{ $news->id }}/edit" class="parent_page_edit btn_2">
                                        <i class="icon-edit-3"></i>
                                        Edit
                                    </a>
                                    <a data-id="{{ $news->id }}" class="page_del_btn news_delete btn_delete btn-danger">
                                        <i class="icon-trash"></i>
                                        Delete
                                    </a>
                                </div>
                            </div>
                            @if($news->pdf_config == 1)
                                @if(!empty($news->pdf))
                                <div class="child_page_pdfs">
                                    @foreach($news->pdf as $news_sub_pdf)
                                        <div class="page_title_box child_page_title">
                                            <div class="check_one">
                                                <input type="checkbox" class="checked_one_input" name="checked_one_input[]" value="{{ $news_sub_pdf->id }}">
                                                <input type="checkbox" class="hidden check_type" name="check_type[]" value="pdf" >
                                            </div>
                                            <div class="page_hidden">
                                                <i class="icon-doc-add"></i>
                                                @if($news_sub_pdf->hidden == 0)
                                                    <a alt="Show" title="Show" class="visible" data-hidden="1" data-id="{{ $news_sub_pdf->id }}" data-type="news_pdf">
                                                        <i class="icon-eye"></i>
                                                    </a>
                                                @else
                                                    <a alt="Hide" title="Hide" class="visible" data-hidden="0" data-id="{{ $news_sub_pdf->id }}" data-type="news_pdf">
                                                        <i class="icon-eye-off"></i>
                                                    </a>
                                                @endif
                                            </div>
                                            <p>{{ $news_sub_pdf->title }}</p>
                                            <div class="page_actions_block">
                                                <a href="{{ url('/') }}/admin/news_pdf/{{ $news->id }}/edit/{{ $news_sub_pdf->id }}" class="parent_page_edit btn_2">
                                                    <i class="icon-edit-3"></i>
                                                    Edit
                                                </a>
                                                <a data-id="{{ $news_sub_pdf->id }}" class="page_del_btn news_pdf_delete btn_delete btn-danger">
                                                    <i class="icon-trash"></i>
                                                    Delete
                                                </a>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            @endif
                            @endif

                            @if($news->gallery_config == 1)
                                @if(!empty($news->galleries))
                                <div class="child_page_pdfs">
                                    @foreach($news->galleries as $news_sub_gallery)
                                        <div class="page_title_box child_page_title">
                                            <div class="check_one">
                                                <input type="checkbox" class="checked_one_input" name="checked_one_input[]" value="{{ $news_sub_gallery->id }}">
                                                <input type="checkbox" class="hidden check_type" name="check_type[]" value="pdf" >
                                            </div>
                                            <div class="page_hidden">
                                                <i class="icon-picture-1"></i>
                                                @if($news_sub_gallery->hidden == 0)
                                                    <a alt="Show" title="Show" class="visible" data-hidden="1" data-id="{{ $news_sub_gallery->id }}" data-type="news_gallery">
                                                        <i class="icon-eye"></i>
                                                    </a>
                                                @else
                                                    <a alt="Hide" title="Hide" class="visible" data-hidden="0" data-id="{{ $news_sub_gallery->id }}" data-type="news_gallery">
                                                        <i class="icon-eye-off"></i>
                                                    </a>
                                                @endif
                                            </div>
                                            <p>{{ $news_sub_gallery->{"title_".$default_lang} }}</p>
                                            <div class="page_actions_block">
                                                <a href="{{ url('/') }}/admin/news_gallery/edit/{{ $news_sub_gallery->id }}" class="parent_page_edit btn_2">
                                                    <i class="icon-edit-3"></i>
                                                    Edit
                                                </a>
                                                <a data-count = "{{ $news_sub_gallery->count_news_sub_page_pdf }}" data-id="{{ $news_sub_gallery->id }}" class="page_del_btn news_gallery_delete btn_delete btn-danger">
                                                    <i class="icon-trash"></i>
                                                    Delete
                                                </a>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            @endif
                            @endif

                            @if($news->page_pdf  == 1)
                                @if(!empty($news->news_sub_page))
                                    <div class="child_page_pdfs">
                                    @foreach($news->news_sub_page as $news_sub_page)
                                        <div class="page_title_box child_page_title">
                                            <div class="check_one">
                                                <input type="checkbox" class="checked_one_input" name="checked_one_input[]" value="{{ $news_sub_page->id }}">
                                                <input type="checkbox" class="hidden check_type" name="check_type[]" value="news_sub_page" >
                                            </div>
                                            <div class="page_hidden">
                                                <i class="icon-folder-empty"></i>
                                                @if($news_sub_page->hidden == 0)
                                                    <a alt="Show" title="Show" class="visible" data-hidden="1" data-id="{{ $news_sub_page->id }}" data-type="news_sub_page">
                                                        <i class="icon-eye"></i>
                                                    </a>
                                                @else
                                                    <a alt="Hide" title="Hide" class="visible" data-hidden="0" data-id="{{ $news_sub_page->id }}" data-type="news_sub_page">
                                                        <i class="icon-eye-off"></i>
                                                    </a>
                                                @endif
                                            </div>
                                            <p>{{ $news_sub_page->title }}</p>
                                            <div class="page_actions_block">
                                                <a href="{{ url('/') }}/admin/news_sub_page_pdf/{{ $news->id }}/edit/{{ $news_sub_page->id }}" class="parent_page_edit btn_2">
                                                    <i class="icon-edit-3"></i>
                                                    Edit
                                                </a>
                                                <a data-count = "{{ $news_sub_page->count_news_sub_page_pdf }}" data-id="{{ $news_sub_page->id }}" class="page_del_btn news_sub_delete btn_delete btn-danger">
                                                    <i class="icon-trash"></i>
                                                    Delete
                                                </a>
                                            </div>
                                        </div>
                                    @endforeach
                                    </div>
                                @endif
                            @endif
                        </div>
                        <div class="col-md-4">
                            <div class="page_additional_block">
                                @if($news->page_pdf  == 1)
                                <a style="width: 120px" href="{{ url('/') }}/admin/news_sub_page_pdf/{{ $news->id }}/create" class="gallery_pdf_btn btn_2">
                                    <i class="icon-folder-empty"></i>
                                    Page | pdf
                                </a>
                                @endif
                                @if($news->pdf_config == 1)
                                    <a href="{{ url('/') }}/admin/news_pdf/{{ $news->id }}/create" class="page_pdf_btn btn_2">
                                        <i class="icon-doc-add"></i>Pdf</a>
                                @endif
                                @if($news->gallery_config == 1)
                                    <a href="{{ url('/') }}/admin/news_gallery/add/{{ $news->id }}" class="gallery_pdf_btn btn_2">
                                        <i class="icon-picture-1"></i>
                                        Gallery
                                    </a>
                                @endif

                            </div>
                        </div>
                    @endforeach
                </div>
            @endif

            </form>
        </div>

    </div>

@endsection