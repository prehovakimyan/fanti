@extends('cellar.layouts.app')
<link href="{{ asset('assets/css/admin/admin.css') }}" rel="stylesheet">
<link href="{{ asset('assets/css/admin/jquery.switch.css') }}" rel="stylesheet">
@section('content')

    <div class="container services_block">
        <div class="col-md-8 page_show_content">
            <div class="page_title_box parent_page_title">
                <h4>Product Categories</h4>
                <div class="page_actions_block">
                    <a href="{{ url('/') }}/admin/product-category/create" class="sub_page_add btn_2">
                        <i class="icon-docs"></i>
                        Add
                    </a>
                </div>
            </div>
            <div class="child_pages">
                @foreach($categories as $category)
                    <div class="page_title_box child_page_title">
                        @if($category->image)
                            <div class="project_img_box col-md-2 col-sm-2">
                                <img src="{{$category->image}}" >
                            </div>
                        @endif
                        <p>{{ $category->title }}</p>
                        <div class="page_actions_block">
                            <a href="{{ url('/') }}/admin/product-category/{{ $category->id }}/edit" class="parent_page_edit btn_2">
                                <i class="icon-edit-3"></i>
                                Edit
                            </a>
                            <a data-id="{{ $category->id }}" data-href="product-category" class="page_del_btn delete_from_db btn_delete btn-danger">
                                <i class="icon-trash"></i>
                                Delete
                            </a>
                        </div>

                    </div>
                @endforeach
            </div>
        </div>
    </div>




    </div>
@endsection
