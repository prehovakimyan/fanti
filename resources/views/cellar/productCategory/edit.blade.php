@extends('cellar.layouts.app')

@section('content')

    <div class="container">
        <div class="form_box">
            @if( count($languages) > 1)
                <ul class="languages_tabs">
                    @foreach($languages as $language)
                        <li data-tab="{{ $language->short }}" class="lang_tab {{ $language->short."_tab" }} @if($language->first == 1) {{"active_tab"}} @endif">
                            <p><span>{{ $language->title }}</span></p>
                        </li>
                    @endforeach
                </ul>
            @endif
            <div class="about_form">
                <form class="form-horizontal" method="POST" action="{{ url('/') }}/admin/product-category/{{ $category->id }}" enctype='multipart/form-data'>
                    {{method_field('PUT')}}
                    {{ csrf_field() }}
                    <div class="col-md-8 page_left">

                        @foreach($languages as $language)
                            <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                <label for="title">{{ $language->title }} Title</label>
                                <input id="about_title" type="text" class="form-control" name="title_{{ $language->short }}" value="{{ $category->{"title_".$language->short} }}"
                                       autofocus>
                            </div>

                            <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                <label for="Description">{{ $language->title }} Short Description</label>
                                <textarea class="form-control short_desc" name="short_desc_{{ $language->short }}">{{ $category->{"short_desc_".$language->short} }}</textarea>
                            </div>

                        @endforeach

                            <div class="article_cat author_fields form-group  ">
                                <label for="product_cat">Parent Category</label>
                                <select id="parent_id" class="author-combobox-wrap article_author_name form-control" name="parent_id"
                                        id="cat_id" required>
                                    <option value="0">--Select Category--</option>
                                    @foreach($categories as $pr_category)
                                        <option data-type="child" @if($pr_category->id == $category->parent_id) selected @endif value="{{ $pr_category->id }}">{{ $pr_category->{"title_".$default_lang} }}</option>
                                    @endforeach
                                </select>
                            </div>

                        <div class="row page_image">
                            <div class="company_logo">
                                <div class="">
                                    <label for="Description">Image</label>
                                </div>
                                <div class="avatar_img">
                                    <div class="file-upload btn btn_1 green">
                                        <span>Choose image</span>
                                        <input type="file" name="image" id="uploadBtnBackImage" class="image upload"
                                               onchange="readURLBackImage(this)">
                                    </div>
                                </div>
                                <div class="profile-image-preview_back_image @if($category->image !='') {{ "active" }} @endif">
                                    <div class="ct-media--left">
                                        <a>
                                            <img id="uploadPreviewBackImage"  src="@if($category->image !='') {{ $category->image }} @endif">
                                        </a>
                                    </div>
                                    <input type="hidden" name="old_image" class="old_image" value="{{ $category->image }}">
                                    <div class="ct-media--content">
                                        <span></span>
                                        <a class="cross" onclick="deleteimgBackImage();" style="cursor:pointer;">
                                            <i class="fa fa-trash-o"></i> Delete
                                        </a>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-12">
                            <input type="hidden" name="add_gallery" value="ok">
                            <button type="submit" class="btn_1 green pull-right">
                                Save
                            </button>
                        </div>
                    </div>


                </form>
            </div>
        </div>


    </div>
@endsection
