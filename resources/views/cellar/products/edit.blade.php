@extends('cellar.layouts.app')

@section('content')

    <div class="container">
        <div class="form_box">
            @if( count($languages) > 1)
                <ul class="languages_tabs">
                    @foreach($languages as $language)
                        <li data-tab="{{ $language->short }}"
                            class="lang_tab {{ $language->short."_tab" }} @if($language->first == 1) {{"active_tab"}} @endif">
                            <p><span>{{ $language->title }}</span></p>
                        </li>
                    @endforeach
                </ul>
            @endif
            <div class="about_form">
                <form class="form-horizontal" method="POST" action="{{ url('/') }}/admin/products/{{ $product->id }}"
                      enctype='multipart/form-data'>
                    {{method_field('PUT')}}
                    {{ csrf_field() }}
                    <div class="col-md-8 page_left">

                        @foreach($languages as $language)
                            <div
                                class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                <label for="title">{{ $language->title }} Title</label>
                                <input id="about_title" type="text" class="form-control"
                                       name="title_{{ $language->short }}"
                                       value="{{ $product->{"title_".$language->short} }}"
                                       autofocus>
                            </div>

                            <div
                                class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                <label for="Description">{{ $language->title }} Short Description</label>
                                <textarea class="form-control short_desc"
                                          name="short_desc_{{ $language->short }}">{{ $product->{"short_desc_".$language->short} }}</textarea>
                            </div>
                            <div
                                class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                <label for="Description">{{ $language->title }} Description</label>
                                <textarea class="form-control  description"
                                          name="desc_{{ $language->short }}">{{ $product->{"desc_".$language->short} }}</textarea>
                            </div>
                        @endforeach

                        <div class="article_cat author_fields form-group  ">
                            <label for="product_cat">Category</label>
                            <select id="product_cat" class="author-combobox-wrap article_author_name form-control"
                                    name="product_cat_id[]"
                                    id="cat_id" required>
                                <option value="0">--Select Category--</option>
                                @foreach($categories as $category)
                                    <option data-type="child"
                                            @if(in_array($category->id, $product->categoriesID)) selected
                                            @endif value="{{ $category->id }}">{{ $category->{"title_".$default_lang} }}</option>
                                @endforeach
                            </select>
                        </div>

                            <div class="article_cat author_fields form-group">
                                <label for="pr_code">Available Count</label>
                                <input id="pr_code" type="text" class="author-combobox-wrap article_author_name form-control" name="available_count" value="{{ $product->available_count }}"
                                       autofocus>
                            </div>
                            <div class="article_cat author_fields form-group">
                                <label for="pr_code">Product Code</label>
                                <input id="pr_code" type="text" class="author-combobox-wrap article_author_name form-control" name="pr_code" value="{{ $product->pr_code }}"
                                       autofocus>
                            </div>
                            <div class="article_cat author_fields form-group">
                                <label for="price_1">Product Price (pcs)</label>
                                <input id="price_1" type="text" class="author-combobox-wrap article_author_name form-control" name="price_1" value="{{ $product->price_1 }}"
                                       autofocus>
                            </div>
                            <div class="article_cat author_fields form-group">
                                <label for="new_price_1">Discounted price (pcs)</label>
                                <input id="new_price_1" type="text" class="author-combobox-wrap article_author_name form-control" name="new_price_1" value="{{ $product->new_price_1 }}"
                                       autofocus>
                            </div>

                            <div class="article_cat author_fields form-group">
                                <label for="new_price_1">Second Price</label>
                                <div class="second-price-box">
                                    <select name="price_unit_2" id="" class="form-control">
                                        <option value="">Select Unit</option>
                                        <option @if($product->price_unit_2 == 1) selected @endif  value="1">Տուփ</option>
                                        <option @if($product->price_unit_2 == 2) selected @endif value="2">Տոննա</option>
                                    </select>
                                    <input id="new_price_2" type="text" class="author-combobox-wrap article_author_name form-control" name="price_2" value="{{$product->price_2}}"
                                           autofocus>
                                    <input id="new_price_2" type="text" class="author-combobox-wrap article_author_name form-control" name="new_price_2" value="{{$product->new_price_2}}"
                                           autofocus placeholder="Discounted price">
                                </div>

                            </div>

                            <div class="article_cat author_fields form-group">
                                <label for="Description">In Stock</label>
                                <div class="check_pr">
                                    <input @if($product->in_stock) checked @endif type="checkbox" name="in_stock" value="1">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="title">Video</label>
                                <input id="about_title" type="text" class="form-control" name="video" value="{{$product->video}}"
                                       autofocus>
                            </div>

                            <div class="product_attribute_section author_fields form-group  ">
                                <label>Attributes</label>
                                <div class="add_attribute">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                </div>
                                <div class="pr_attr_values">
                                    @foreach($product->attributes as $attributeItem)
                                        <div class="pr_attr_box">
                                            <div class="select_box_attr">
                                                <select class="form-control" name="pr_attr_name[]" id="">
                                                    @foreach($productCategory->attributes as $product_cat_attribute)
                                                        <option @if(in_array($product_cat_attribute->id, $product->attributesID)) selected @endif value="{{$product_cat_attribute->id}}">{{$product_cat_attribute->{"title_".$default_lang} }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            @foreach($languages as $language)
                                                <div style="margin: 0" class="form-group lang_field lang_field_{{$language->short}} value_box_attr @if($language->first == 1) active_field @else hidden_field @endif">
                                                    <input class="form-control" type="text" name="pr_attr_value_{{$language->short}}[]" value="{{$attributeItem->{"value_".$language->short} }}">
                                                </div>
                                            @endforeach
                                            <div class="delete_box_attr">
                                                <a class="del_row"><i class="fa fa-minus" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                    @endforeach

                                </div>
                            </div>

                        <div class="row page_image">
                            <div class="company_logo">
                                <div class="">
                                    <label for="Description">Image</label>
                                </div>
                                <div class="avatar_img">
                                    <div class="file-upload btn btn_1 green">
                                        <span>Choose image</span>
                                        <input type="file" name="image" id="uploadBtnBackImage" class="image upload"
                                               onchange="readURLBackImage(this)">
                                    </div>
                                </div>
                                <div
                                    class="profile-image-preview_back_image @if($product->image !='') {{ "active" }} @endif">
                                    <div class="ct-media--left">
                                        <a>
                                            <img id="uploadPreviewBackImage"
                                                 src="@if($product->image !='') {{ $product->image }} @endif">
                                        </a>
                                    </div>
                                    <input type="hidden" name="old_image" class="old_image"
                                           value="{{ $product->image }}">
                                    <div class="ct-media--content">
                                        <span></span>
                                        <a class="cross" onclick="deleteimgBackImage();" style="cursor:pointer;">
                                            <i class="fa fa-trash-o"></i> Delete
                                        </a>
                                    </div>
                                </div>
                            </div>

                        </div>

                            <div class="form-group gallery_box">
                                <label>Gallery ( Upload multiple picts )</label>
                                <div id="uploadProductGallery"  class="dropzone" dir="ltr"
                                     class="dropzone  @if(count($product->images) != '0') dz-default-hidden @endif">
                                    <div class="dz-default dz-message"><i class="fa fa-camera"></i>
                                        <!--<img src="/../assets/images/fileuploader-dragdrop-icon.png">-->
                                        <div class="btn_1 green"><span>Upload file</span></div>
                                        <h3 class="fileuploader-input-caption"><span>Drop files here or click to upload.</span></h3></div>
                                    @if(count($product->images) > 0)
                                        @foreach($product->images as $property_image)
                                            <div class="dz-preview dz-processing dz-image-preview dz-success dz-complete">
                                                <div class="dz-image"><img data-dz-thumbnail=""
                                                                           alt="{{ $property_image->image }}"
                                                                           src="{{ $property_image->image }}">
                                                </div>
                                                <div class="dz-details">
                                                    <div class="dz-filename">
                                                        <span data-dz-name="">@if(isset($property_image->image)){{ $property_image->image }} @endif</span>
                                                    </div>
                                                    <input type="hidden" data-dz-name1="" name="filename[]"
                                                           value="{{ $property_image->image }}"
                                                           class="dz-filename-after">
                                                </div>
                                                <div class="dz-progress">
                                                        <span class="dz-upload" data-dz-uploadprogress=""
                                                              style="width: 100%;"></span>
                                                </div>
                                                <div class="dz-error-message">
                                                    <span data-dz-errormessage=""></span>
                                                </div>
                                                <div class="dz-success-mark">
                                                    <svg width="54px" height="54px" viewBox="0 0 54 54"
                                                         version="1.1" xmlns="http://www.w3.org/2000/svg"
                                                         xmlns:xlink="http://www.w3.org/1999/xlink"
                                                         xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">
                                                        <title>Check</title>
                                                        <defs></defs>
                                                        <g id="Page-1" stroke="none" stroke-width="1" fill="none"
                                                           fill-rule="evenodd" sketch:type="MSPage">
                                                            <path d="M23.5,31.8431458 L17.5852419,25.9283877 C16.0248253,24.3679711 13.4910294,24.366835 11.9289322,25.9289322 C10.3700136,27.4878508 10.3665912,30.0234455 11.9283877,31.5852419 L20.4147581,40.0716123 C20.5133999,40.1702541 20.6159315,40.2626649 20.7218615,40.3488435 C22.2835669,41.8725651 24.794234,41.8626202 26.3461564,40.3106978 L43.3106978,23.3461564 C44.8771021,21.7797521 44.8758057,19.2483887 43.3137085,17.6862915 C41.7547899,16.1273729 39.2176035,16.1255422 37.6538436,17.6893022 L23.5,31.8431458 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z"
                                                                  id="Oval-2" stroke-opacity="0.198794158"
                                                                  stroke="#747474" fill-opacity="0.816519475"
                                                                  fill="#FFFFFF" sketch:type="MSShapeGroup"></path>
                                                        </g>
                                                    </svg>
                                                </div>
                                                <div class="dz-error-mark">
                                                    <svg width="54px" height="54px" viewBox="0 0 54 54"
                                                         version="1.1" xmlns="http://www.w3.org/2000/svg"
                                                         xmlns:xlink="http://www.w3.org/1999/xlink"
                                                         xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">
                                                        <title>Error</title>
                                                        <defs></defs>
                                                        <g id="Page-1" stroke="none" stroke-width="1" fill="none"
                                                           fill-rule="evenodd" sketch:type="MSPage">
                                                            <g id="Check-+-Oval-2" sketch:type="MSLayerGroup"
                                                               stroke="#747474" stroke-opacity="0.198794158"
                                                               fill="#FFFFFF" fill-opacity="0.816519475">
                                                                <path d="M32.6568542,29 L38.3106978,23.3461564 C39.8771021,21.7797521 39.8758057,19.2483887 38.3137085,17.6862915 C36.7547899,16.1273729 34.2176035,16.1255422 32.6538436,17.6893022 L27,23.3431458 L21.3461564,17.6893022 C19.7823965,16.1255422 17.2452101,16.1273729 15.6862915,17.6862915 C14.1241943,19.2483887 14.1228979,21.7797521 15.6893022,23.3461564 L21.3431458,29 L15.6893022,34.6538436 C14.1228979,36.2202479 14.1241943,38.7516113 15.6862915,40.3137085 C17.2452101,41.8726271 19.7823965,41.8744578 21.3461564,40.3106978 L27,34.6568542 L32.6538436,40.3106978 C34.2176035,41.8744578 36.7547899,41.8726271 38.3137085,40.3137085 C39.8758057,38.7516113 39.8771021,36.2202479 38.3106978,34.6538436 L32.6568542,29 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z"
                                                                      id="Oval-2" sketch:type="MSShapeGroup"></path>
                                                            </g>
                                                        </g>
                                                    </svg>
                                                </div>
                                                <a class="dz-remove old_img_remove" data-dz-remove="">Remove
                                                    file</a>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>

                        <div class="col-md-12">
                            <input type="hidden" name="add_gallery" value="ok">
                            <button type="submit" class="btn_1 green pull-right">
                                Save
                            </button>
                        </div>
                    </div>


                </form>
            </div>
        </div>


    </div>
@endsection
