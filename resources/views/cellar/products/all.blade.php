@extends('cellar.layouts.app')
<link href="{{ asset('assets/css/admin/admin.css') }}" rel="stylesheet">
<link href="{{ asset('assets/css/admin/jquery.switch.css') }}" rel="stylesheet">
@section('content')

    <div class="container services_block">
        <div class="col-md-8 page_show_content">
            <div class="page_title_box parent_page_title">
                <h4>Products</h4>
                <div class="page_actions_block">
                    <a href="{{ url('/') }}/admin/products/create" class="sub_page_add btn_2">
                        <i class="icon-docs"></i>
                        Add
                    </a>
                </div>
            </div>
            <div class="child_pages">
                @foreach($products as $product)
                    <div class="page_title_box child_page_title">
                        @if($product->image)
                            <div class="project_img_box col-md-2 col-sm-2">
                                <img src="{{$product->image}}" >
                            </div>
                        @endif
                        <p>{{ $product->title }}</p>
                        <div class="page_actions_block">
                            <a href="{{ url('/') }}/admin/products/{{ $product->id }}/edit" class="parent_page_edit btn_2">
                                <i class="icon-edit-3"></i>
                                Edit
                            </a>
                            <a data-id="{{ $product->id }}" data-href="products" class="page_del_btn delete_from_db btn_delete btn-danger">
                                <i class="icon-trash"></i>
                                Delete
                            </a>
                        </div>

                    </div>
                @endforeach
            </div>
        </div>
    </div>




    </div>
@endsection
