@extends('cellar.layouts.app')
<link href="{{ asset('assets/css/admin/admin.css') }}" rel="stylesheet">
<link href="{{ asset('assets/css/admin/jquery.switch.css') }}" rel="stylesheet">
@section('content')
    <div class="container">
        <div class="form_box">
            <div class="page_title_box ">
                <h4 class="eng_block"><a href="{{ url('/') }}/admin/slider/1/edit">TOP BANNER</a> | EDIT SLIDE</h4>
            </div>
            @if( count($languages) > 1)
                <ul class="languages_tabs">
                    @foreach($languages as $language)
                        <li data-tab="{{ $language->short }}"
                            class="lang_tab {{ $language->short."_tab" }} @if($language->first == 1) {{"active_tab"}} @endif">
                            <p><span>{{ $language->title }}</span></p>
                        </li>
                    @endforeach
                </ul>
            @endif
            <div class="about_form">

                <form class="form-horizontal" method="POST"
                      action="{{ url('/') }}/admin/slider/1" enctype='multipart/form-data'>
                    {{method_field('PUT')}}
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-12 col-sm-12 pull-left add_slide_box">

                            <div class="add_slide_left">
                                <div class="add_slide_left_content">


                                    @foreach($languages as $language)
                                        <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                            <label for="title">{{ $language->title }} Title</label>
                                            <input id="about_title" type="text" class="form-control" name="slide_title_{{ $language->short }}" value="{{ $slide->{"slide_title_".$language->short } }}"
                                                   autofocus>
                                        </div>
                                        <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                            <label for="title">{{ $language->title }} Slide sub title <span class="slider_sub_title_length_{{$language->short}}">( {{ 300 - mb_strlen($slide->{"slide_sub_title_".$language->short}) }} chars)</span></label>
                                            <textarea data-lang="{{ $language->short }}" id="about_title" type="text" class="check_chars form-control" maxlength="300" name="slide_sub_title_{{ $language->short }}">{{ $slide->{"slide_sub_title_".$language->short } }}</textarea>
                                        </div>

                                    @endforeach
                                    <div class="form-group slide_link_box">
                                        <label for="title">Slide link</label>
                                        <input id="about_title" type="text" class="form-control" name="slide_link"
                                               value="{{ $slide->slide_link }}" autofocus>
                                    </div>
                                    <div class="form-group  slide_new_window">
                                            <div class="slide_new_window_box">
                                                <label class="slide_new_window_title pull-left" for="title">New window</label>
                                                <label class="switch-light switch-ios pull-left">
                                                    <input type="checkbox" name="new_blank" @if($slide->new_blank == '1'){{ "checked" }}@endif id="option_1" value="1">
                                                    <span>
							                        <span>No</span>
													<span>Yes</span>
                                            </span>
                                                    <a></a>
                                                </label>
                                            </div>

                                        </div>
                                    <div class="form-group">
                                            <label for="service_order">Order</label>
                                            <input id="service_order" type="text" class="form-control" name="item_order"
                                                   value="{{ $slide->item_order }}">
                                        </div>
                                </div>
                            </div>


                        </div>
                        <div class="col-md-12 col-sm-12 pull-left">
                            <div class="add_slide_left_img">
                                <label>Image (width:1920, heght:1024px)</label>
                                <div class="form-group">
                                    <div class="avatar_img">

                                        <div class="file-upload btn btn_1 button-primary">
                                            <span>Choose Image</span>
                                            <input type="file" name="image" id="uploadBtn1" class="image upload"
                                                   onchange="readURL(this)">
                                        </div>
                                    </div>
                                </div>
                                <div class="profile-image-preview @if($slide->image !='') {{ "active" }} @endif">
                                    <div class="ct-media--left">
                                        <a>
                                            <img id="uploadPreview" src="{{ $slide->image }}">
                                        </a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>


                    <div class="save_slider">
                        <button type="submit" class="btn_1 slide_save_btn green">
                            Save
                        </button>
                    </div>
                </form>
            </div>

        </div>
    </div>
@endsection
