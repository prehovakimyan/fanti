@extends('admin.layouts.page')
<link href="{{ asset('css/admin/admin.css') }}" rel="stylesheet">
<link href="{{ asset('css/admin/jquery.switch.css') }}" rel="stylesheet">

@section('content')

    <div class="container">
        <div class="form_box">
            <div class="page_title_box ">
                <h4 class="eng_block"><a href="{{ url('/') }}/admin/slider/1/edit">TOP BANNER</a> | EDIT SLIDE</h4>
            </div>
            @if( count($languages) > 1)
                <ul class="languages_tabs">
                    @foreach($languages as $language)
                        <li data-tab="{{ $language->short }}"
                            class="lang_tab {{ $language->short."_tab" }} @if($language->first == 1) {{"active_tab"}} @endif">
                            <p><span>{{ $language->title }}</span></p>
                        </li>
                    @endforeach
                </ul>
            @endif
            <div class="about_form">

                        <form class="form-horizontal" method="POST"
                              action="{{ url('/') }}/admin/slide/update" enctype='multipart/form-data'>
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-12 col-sm-12 pull-left add_slide_box">

                                    <div class="add_slide_left">
                                        <div class="add_slide_left_content">


                                            @foreach($languages as $language)
                                                <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                                    <label for="title">{{ $language->title }} Title</label>
                                                    <input id="about_title" type="text" class="form-control" name="slide_title_{{ $language->short }}" value="{{ $slide->{"slide_title_".$language->short } }}"
                                                           autofocus>
                                                </div>
                                                <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                                    <label for="title">{{ $language->title }} Slide sub title <span class="slider_sub_title_length_{{$language->short}}">( {{ 300 - mb_strlen($slide->{"slide_sub_title_".$language->short}) }} chars)</span></label>
                                                    <textarea data-lang="{{ $language->short }}" id="about_title" type="text" class="check_chars form-control" maxlength="300" name="slide_sub_title_{{ $language->short }}">{{ $slide->{"slide_sub_title_".$language->short } }}</textarea>
                                                </div>

                                            @endforeach
                                            <div class="form-group slide_link_box">
                                                <label for="title">Slide link</label>
                                                <input id="about_title" type="text" class="form-control" name="slide_link"
                                                       value="{{ $slide->slide_link }}" autofocus>
                                            </div>
                                            <div class="form-group slide_link_box">
                                                <label for="title">Slide Contact Phone</label>
                                                <input id="about_title" type="text" class="form-control" name="slide_contact"
                                                       value="{{ $slide->slide_contact }}" autofocus>
                                            </div>
                                            <div class="form-group slide_new_window">
                                                <label class="pull-left" style="margin-right: 15px;margin-top: 5px" for="title">New
                                                    window</label>

                                                <label class="switch-light switch-ios pull-left">
                                                    <input @if($slide->new_blank == '1'){{ "checked" }}@endif type="checkbox" name="new_blank" id="option_1"  value="1">
                                                    <span>
							                        <span>No</span>
													<span>Yes</span>
                                            </span>
                                                    <a></a>
                                                </label>

                                            </div>
                                            <div class="form-group">
                                                <label for="service_order">Order</label>
                                                <input id="service_order" type="text" class="form-control" name="item_order"
                                                       value="{{ $slide->item_order }}">
                                            </div>


                                        </div>
                                    </div>

                                    <div class="add_slide_right pull-right">
                                        <div class="slide_effects_title">
                                            Image Gallery Custom Options
                                        </div>
                                        <div class="slide_effects_content">
                                            <div class="form-group">
                                                <label for="service_order">Slide Effect</label>
                                                <select class="slide_effect" name="slide_effect">
                                                    <option @if($slide->slide_effect == "fade"){{ "selected" }}@endif value="fade">Fade</option>
                                                    <option @if($slide->slide_effect == "rendom"){{ "selected" }}@endif value="rendom">Random</option>
                                                    <option @if($slide->slide_effect == "zoomin"){{ "selected" }}@endif value="zoomin">Zoom in</option>
                                                    <option @if($slide->slide_effect == "zoomout"){{ "selected" }}@endif value="zoomout">Zoom out</option>
                                                    <option @if($slide->slide_effect == "fadetoleftfadefromright"){{ "selected" }}@endif value="fadetoleftfadefromright">Left to Right</option>
                                                    <option @if($slide->slide_effect == "fadetorightfadefromleft"){{ "selected" }}@endif value="fadetorightfadefromleft">Right to Left</option>
                                                    <option @if($slide->slide_effect == "fadetotopfadefrombottom"){{ "selected" }}@endif value="fadetotopfadefrombottom">Top to Bottom</option>
                                                    <option @if($slide->slide_effect == "fadetobottomfadefromtop"){{ "selected" }}@endif value="fadetobottomfadefromtop">Bottom to Top</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Change speed</label>
                                                <input type="text" class="change_speed" name="change_speed" value="{{ $slide->change_speed }}">
                                            </div>
                                            <div class="form-group">
                                                <label>Title Effect</label>
                                                <select class="title_effect" name="title_effect">
                                                    <option @if($slide->title_effect_id == "1"){{ "selected" }}@endif value="1">From left</option>
                                                    <option @if($slide->title_effect_id == "2"){{ "selected" }}@endif value="2">From Right</option>
                                                    <option @if($slide->title_effect_id == "3"){{ "selected" }}@endif value="3">From Bottom</option>
                                                    <option @if($slide->title_effect_id == "4"){{ "selected" }}@endif value="4">From Top</option>
                                                    <option @if($slide->title_effect_id == "5"){{ "selected" }}@endif value="5">Paralax</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Title paralax</label>
                                                <label class="switch-light switch-ios pull-right">
                                                    <input data-id="{{ $slide->id }}" name="title_paralax" type="checkbox"
                                                           class="slide_hidden option_hidden{{$slide->id}}"
                                                           @if($slide->title_paralax == 1){{ "checked" }}@endif
                                                           value="1">

                                                    <span>
							                        <span>No</span>
													<span>Yes</span>
													</span>
                                                    <a></a>
                                                </label>
                                            </div>
                                        </div>


                                        <input type="hidden" name="edit_slide" value="ok">
                                        <input type="hidden" name="slide_id" value="{{ $slide->id }}">
                                        <input type="hidden" name="old_image" value="{{ $slide->image }}">
                                    </div>



                                </div>
                                <div class="col-md-12 col-sm-12 pull-left">
                                    <div class="add_slide_left_img slid_icon_image">
                                        <label>Slider Icon (width:74px, heght:74px)</label>
                                        <div class="form-group">
                                            <div class="avatar_img">
                                                <div class="file-upload btn btn_1 green">
                                                    <span>Choose Image</span>
                                                    <input type="file" name="image_logo" id="uploadBtnLogo" class="image upload" onchange="readURLLogo(this)">
                                                </div>
                                            </div>


                                        </div>
                                        <div class="profile-image-preview_logo @if($slide->slider_icon !='') {{ "active" }} @endif">
                                            <div class="ct-media--left">
                                                <a>
                                                    <img id="uploadPreviewLogo" src="@if($slide->slider_icon !='') {{ url('/') }}/public/uploads/slider/{{ $slide->slider_icon  }} @endif">
                                                </a>
                                            </div>
                                            <input type="hidden" name="old_image_logo" class="old_image_logo" value="{{ $slide->slider_icon }}">
                                            <div class="ct-media--content">
                                                <span></span>
                                                <a class="cross" onclick="deleteimgLogo();" style="cursor:pointer;">
                                                    <i class="fa fa-trash-o"></i> Delete
                                                </a>
                                            </div>
                                        </div>

                                         </div>
                                </div>
                                <div class="col-md-12 col-sm-12 pull-left">
                                    <div class="add_slide_left_img">
                                        <label>Image (width:1600px, heght:800px)</label>
                                        <div class="form-group">
                                            <div class="avatar_img">

                                                <div class="file-upload btn btn_1 button-primary">
                                                    <span>Choose Image</span>
                                                    <input type="file" name="image" id="uploadBtn1" class="image upload"
                                                           onchange="readURL(this)">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="profile-image-preview @if($slide->image !='') {{ "active" }} @endif">
                                            <div class="ct-media--left">
                                                <a>
                                                    <img id="uploadPreview" src="{{ url('/') }}/uploads/slider/{{ $slide->image }}">
                                                </a>
                                            </div>
                                        </div>

                                         </div>
                                </div>
                            </div>


                            <div class="save_slider">
                                <button type="submit" class="btn_1 slide_save_btn green">
                                    Save
                                </button>
                            </div>
                        </form>
                    </div>

        </div>
    </div>
@endsection