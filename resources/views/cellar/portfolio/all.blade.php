@extends('cellar.layouts.app')
<link href="{{ asset('assets/css/admin/admin.css') }}" rel="stylesheet">
<link href="{{ asset('assets/css/admin/jquery.switch.css') }}" rel="stylesheet">
@section('content')

    <div class="container services_block">
        <div class="col-md-8 page_show_content">
            <div class="page_title_box parent_page_title">
                <h4>Portfolio</h4>
                <div class="actions-portfolio">
                    <div class="page_actions_block">
                        <a href="{{ url('/') }}/admin/portfolio/create" class="sub_page_add btn_2">
                            <i class="icon-docs"></i>
                            Add
                        </a>
                    </div>
                    <a href="{{ url('/') }}/admin/portfolio-category" class="categories_add btn_2">
                        <i class="icon-th-list-3"></i>
                        Categories
                    </a>
                </div>

            </div>
            <div class="child_pages">
                @foreach($portfolios as $portfolio)
                    <div class="page_title_box child_page_title">
                        @if($portfolio->image)
                            <div class="project_img_box col-md-2 col-sm-2">
                                <img src="{{$portfolio->image}}" >
                            </div>
                        @endif
                        <p>{{ $portfolio->title }}</p>
                        <div class="page_actions_block">
                            <a href="{{ url('/') }}/admin/portfolio/{{ $portfolio->id }}/edit" class="parent_page_edit btn_2">
                                <i class="icon-edit-3"></i>
                                Edit
                            </a>
                            <a data-id="{{ $portfolio->id }}" class="page_del_btn portfolio_delete btn_delete btn-danger">
                                <i class="icon-trash"></i>
                                Delete
                            </a>
                        </div>

                    </div>
                @endforeach
            </div>
        </div>
    </div>




    </div>
@endsection
