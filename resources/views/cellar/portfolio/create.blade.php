@extends('cellar.layouts.app')

@section('content')

    <div class="container">
        <div class="form_box">
            @if( count($languages) > 1)
                <ul class="languages_tabs">
                    @foreach($languages as $language)
                        <li data-tab="{{ $language->short }}" class="lang_tab {{ $language->short."_tab" }} @if($language->first == 1) {{"active_tab"}} @endif">
                            <p><span>{{ $language->title }}</span></p>
                        </li>
                    @endforeach
                </ul>
            @endif
            <div class="about_form">

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <form class="form-horizontal" method="POST" action="{{ url('/') }}/admin/portfolio" enctype='multipart/form-data'>
                    {{ csrf_field() }}
                    <div class="col-md-8 page_left">

                        @foreach($languages as $language)
                            <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                <label for="title">{{ $language->title }} Title</label>
                                <input id="about_title" type="text" class="form-control" name="title_{{ $language->short }}" value="{{old("title_".$language->short)}}"
                                       autofocus>
                            </div>

                            <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                <label for="Description">{{ $language->title }} Short Description</label>
                                <textarea class="form-control short_desc" name="short_desc_{{ $language->short }}">{{old("short_desc_".$language->short)}}</textarea>
                            </div>
                            <div  class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                <label for="Description">{{ $language->title }} Description</label>
                                <textarea class="form-control  @if($language->short == 'ir') description_ir @else description @endif" name="desc_{{ $language->short }}">{{old("desc_".$language->short)}}</textarea>
                            </div>
                            <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                <label for="title">{{ $language->title }} Location</label>
                                <input id="about_title" type="text" class="form-control" name="location_{{ $language->short }}" value="{{old("location_".$language->short)}}"
                                       autofocus>
                            </div>
                            <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                <label for="title">{{ $language->title }} Video Description</label>
                                <input id="about_title" type="text" class="form-control" name="video_desc_{{ $language->short }}" value="{{old("video_desc_".$language->short)}}"
                                       autofocus>
                            </div>
                        @endforeach
                        <div class="form-group">
                            <label for="title">Video Link</label>
                            <input id="about_title" type="text" class="form-control" name="video_link" value="{{old("video_link")}}" autofocus>
                        </div>
                        <div class="form-group">
                            <label for="title">Project Date</label>
                            <input id="about_title" type="date" class="form-control" name="project_date" value="{{old("project_date")}}" autofocus>
                        </div>
                        <div class="article_cat author_fields form-group">
                                <label>Categories</label>
                                <div class="recomed_cats">
                                    @foreach($categories as $category)
                                        <div class="check_block_parent">
                                            <div class="check_block">
                                                <input class="pr_category"
                                                       type="checkbox" value="{{ $category->id }}" name="category[]">
                                                <span><b>{{ $category->{"title_".$default_lang} }}</b></span>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>

                            </div>

                        <div class="row page_image">
                            <div class="company_logo">
                                <div class="">
                                    <label for="Description">Image</label>
                                </div>
                                <div class="avatar_img">
                                    <div class="file-upload btn btn_1 green">
                                        <span>Choose image</span>
                                        <input type="file" name="image" id="uploadBtnLogo" class="image upload"
                                               onchange="readURLLogo(this)">
                                    </div>
                                </div>
                                <div class="profile-image-preview_logo">
                                    <div class="ct-media--left">
                                        <a>
                                            <img id="uploadPreviewLogo" src="">
                                        </a>
                                    </div>
                                    <div class="ct-media--content">
                                        <span></span>
                                        <a class="cross" onclick="deleteimgLogo();" style="cursor:pointer;">
                                            <i class="fa fa-trash-o"></i> Delete
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group gallery_box">
                            <label>Gallery ( Upload multiple picts )</label>
                            <div id="uploadPortfolioGallery" class="dropzone" dir="ltr"></div>
                        </div>
                        <div class="form-group gallery_box">
                            <label>Plan Images ( Upload multiple picts )</label>
                            <div id="uploadPortfolioPlan" class="dropzone" dir="ltr"></div>
                        </div>

                        <div class="col-md-12">
                            <input type="hidden" name="add_gallery" value="ok">
                            <button type="submit" class="btn_1 green pull-right">
                                Save
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
