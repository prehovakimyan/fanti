$(document).on("click", ".sub_pdf_append_update", function () {
    var result = true;


    var news_sub_pdf_id = $(".news_sub_pdf_id").val();
    var current_page_id = $(".current_page_id").val();
    var current_file = $(".current_pdf_file_name").val();
    var title_block = '.open_sub_pdf_block_title_' + news_sub_pdf_id;

    if($(".append_file_title_edit").val() != ''){
        $(".append_file_title_edit").removeClass("error_empty");
        var title = $(".append_file_title_edit").val();
    }else{
        result = false;
        $(".append_file_title_edit").addClass("error_empty");
    }

    var desc = $(".append_file_desc_edit").val();

    var file_data = $('#editMyFile').prop('files')[0];
    var form_data = new FormData();
    form_data.append('file', file_data);
    form_data.append('title', title);
    form_data.append('desc', desc);
    form_data.append('news_sub_pdf_id', news_sub_pdf_id);
    form_data.append('old_file', current_file);
    form_data.append('parent_id', current_page_id);
    if(result){
        $.ajax({
            type: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '/admin/news_sub_page_pdf/update_sub_pdf',
            data: form_data,
            type: 'POST',
            contentType: false, // The content type used when sending data to the server.
            cache: false, // To unable request pages to be cached
            processData: false,
            success: function (msg) {
                $(title_block).html(msg);
                $(".edit_sub_pdf_append_popup").toggle();
                $(".edit_sub_pdf_append_popup .sub_pdf_append_block").remove();
            }
        });
    }
});
$(document).on("click", ".sub_with_pdf_append_update", function () {
    var result = true;

    var site_langs = $(".site_langs").val();
    var res = site_langs.split(",");
    var form_data = new FormData();
    var news_sub_pdf_id = $(".sub_with_pdf_id").val();

    var form_data = new FormData();
    form_data.append('news_sub_pdf_id', news_sub_pdf_id);
    var title_block = '.open_sub_pdf_block_title_' + news_sub_pdf_id;

    res.forEach(function(entry) {
        if(entry !=''){
            var lang = entry;
            var title = $(".append__with_file_title_edit_" + lang).val();
            var desc = $(".append_with_file_desc_edit_" + lang).val();
            var current_file = $(".old_pdf_title_" + lang).val();
            form_data.append('title_' + lang, title);
            form_data.append('short_desc_' + lang, desc);
            form_data.append('old_file_' + lang, current_file);
            if($('#editpage_with_pdf_' + lang).prop('files')[0]){
                var file_data = $('#editpage_with_pdf_' + lang).prop('files')[0];
            }else{
                var file_data = '';
            }
            form_data.append('file_' + lang, file_data);
            form_data.append('news_sub_pdf_id', news_sub_pdf_id);
        }

    });

    if(result){
        $.ajax({
            type: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '/admin/sub_pages_with_pdf/update_sub_pdf',
            data: form_data,
            type: 'POST',
            contentType: false, // The content type used when sending data to the server.
            cache: false, // To unable request pages to be cached
            processData: false,
            success: function (msg) {
                var i = 0;
                $.each(msg, function( index, value ) {
                    $(".open_sub_pdf_block_title_" + news_sub_pdf_id + "_" + index).html(value);
                    i++;
                });

                $(".edit_sub_pages_with_pdf_append_popup").toggle();
                $(".edit_sub_pages_with_pdf_append_popup .sub_pdf_append_block").remove();
            }
        });
    }
});


$('.edit_successful').delay(1500).slideToggle(300);


$(document).on('click', '.add_attribute', function () {
    /*var cats = [];
    var i = 0;
    $(".pr_category").each(function(){
        if($(this).is(":checked")) {
            cats[i] = {};
            cats[i].id = $(this).val();
            i++;
        }
    });*/

    var catId = $("#product_cat").val();

    if(catId > 0){
        $.ajax({
            type: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '/admin/products/get-attrs',
            data: {"catId": catId},
            success: function (msg) {
                $(".pr_attr_values").append(msg);
            }
        });
    } else {
        alert("Please Select Category");
    }
});

$(document).on('click', '.delete_from_db', function () {
    var row_id = $(this).attr('data-id');
    var from = $(this).attr('data-href');
    swal({
            title: "Are you sure you want to delete?",
            text: "Your will not be able to recover this row!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Delete",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/admin/'+from+'/' + row_id,
                    data: {"id": row_id, "_method": "DELETE"},
                    success: function (msg) {
                        location.reload();
                    }
                });
            } else {
                $(".sweet-overlay").css("display", "none");$(".sweet-alert").css("display", "none");
            }
        });
});

$(document).on('click', '.service_delete', function () {
    var service_id = $(this).attr('data-id');
    swal({
            title: "Are you sure you want to delete?",
            text: "Your will not be able to recover this service!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Delete",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/admin/services/' + service_id,
                    data: {"id": service_id, "_method": "DELETE"},
                    success: function (msg) {
                        location.reload();
                    }
                });
            } else {
                $(".sweet-overlay").css("display", "none");$(".sweet-alert").css("display", "none");
            }
        });
});
$(document).on('click', '.team_delete', function () {
    var service_id = $(this).attr('data-id');
    swal({
            title: "Are you sure you want to delete?",
            text: "Your will not be able to recover this Member!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Delete",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/admin/our-team/' + service_id,
                    data: {"id": service_id, "_method": "DELETE"},
                    success: function (msg) {
                        location.reload();
                    }
                });
            } else {
                $(".sweet-overlay").css("display", "none");$(".sweet-alert").css("display", "none");
            }
        });
});
$(document).on('click', '.prcat_delete', function () {
    var service_id = $(this).attr('data-id');
    swal({
            title: "Are you sure you want to delete?",
            text: "Your will not be able to recover this category!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Delete",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/admin/prcats/' + service_id,
                    data: {"id": service_id, "_method": "DELETE"},
                    success: function (msg) {
                        location.reload();
                    }
                });
            } else {
                $(".sweet-overlay").css("display", "none");$(".sweet-alert").css("display", "none");
            }
        });
});
$(document).on('click', '.brand_delete', function () {
    var service_id = $(this).attr('data-id');
    swal({
            title: "Are you sure you want to delete?",
            text: "Your will not be able to recover this brand!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Delete",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/admin/brands/' + service_id,
                    data: {"id": service_id, "_method": "DELETE"},
                    success: function (msg) {
                        location.reload();
                    }
                });
            } else {
                $(".sweet-overlay").css("display", "none");$(".sweet-alert").css("display", "none");
            }
        });
});
$(document).on('click', '.product_delete', function () {
    var service_id = $(this).attr('data-id');
    swal({
            title: "Are you sure you want to delete?",
            text: "Your will not be able to recover this product!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Delete",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/admin/products/' + service_id,
                    data: {"id": service_id, "_method": "DELETE"},
                    success: function (msg) {
                        location.reload();
                    }
                });
            } else {
                $(".sweet-overlay").css("display", "none");$(".sweet-alert").css("display", "none");
            }
        });
});
$(document).on('click', '.language_delete', function () {
    var service_id = $(this).attr('data-id');
    swal({
            title: "Are you sure you want to delete?",
            text: "Your will not be able to recover this service!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Delete",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/admin/settings/delete/' + service_id,
                    data: {"id": service_id},
                    success: function (msg) {
                        location.reload();
                    }
                });
            } else {
                $(".sweet-overlay").css("display", "none");$(".sweet-alert").css("display", "none");
            }
        });
});
$(document).on('click', '.news_sub_delete', function () {
    var service_id = $(this).attr('data-id');
    var count_pdfs = $(this).attr('data-count');
    if(count_pdfs > 0){
        var subtitle = "Your will not be able to recover this page! Pdf`s count =" + count_pdfs;
    }else{
        var subtitle = "Your will not be able to recover this page!";
    }
    swal({
            title: "Are you sure you want to delete?",
            text: subtitle,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Delete",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/admin/news_sub_page_pdf_delete',
                    data: {"id": service_id},
                    success: function (msg) {
                        location.reload();
                    }
                });
            } else {
                $(".sweet-overlay").css("display", "none");$(".sweet-alert").css("display", "none");
            }
        });
});

$(document).on('click', '.private_area_del', function () {
    var private_area_id = $(this).attr('data-id');
    swal({
            title: "Are you sure you want to delete?",
            text: "Your will not be able to recover this private area!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Delete",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/admin/private_area/delete/' + private_area_id,
                    data: {"id": private_area_id},
                    success: function (msg) {
                        location.reload();
                    }
                });
            } else {
                $(".sweet-overlay").css("display", "none");$(".sweet-alert").css("display", "none");
            }
        });
});

$(document).on('click', '.delete_private_area_pdf', function () {
    var private_area_pdf_id = $(this).attr('data-id');
    swal({
            title: "Are you sure you want to delete?",
            text: "Your will not be able to recover this private area!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Delete",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/admin/private_area/delete_private_area_pdf/' + private_area_pdf_id,
                    data: {"id": private_area_pdf_id},
                    success: function (msg) {
                        location.reload();
                    }
                });
            } else {
                $(".sweet-overlay").css("display", "none");$(".sweet-alert").css("display", "none");
            }
        });
});

//Delete menu
$(document).on('click', '.menu_delete', function () {
    var menu_id = $(this).attr('data-id');
    swal({
            title: "Are you sure you want to delete?",
            text: "Your will not be able to recover this menu!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Delete",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/admin/menu/' + menu_id,
                    data: {"id": menu_id, "_method": "DELETE"},
                    success: function (msg) {
                        window.location.href = '/admin/menu';
                    }
                });
            } else {
                $(".sweet-overlay").css("display", "none");$(".sweet-alert").css("display", "none");
            }
        });
});


$(document).on('click', '.media_delete', function () {
    var media_id = $(this).attr('data-id');
    swal({
            title: "Are you sure you want to delete?",
            text: "Your will not be able to recover this media!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Delete",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/admin/media/' + media_id,
                    data: {"id": media_id, "_method": "DELETE"},
                    success: function (msg) {
                        location.reload();
                    }
                });
            } else {
                $(".sweet-overlay").css("display", "none");$(".sweet-alert").css("display", "none");
            }
        });
});
$(document).on('click', '.author_delete', function () {
    var author_id = $(this).attr('data-id');
    swal({
            title: "Are you sure you want to delete?",
            text: "Your will not be able to recover this author!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Delete",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/admin/authors/' + author_id,
                    data: {"id": author_id, "_method": "DELETE"},
                    success: function (msg) {
                        location.reload();
                    }
                });
            } else {
                $(".sweet-overlay").css("display", "none");$(".sweet-alert").css("display", "none");
            }
        });
});
$(document).on('click', '.project_delete ', function () {
    var service_id = $(this).attr('data-id');
    swal({
            title: "Are you sure you want to delete?",
            text: "Your will not be able to recover this project!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Delete",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/admin/projects/' + service_id,
                    data: {"id": service_id, "_method": "DELETE"},
                    success: function (msg) {
                        location.reload();
                    }
                });
            } else {
                $(".sweet-overlay").css("display", "none");$(".sweet-alert").css("display", "none");
            }
        });
});
$(document).on('click', '.partner_delete  ', function () {
    var partner_id = $(this).attr('data-id');
    swal({
            title: "Are you sure you want to delete?",
            text: "Your will not be able to recover this partner!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Delete",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/admin/partners/' + partner_id,
                    data: {"id": partner_id, "_method": "DELETE"},
                    success: function (msg) {
                        location.reload();
                    }
                });
            } else {
                $(".sweet-overlay").css("display", "none");$(".sweet-alert").css("display", "none");
            }
        });
});


$(document).on('click', '.package_delete  ', function () {
    var partner_id = $(this).attr('data-id');
    swal({
            title: "Are you sure you want to delete?",
            text: "Your will not be able to recover this package!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Delete",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/admin/package/' + partner_id,
                    data: {"id": partner_id, "_method": "DELETE"},
                    success: function (msg) {
                        location.reload();
                    }
                });
            } else {
                $(".sweet-overlay").css("display", "none");$(".sweet-alert").css("display", "none");
            }
        });
});
$(document).on('click', '.remove_package  ', function () {
    var index = $(this).attr("data-index");
    swal({
            title: "Are you sure you want to delete?",
            text: "Your will not be able to recover this package!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Delete",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function (isConfirm) {
            if (isConfirm) {
                $(".sweet-overlay").css("display", "none");$(".sweet-alert").css("display", "none");
                $(".old_package_desc" + index).remove();
            } else {
                $(".sweet-overlay").css("display", "none");$(".sweet-alert").css("display", "none");
            }
        });
});

$(document).on('click', '.new_remove_package  ', function () {
    $(this).parent().addClass("delete_block");
    swal({
            title: "Are you sure you want to delete?",
            text: "Your will not be able to recover this package!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Delete",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function (isConfirm) {
            if (isConfirm) {
                $(".sweet-overlay").css("display", "none");$(".sweet-alert").css("display", "none");
                $(".delete_block").remove();
            } else {
                $(".sweet-overlay").css("display", "none");$(".sweet-alert").css("display", "none");
                $(".delete_block").removeClass("delete_block");
            }
        });
});

$(document).on('click', '.company_delete  ', function () {
    var company_id = $(this).attr('data-id');
    swal({
            title: "Are you sure you want to delete?",
            text: "Your will not be able to recover this company!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Delete",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/admin/companies/' + company_id,
                    data: {"id": company_id, "_method": "DELETE"},
                    success: function (msg) {
                        location.reload();
                    }
                });
            } else {
                $(".sweet-overlay").css("display", "none");$(".sweet-alert").css("display", "none");
            }
        });
});
$(document).on('click', '.careers_delete', function () {
    var company_id = $(this).attr('data-id');
    swal({
            title: "Are you sure you want to delete?",
            text: "Your will not be able to recover this career!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Delete",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/admin/career/' + company_id,
                    data: {"id": company_id, "_method": "DELETE"},
                    success: function (msg) {
                        location.reload();
                    }
                });
            } else {
                $(".sweet-overlay").css("display", "none");$(".sweet-alert").css("display", "none");
            }
        });
});
$(document).on('click', '.slide_delete  ', function () {
    var slide_id = $(this).attr('data-id');
    swal({
            title: "Are you sure you want to delete?",
            text: "Your will not be able to recover this Slide!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Delete",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/admin/slider/' + slide_id,
                    data: {"id": slide_id, "_method": "DELETE"},
                    success: function (msg) {
                        location.reload();
                    }
                });
            } else {
                $(".sweet-overlay").css("display", "none");$(".sweet-alert").css("display", "none");
            }
        });
});


$(document).on('click', '.page_pdf_delete ', function () {
    var page_pdf_delete = $(this).attr('data-id');
    swal({
            title: "Are you sure you want to delete?",
            text: "Your will not be able to recover this pdf!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Delete",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/admin/page_pdf_delete',
                    data: {"id": page_pdf_delete},
                    success: function (msg) {
                        location.reload();
                    }
                });
            } else {
                $(".sweet-overlay").css("display", "none");$(".sweet-alert").css("display", "none");
            }
        });
});
$(document).on('click', '.news_pdf_delete  ', function () {
    var news_pdf_delete = $(this).attr('data-id');
    swal({
            title: "Are you sure you want to delete?",
            text: "Your will not be able to recover this pdf!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Delete",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/admin/news_pdf_delete',
                    data: {"id": news_pdf_delete},
                    success: function (msg) {
                        location.reload();
                    }
                });
            } else {
                $(".sweet-overlay").css("display", "none");$(".sweet-alert").css("display", "none");
            }
        });
});
$(document).on('click', '.news_gallery_delete  ', function () {
    var news_pdf_delete = $(this).attr('data-id');
    var count_pdfs = $(this).attr('data-count');
    if(count_pdfs > 0){
        var subtitle = "Your will not be able to recover this gallery! Images count =" + count_pdfs;
    }else{
        var subtitle = "Your will not be able to recover this page!";
    }
    swal({
            title: "Are you sure you want to delete?",
            text: subtitle,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Delete",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/admin/news_gallery_delete',
                    data: {"id": news_pdf_delete},
                    success: function (msg) {
                        location.reload();
                    }
                });
            } else {
                $(".sweet-overlay").css("display", "none");$(".sweet-alert").css("display", "none");
            }
        });
});


$(document).on('click', '.category_delete', function () {
    var service_id = $(this).attr('data-id');
    swal({
            title: "Are you sure you want to delete?",
            text: "Your will not be able to recover this category!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Delete",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/admin/categories/' + service_id,
                    data: {"id": service_id, "_method": "DELETE"},
                    success: function (msg) {
                        location.reload();
                    }
                });
            } else {
                $(".sweet-overlay").css("display", "none");$(".sweet-alert").css("display", "none");
            }
        });
});
$(document).on('click', '.coach_delete', function () {
    var service_id = $(this).attr('data-id');
    swal({
            title: "Are you sure you want to delete?",
            text: "Your will not be able to recover this coach!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Delete",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/admin/coaches/' + service_id,
                    data: {"id": service_id, "_method": "DELETE"},
                    success: function (msg) {
                        location.reload();
                    }
                });
            } else {
                $(".sweet-overlay").css("display", "none");$(".sweet-alert").css("display", "none");
            }
        });
});
$(document).on('click', '.project_category_delete', function () {
    var service_id = $(this).attr('data-id');
    swal({
            title: "Are you sure you want to delete?",
            text: "Your will not be able to recover this category!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Delete",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/admin/project_categories/' + service_id,
                    data: {"id": service_id, "_method": "DELETE"},
                    success: function (msg) {
                        location.reload();
                    }
                });
            } else {
                $(".sweet-overlay").css("display", "none");$(".sweet-alert").css("display", "none");
            }
        });
});
$(document).on('click', '.page_gallery_delete', function () {
    var page_gallery = $(this).attr('data-id');
    var count_pdfs = $(this).attr('data-count');
    if(count_pdfs > 0){
        var subtitle = "Your will not be able to recover this gallery! Images count =" + count_pdfs;
    }else{
        var subtitle = "Your will not be able to recover this page!";
    }
    swal({
            title: "Are you sure you want to delete?",
            text: subtitle,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Delete",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/admin/page_gallery_delete',
                    data: {"id": page_gallery},
                    success: function (msg) {
                        location.reload();
                    }
                });
            } else {
                $(".sweet-overlay").css("display", "none");$(".sweet-alert").css("display", "none");
            }
        });
});

$(document).on('click', '.article_delete', function () {
    var article_id = $(this).attr('data-id');
    swal({
            title: "Are you sure you want to delete?",
            text: "Your will not be able to recover this article!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Delete",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/admin/articles/' + article_id,
                    data: {"id": article_id, "_method": "DELETE"},
                    success: function (msg) {
                        location.reload();
                    }
                });
            } else {
                $(".sweet-overlay").css("display", "none");$(".sweet-alert").css("display", "none");
            }
        });
});

$(document).on('click', '.comment_delete', function () {
    var service_id = $(this).attr('data-id');
    swal({
            title: "Are you sure you want to delete?",
            text: "Your will not be able to recover this comment!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Delete",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/admin/delete_comments/' + service_id,
                    data: {"id": service_id},
                    success: function (msg) {
                        location.reload();
                    }
                });
            } else {
                $(".sweet-overlay").css("display", "none");$(".sweet-alert").css("display", "none");
            }
        });
});

$(document).on('click', '.user_delete', function () {
    var service_id = $(this).attr('data-id');
    swal({
            title: "Are you sure you want to delete?",
            text: "Your will not be able to recover this user!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Delete",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/admin/delete_user/' + service_id,
                    data: {"id": service_id},
                    success: function (msg) {
                        location.reload();
                    }
                });
            } else {
                $(".sweet-overlay").css("display", "none");$(".sweet-alert").css("display", "none");
            }
        });
});


$(document).on('click', '.page_delete', function () {
    var page_id = $(this).attr('data-id');
    swal({
            title: "Are you sure you want to delete?",
            text: "Your will not be able to recover this page!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Delete",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/admin/pages/' + page_id,
                    data: {"id": page_id, "_method": "DELETE"},
                    success: function (msg) {
                        location.reload();
                    }
                });
            } else {
                $(".sweet-overlay").css("display", "none");$(".sweet-alert").css("display", "none");
            }
        });
});
$(document).on('click', '.blog_delete', function () {
    var page_id = $(this).attr('data-id');
    swal({
            title: "Are you sure you want to delete?",
            text: "Your will not be able to recover this blog!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Delete",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/admin/blog/' + page_id,
                    data: {"id": page_id, "_method": "DELETE"},
                    success: function (msg) {
                        location.reload();
                    }
                });
            } else {
                $(".sweet-overlay").css("display", "none");$(".sweet-alert").css("display", "none");
            }
        });
});
$(document).on('click', '.services_delete', function () {
    var page_id = $(this).attr('data-id');
    swal({
            title: "Are you sure you want to delete?",
            text: "Your will not be able to recover this Service!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Delete",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/admin/services/' + page_id,
                    data: {"id": page_id, "_method": "DELETE"},
                    success: function (msg) {
                        location.reload();
                    }
                });
            } else {
                $(".sweet-overlay").css("display", "none");$(".sweet-alert").css("display", "none");
            }
        });
});
$(document).on('click', '.portfolio_delete', function () {
    var page_id = $(this).attr('data-id');
    swal({
            title: "Are you sure you want to delete?",
            text: "Your will not be able to recover this Portfolio!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Delete",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/admin/portfolio/' + page_id,
                    data: {"id": page_id, "_method": "DELETE"},
                    success: function (msg) {
                        location.reload();
                    }
                });
            } else {
                $(".sweet-overlay").css("display", "none");$(".sweet-alert").css("display", "none");
            }
        });
});
$(document).on('click', '.portfolio_category_delete', function () {
    var page_id = $(this).attr('data-id');
    swal({
            title: "Are you sure you want to delete?",
            text: "Your will not be able to recover this Portfolio Category!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Delete",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/admin/portfolio-category/' + page_id,
                    data: {"id": page_id, "_method": "DELETE"},
                    success: function (msg) {
                        location.reload();
                    }
                });
            } else {
                $(".sweet-overlay").css("display", "none");$(".sweet-alert").css("display", "none");
            }
        });
});

$(document).on('click', '.review_delete', function () {
    var page_id = $(this).attr('data-id');
    swal({
            title: "Are you sure you want to delete?",
            text: "Your will not be able to recover this review!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Delete",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/admin/reviews/' + page_id,
                    data: {"id": page_id, "_method": "DELETE"},
                    success: function (msg) {
                        location.reload();
                    }
                });
            } else {
                $(".sweet-overlay").css("display", "none");$(".sweet-alert").css("display", "none");
            }
        });
});


$(document).on('click', '.page_with_pdf_delete', function () {
    var service_id = $(this).attr('data-id');
    var subtitle = "Your will not be able to recover this page!";

    swal({
            title: "Are you sure you want to delete?",
            text: subtitle,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Delete",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/admin/sub_pages_with_pdf_delete',
                    data: {"id": service_id},
                    success: function (msg) {
                        location.reload();
                    }
                });
            } else {
                $(".sweet-overlay").css("display", "none");$(".sweet-alert").css("display", "none");
            }
        });
});


$(document).on('click', '.news_delete', function () {
    var news_id = $(this).attr('data-id');
    swal({
            title: "Are you sure you want to delete?",
            text: "Your will not be able to recover this news!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Delete",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/admin/news/' + news_id,
                    data: {"id": news_id, "_method": "DELETE"},
                    success: function (msg) {
                        location.reload();
                    }
                });
            } else {
                $(".sweet-overlay").css("display", "none");$(".sweet-alert").css("display", "none");
            }
        });
});
$(document).on('click', '.teams_delete ', function () {
    var news_id = $(this).attr('data-id');
    swal({
            title: "Are you sure you want to delete?",
            text: "Your will not be able to recover this team!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Delete",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/admin/teams/' + news_id,
                    data: {"id": news_id, "_method": "DELETE"},
                    success: function (msg) {
                        location.reload();
                    }
                });
            } else {
                $(".sweet-overlay").css("display", "none");$(".sweet-alert").css("display", "none");
            }
        });
});

$(document).on('click', '.dictionary_delete', function () {
    var id = $(this).attr('data-id');
    swal({
            title: "Are you sure you want to delete?",
            text: "Your will not be able to recover this dictionary!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Delete",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/admin/dictionary/' + id,
                    data: {"id": id, "_method": "DELETE"},
                    success: function (msg) {
                        location.reload();
                    }
                });
            } else {
                $(".sweet-overlay").css("display", "none");$(".sweet-alert").css("display", "none");
            }
        });
});

Dropzone.autoDiscover = false;
$("div#uploadzone").dropzone({
    url: "/admin/upload",
    headers: {
        'X-CSRF-Token': $('input[name="_token"]').val()
    },
    addRemoveLinks: true,
    maxFilesize: 10,
    acceptedFiles: ".jpeg,.jpg,.png,.gif"
});

$("div#uploadBlogZone").dropzone({
    url: "/admin/blog/upload",
    headers: {
        'X-CSRF-Token': $('input[name="_token"]').val()
    },
    addRemoveLinks: true,
    maxFilesize: 10,
    acceptedFiles: ".jpeg,.jpg,.png,.gif"
});
$("div#uploadPortfolioGallery").dropzone({
    url: "/admin/portfolio/upload",
    headers: {
        'X-CSRF-Token': $('input[name="_token"]').val()
    },
    addRemoveLinks: true,
    maxFilesize: 10,
    acceptedFiles: ".jpeg,.jpg,.png,.gif"
});
$("div#uploadProductGallery").dropzone({
    url: "/admin/products/upload",
    headers: {
        'X-CSRF-Token': $('input[name="_token"]').val()
    },
    addRemoveLinks: true,
    maxFilesize: 10,
    acceptedFiles: ".jpeg,.jpg,.png,.gif"
});
$("div#uploadPortfolioPlan").dropzone({
    url: "/admin/portfolio/upload-plan",
    headers: {
        'X-CSRF-Token': $('input[name="_token"]').val()
    },
    addRemoveLinks: true,
    maxFilesize: 10,
    previewTemplate: '<div class="dz-preview dz-file-preview">\n  <div class="dz-image"><img data-dz-thumbnail /></div>\n  <div class="dz-details">\n    <div class="dz-size"><span data-dz-size></span></div>\n    <div class="dz-filename"><span data-dz-name></span></div>\n <input type="hidden" data-dz-name1="" name="planImages[]" value="" class="dz-filename-after"/>  </div>\n  <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>\n <div class="dz-success-mark">\n    <svg width="54px" height="54px" viewBox="0 0 54 54" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">\n      <title>Check</title>\n      <defs></defs>\n      <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">\n        <path d="M23.5,31.8431458 L17.5852419,25.9283877 C16.0248253,24.3679711 13.4910294,24.366835 11.9289322,25.9289322 C10.3700136,27.4878508 10.3665912,30.0234455 11.9283877,31.5852419 L20.4147581,40.0716123 C20.5133999,40.1702541 20.6159315,40.2626649 20.7218615,40.3488435 C22.2835669,41.8725651 24.794234,41.8626202 26.3461564,40.3106978 L43.3106978,23.3461564 C44.8771021,21.7797521 44.8758057,19.2483887 43.3137085,17.6862915 C41.7547899,16.1273729 39.2176035,16.1255422 37.6538436,17.6893022 L23.5,31.8431458 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z" id="Oval-2" stroke-opacity="0.198794158" stroke="#747474" fill-opacity="0.816519475" fill="#FFFFFF" sketch:type="MSShapeGroup"></path>\n      </g>\n    </svg>\n  </div>\n  <div class="dz-error-mark">\n    <svg width="54px" height="54px" viewBox="0 0 54 54" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">\n      <title>Error</title>\n      <defs></defs>\n      <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">\n        <g id="Check-+-Oval-2" sketch:type="MSLayerGroup" stroke="#747474" stroke-opacity="0.198794158" fill="#FFFFFF" fill-opacity="0.816519475">\n          <path d="M32.6568542,29 L38.3106978,23.3461564 C39.8771021,21.7797521 39.8758057,19.2483887 38.3137085,17.6862915 C36.7547899,16.1273729 34.2176035,16.1255422 32.6538436,17.6893022 L27,23.3431458 L21.3461564,17.6893022 C19.7823965,16.1255422 17.2452101,16.1273729 15.6862915,17.6862915 C14.1241943,19.2483887 14.1228979,21.7797521 15.6893022,23.3461564 L21.3431458,29 L15.6893022,34.6538436 C14.1228979,36.2202479 14.1241943,38.7516113 15.6862915,40.3137085 C17.2452101,41.8726271 19.7823965,41.8744578 21.3461564,40.3106978 L27,34.6568542 L32.6538436,40.3106978 C34.2176035,41.8744578 36.7547899,41.8726271 38.3137085,40.3137085 C39.8758057,38.7516113 39.8771021,36.2202479 38.3106978,34.6538436 L32.6568542,29 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z" id="Oval-2" sketch:type="MSShapeGroup"></path>\n        </g>\n      </g>\n    </svg>\n  </div>\n</div>',
    acceptedFiles: ".jpeg,.jpg,.png,.gif"
});

$("div#partnerzone").dropzone({
    url: "/admin/partners/upload",
    headers: {
        'X-CSRF-Token': $('input[name="_token"]').val()
    },
    addRemoveLinks: true,
    maxFiles: 1,
    maxFilesize: 10,
    previewTemplate: 'dddd',
    acceptedFiles: ".jpeg,.jpg,.png,.gif",
    maxfilesexceeded: function (file) {
        this.removeFile(file);
    }
});
$("div#gallerydzone").dropzone({
    url: "/admin/companies/upload",
    headers: {
        'X-CSRF-Token': $('input[name="_token"]').val()
    },
    addRemoveLinks: true,
    maxFilesize: 10,
    acceptedFiles: ".jpeg,.jpg,.png,.gif",
    maxfilesexceeded: function (file) {
        this.removeFile(file);
    }
});
$(".old_img_remove").click(function () {
    $(this).parent(".dz-preview").remove();
});
