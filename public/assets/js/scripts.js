
$(document).ready(function(){

    $(".open-sub").on("click", function () {
       $(this).next().slideToggle(200);
    });

    $(".service-tab").on("click", function () {
        const key = $(this).attr("data-key");
        $(".service-tab").removeClass("active");
        $(".service-tab-" + key).addClass("active");
        $(".services-info-box").removeClass("show-service");
        $(".services-info-box-" + key).addClass("show-service");
    })

    $(".contact_btn").on("click",function () {
        var text = $(".contact_question").val();
        var result = true;
        if(text == ''){
            result = false;
            $(".form-msg .empty").removeClass("d-none");
        } else {
            $(".form-msg .empty").addClass("d-none");
        }

        if(result) {
            $(".send_form").submit();
        }
    })
});

function left_carusel(carusel) {
    var block_width = $(carusel).find('.carousel-block').outerWidth();
    jQuery(carusel).find(".carousel-items .carousel-block").eq(-1).clone().prependTo($(carusel).find(".carousel-items"));
    jQuery(carusel).find(".carousel-items").css({"left": "-" + block_width + "px"});
    jQuery(carusel).find(".carousel-items .carousel-block").eq(-1).remove();
    jQuery(carusel).find(".carousel-items").animate({left: "0px"}, 200);

}

function right_carusel(carusel) {
    var block_width = $(carusel).find('.carousel-block').outerWidth();
    jQuery(carusel).find(".carousel-items").animate({left: "-" + block_width + "px"}, 200, function () {
        jQuery(carusel).find(".carousel-items .carousel-block").eq(0).clone().appendTo(jQuery(carusel).find(".carousel-items"));
        jQuery(carusel).find(".carousel-items .carousel-block").eq(0).remove();
        jQuery(carusel).find(".carousel-items").css({"left": "0px"});
    });
}

jQuery(function () {
    auto_right('.carousel');
    auto_right('.about-us-slider');

    if($('.recent_products_carousel .carousel-block').length > 1){
        auto_right_related('.recent_products_carousel');
    }
    if($('.carousel_related_mobile .carousel-block').length > 1){
        auto_right_related('.carousel_related_mobile');
    }
    if($('.carousel_recomended .carousel-block').length > 3){
        auto_right_recomended('.carousel_recomended');
    }
    if($('.carousel_recomended_mobile .carousel-block').length > 1){
        auto_right_recomended('.carousel_recomended_mobile');
    }

    auto_right_category('.carousel_category');
    auto_right_category('.mobile_sloder');
    auto_right_category('.the_best_carusel');
});

function auto_right(carusel) {
    setInterval(function () {
        if (!jQuery(carusel).is('.hover'))
            right_carusel(carusel);
    }, 3000)
}
function auto_right_related(carusel) {
    setInterval(function () {
        if (!jQuery(carusel).is('.hover'))
            right_carusel(carusel);
    }, 5000)
}
function auto_right_recomended(carusel) {
    setInterval(function () {
        if (!jQuery(carusel).is('.hover'))
            right_carusel(carusel);
    }, 5500)
}
function auto_right_category(carusel) {
    setInterval(function () {
        if (!jQuery(carusel).is('.hover'))
            right_carusel(carusel);
    }, 6500)
}

jQuery(document).on('mouseenter', '.carousel', function () {
    jQuery(this).addClass('hover')
});
jQuery(document).on('mouseleave', '.carousel', function () {
    jQuery(this).removeClass('hover')
})

jQuery(document).on('click', ".carousel-button-right", function () {
    var carusel = jQuery(this).parents('.recent_products_carousel');
    right_carusel(carusel);
    return false;
});
jQuery(document).on('click', ".carousel-button-left", function () {
    var carusel = jQuery(this).parents('.recent_products_carousel');
    left_carusel(carusel);
    return false;
});