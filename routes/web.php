<?php

use App\Http\Controllers\Cellar\DashboardController as CellarDashboardController;
use App\Http\Controllers\Cellar\LoginController as CellarAuthController;
use App\Http\Controllers\Cellar\PagesController as CellarPagesController;
use App\Http\Controllers\Cellar\PageGalleryController as CellarPageGalleryController;
use App\Http\Controllers\Cellar\SliderController as CellarSliderController;
use App\Http\Controllers\Cellar\DictionaryController as CellarDictionaryController;
use App\Http\Controllers\Cellar\BlogController as CellarBlogController;
use App\Http\Controllers\Cellar\ServiceController as CellarServiceController;
use App\Http\Controllers\Cellar\OurTeamController as CellarOurTeamController;
use App\Http\Controllers\Cellar\PortfolioController as CellarPortfolioController;
use App\Http\Controllers\Cellar\PortfolioCategoryController as CellarPortfolioCategoryController;
use App\Http\Controllers\Cellar\BrandController as CellarBrandsController;
use App\Http\Controllers\Cellar\Products\CategoryController as CellarProductCategoryController;
use App\Http\Controllers\Cellar\Products\AttributeController as CellarProductAttributeController;
use App\Http\Controllers\Cellar\Products\ProductController as CellarProductController;
use App\Http\Controllers\Cellar\Products\OrderController as CellarOrderController;
use App\Http\Controllers\Cellar\DeliveryCitiesController as CellarDeliveryCitiesController;
use App\Http\Controllers\Front\HomeController as HomeController;
use App\Http\Controllers\Front\AboutController as AboutController;
use App\Http\Controllers\Front\ServicesController as ServicesController;
use App\Http\Controllers\Front\ContactsController as ContactsController;
use App\Http\Controllers\Front\BlogController as BlogController;
use App\Http\Controllers\Front\PortfolioController as FrontPortfolioController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

/* Frontend routes */
Route::get('/', [HomeController::class, 'index']);
Route::get('/about', [AboutController::class, 'index']);
Route::get('/services', [ServicesController::class, 'index']);
Route::get('/contacts', [ContactsController::class, 'index']);
Route::get('/blog', [BlogController::class, 'index']);
Route::get('/blog/{slug}', [BlogController::class, 'single']);
Route::get('/portfolio', [FrontPortfolioController::class, 'index']);
Route::get('/portfolio/{slug}', [FrontPortfolioController::class, 'single']);

/*----*/

Route::get('/admin', [CellarAuthController::class, 'index']);
Route::post('/admin', [CellarAuthController::class, 'signIn']);


Route::group(['middleware'=> ['admin'], 'prefix'=>'admin'], function () {
    Route::get('/logout', [CellarAuthController::class, 'logout']);
    //Route::get('/orders', [CellarPageGalleryController::class, 'create'])->name("createPageGallery");
    //Route::get('/', [CellarDashboardController::class, 'index'])->name("dashboard");
    Route::get('/dashboard', [CellarDashboardController::class, 'index'])->name("dashboard");
    Route::resources([
        '/pages' => CellarPagesController::class,
        '/slider' => CellarSliderController::class,
        '/dictionary' => CellarDictionaryController::class,
        '/blog' => CellarBlogController::class,
        '/services' => CellarServiceController::class,
        '/our-team' => CellarOurTeamController::class,
        '/portfolio' => CellarPortfolioController::class,
        '/portfolio-category' => CellarPortfolioCategoryController::class,
        '/brands' => CellarBrandsController::class,
        '/product-category' => CellarProductCategoryController::class,
        '/product-attribute' => CellarProductAttributeController::class,
        '/products' => CellarProductController::class,
        '/orders' => CellarOrderController::class,
        '/delivery-cities' => CellarDeliveryCitiesController::class,
    ]);
    //Product Attributes
    Route::post('/products/get-attrs', [CellarProductController::class, 'getAttrByCatId']);
    Route::post('/products/upload', [CellarProductController::class, 'uploadGallery']);

    Route::get('/page-gallery/create/{parent}', [CellarPageGalleryController::class, 'create'])->name("createPageGallery");
    Route::post('/page-gallery/store/{parent}', [CellarPageGalleryController::class, 'store'])->name("storePageGallery");
    Route::get('/page-gallery/edit/{gallery_id}', [CellarPageGalleryController::class, 'edit'])->name("editPageGallery");
    Route::get('/page-gallery/edit/{gallery_id}', [CellarPageGalleryController::class, 'edit'])->name("editPageGallery");
    Route::post('/page-gallery/edit/{gallery_id}', [CellarPageGalleryController::class, 'update'])->name("updatePageGallery");
    Route::post('/upload', [CellarPageGalleryController::class, 'upload'])->name("uploadPageGallery");
    Route::post('/blog/upload', [CellarBlogController::class, 'upload'])->name("uploadBlogGallery");
    Route::post('/portfolio/upload', [CellarPortfolioController::class, 'uploadGallery'])->name("uploadPortfolioGallery");
    Route::post('/portfolio/upload-plan', [CellarPortfolioController::class, 'uploadPlan'])->name("uploadPortfolioPlan");



    /*Route::resource('/slider', 'Admin\SliderController');
    Route::post('/slider/changeHidden', 'Admin\SliderController@changeHidden');
    Route::post('/slider/slide_delete', 'Admin\SliderController@slide_delete');
    Route::get('/slide/edit/{token}', 'Admin\SliderController@edit_slide')->name("slide_edit");
    Route::get('/slide/create/{token}', 'Admin\SliderController@create')->name("slide_edit");
    Route::get('/slide/update', 'Admin\SliderController@update_slide');
    Route::post('/slide/update', 'Admin\SliderController@update_slide');*/

});
/*Route::group(['middleware'=> ['admin'], 'prefix'=>'cellar'], function () {


    Route::get('/upload_content', 'Admin\DashboardController@upload_content');
    Route::post('/upload_content', 'Admin\DashboardController@upload_content');

    Route::get('/homepage', 'Admin\HomePageController@index')->name("home_page");
    Route::post('/homepage', 'Admin\HomePageController@save')->name("home_page");

    Route::resource('/pa-orders', 'Admin\ProductTypeController');



    Route::post('/pages/addCalendarRow', 'Admin\PagesController@addCalendarRow');
    Route::post('/package_add', 'Admin\PackageController@packageAdd');
    Route::resource('/package', 'Admin\PackageController');
    Route::resource('/prcats', 'Admin\ProductCatController');
    Route::resource('/brands', 'Admin\BrandController');

    Route::resource('/product-attribute', 'Admin\ProductAttributeController');
    Route::resource('/product-attribute-value', 'Admin\ProductAttributeValueController');
    Route::resource('/products', 'Admin\ProductController');
    Route::post('/products/get-attrs', 'Admin\ProductController@getAttrByCatId');
    Route::resource('/coachcat', 'Admin\CoachCategoryController');
    Route::resource('/coaches', 'Admin\CoachController');
    Route::resource('/teams', 'Admin\TeamController');
    Route::resource('/career', 'Admin\CareerController');
    Route::resource('/all', 'Admin\AllFunctionsController');
    Route::get('/logout', [CellarAuthController::class, 'logout']);



    Route::resource('/categories', 'Admin\CategoriesController');
    Route::post('/categories/changeHidden', 'Admin\CategoriesController@changeHidden');
    Route::resource('/project_categories', 'Admin\ProjectCategoriesController');
    Route::post('/project_categories/changeHidden', 'Admin\ProjectCategoriesController@changeHidden');

    Route::resource('/pages', 'Admin\PagesController');
    Route::resource('/reviews', 'Admin\ReviewsController');

    Route::resource('/news', 'Admin\NewsController');

    Route::resource('/articles', 'Admin\ArticlesController');
    Route::get('articles/add_pdf', 'Admin\ArticlesController@add_pdf');
    Route::post('articles/add_pdf', 'Admin\ArticlesController@add_pdf');

    Route::resource('/media', 'Admin\MediaController');

    Route::get('/about_us', 'Admin\AboutController@index')->name('about_us');
    Route::post('/about_us', 'Admin\AboutController@save');

    Route::post('/services/changeHidden', 'Admin\ServicesController@changeHidden');
    Route::resource('/services', 'Admin\ServicesController');
    Route::get('services/add_sub_pages', 'Admin\ServicesController@add_sub_pages');
    Route::post('services/add_sub_pages', 'Admin\ServicesController@add_sub_pages');


    Route::get('/contact', 'Admin\ContactController@index')->name('contact_page');
    Route::post('/contact', 'Admin\ContactController@save');

    Route::resource('/projects', 'Admin\ProjectsController');
    Route::get('/upload', 'Admin\PageGalleryController@upload');
    Route::post('/upload', 'Admin\PageGalleryController@upload');
    Route::post('/projects/changeHidden', 'Admin\ProjectsController@changeHidden');

    Route::resource('/partners', 'Admin\PartnersController');
    Route::get('/partners/upload', 'Admin\PartnersController@upload');
    Route::post('/partners/upload', 'Admin\PartnersController@upload');
    Route::post('/partners/changeHidden', 'Admin\PartnersController@changeHidden');

    Route::resource('/companies', 'Admin\CompaniesController');
    Route::get('/companies/upload', 'Admin\CompaniesController@upload');
    Route::post('/companies/upload', 'Admin\CompaniesController@upload');
    Route::post('/companies/changeHidden', 'Admin\CompaniesController@changeHidden');

    Route::resource('/menu', 'Admin\MenuController');
    Route::get('/menu/save_menu', 'Admin\MenuController@save_menu');
    Route::post('/menu/save_menu', 'Admin\MenuController@save_menu');
    Route::get('/menu/add_link_to_menu', 'Admin\MenuController@add_link_to_menu');
    Route::post('/menu/add_link_to_menu', 'Admin\MenuController@add_link_to_menu');
    Route::resource('/slider', 'Admin\SliderController');
    Route::post('/slider/changeHidden', 'Admin\SliderController@changeHidden');
    Route::post('/slider/slide_delete', 'Admin\SliderController@slide_delete');
    Route::get('/slide/edit/{token}', 'Admin\SliderController@edit_slide')->name("slide_edit");
    Route::get('/slide/create/{token}', 'Admin\SliderController@create')->name("slide_edit");
    Route::get('/slide/update', 'Admin\SliderController@update_slide');
    Route::post('/slide/update', 'Admin\SliderController@update_slide');
    Route::get('/comments', 'Admin\CommentsController@index');
    Route::post('/comments/changeHidden', 'Admin\CommentsController@changeHidden');
    Route::get('/delete_comments/{token}', 'Admin\CommentsController@destroy');
    Route::post('/delete_comments/{token}', 'Admin\CommentsController@destroy');
    Route::resource('/authors', 'Admin\AuthorsController');
    Route::get('authors/add_author', 'Admin\AuthorsController@add_author');
    Route::post('authors/add_author', 'Admin\AuthorsController@add_author');
    Route::get('/users', 'Admin\UsersController@index')->name("users");
    Route::get('/users/{token}', 'Admin\UsersController@show')->name("users");
    Route::get('/delete_user/{token}', 'Admin\UsersController@destroy');
    Route::post('/delete_user/{token}', 'Admin\UsersController@destroy');
    Route::get('/upload/content', 'Admin\DashboardController@upload');
    Route::post('/upload/content', 'Admin\DashboardController@upload');
    Route::resource('/glossary', 'Admin\GlossaryController');
    Route::get('/settings', 'Admin\SettingsController@index')->name("settings");
    Route::post('/settings', 'Admin\SettingsController@save');
    Route::get('/settings/{id}/save', 'Admin\SettingsController@update');
    Route::post('/settings/{id}/save', 'Admin\SettingsController@update');
    Route::get('/settings/delete/{id}', 'Admin\SettingsController@delete');
    Route::post('/settings/delete/{id}', 'Admin\SettingsController@delete');
    Route::get('/sub_pages/{parent}', 'Admin\SubPageController@index');
    Route::get('/sub_pages/{parent}/create', 'Admin\SubPageController@create');
    Route::post('/sub_pages/{parent}/create', 'Admin\SubPageController@store');
    Route::get('/sub_pages/{parent}/edit/{id}', 'Admin\SubPageController@edit');
    Route::get('/sub_pages/{id}/update', 'Admin\SubPageController@update');
    Route::post('/sub_pages/{id}/update', 'Admin\SubPageController@update');
    Route::get('/sub_pdf/{parent}', 'Admin\PagePdfController@index');
    Route::get('/sub_pdf/{parent}/create', 'Admin\PagePdfController@create');
    Route::post('/sub_pdf/{parent}/create', 'Admin\PagePdfController@store');
    Route::get('/sub_pdf/{parent}/edit/{id}', 'Admin\PagePdfController@edit');
    Route::post('/sub_pdf/{parent}/edit/{id}', 'Admin\PagePdfController@update');
    Route::get('/page_pdf_delete', 'Admin\PagePdfController@delete_pdf');
    Route::post('/page_pdf_delete', 'Admin\PagePdfController@delete_pdf');
    Route::get('/news_pdf/{parent}', 'Admin\NewsPdfController@index');
    Route::get('/news_pdf/{parent}/create', 'Admin\NewsPdfController@create');
    Route::post('/news_pdf/{parent}/create', 'Admin\NewsPdfController@store');
    Route::get('/news_pdf/{parent}/edit/{id}', 'Admin\NewsPdfController@edit');
    Route::post('/news_pdf/{parent}/edit/{id}', 'Admin\NewsPdfController@update');
    Route::get('/news_pdf_delete', 'Admin\NewsPdfController@delete_pdf');
    Route::post('/news_pdf_delete', 'Admin\NewsPdfController@delete_pdf');
    Route::get('/news_sub_page_pdf/{parent}', 'Admin\NewsSubPageController@index')->name('news_sub_page_pdf');
    Route::get('/news_sub_page_pdf/{parent}/create', 'Admin\NewsSubPageController@create')->name('news_sub_page_pdf');
    Route::post('/news_sub_page_pdf/{parent}/create', 'Admin\NewsSubPageController@store')->name('news_sub_page_pdf');
    Route::get('/news_sub_page_pdf/{parent}/edit/{id}', 'Admin\NewsSubPageController@edit')->name('news_sub_page_pdf');
    Route::post('/news_sub_page_pdf/{parent}/edit/{id}', 'Admin\NewsSubPageController@update')->name('news_sub_page_pdf');
    Route::get('/news_sub_page_pdf_delete', 'Admin\NewsSubPageController@news_sub_page_pdf_delete')->name('news_sub_page_pdf');
    Route::post('/news_sub_page_pdf_delete', 'Admin\NewsSubPageController@news_sub_page_pdf_delete')->name('news_sub_page_pdf');

    Route::post('/news_sub_page_pdf/plus_pdf', 'Admin\NewsSubPageController@plus_pdf')->name('news_sub_page_pdf');

    Route::get('/news_sub_page_pdf/upload_pdf', 'Admin\NewsSubPageController@upload_pdf')->name('news_sub_page_pdf');
    Route::post('/news_sub_page_pdf/upload_pdf', 'Admin\NewsSubPageController@upload_pdf')->name('news_sub_page_pdf');

    Route::get('/news_sub_page_pdf/delete_sub_pdf', 'Admin\NewsSubPageController@delete_sub_pdf');
    Route::post('/news_sub_page_pdf/delete_sub_pdf', 'Admin\NewsSubPageController@delete_sub_pdf');
    Route::get('/news_sub_page_pdf/edit_sub_pdf_block', 'Admin\NewsSubPageController@edit_sub_pdf_block')->name('news_sub_page_pdf');
    Route::post('/news_sub_page_pdf/edit_sub_pdf_block', 'Admin\NewsSubPageController@edit_sub_pdf_block')->name('news_sub_page_pdf');
    Route::get('/news_sub_page_pdf/update_sub_pdf', 'Admin\NewsSubPageController@update_sub_pdf')->name('news_sub_page_pdf');
    Route::post('/news_sub_page_pdf/update_sub_pdf', 'Admin\NewsSubPageController@update_sub_pdf')->name('news_sub_page_pdf');

    Route::get('/sub_pages_with_pdf/{parent}', 'Admin\PageSubWithPdfController@index')->name('sub_pages_with_pdf');
    Route::get('/sub_pages_with_pdf/{parent}/create', 'Admin\PageSubWithPdfController@create')->name('sub_pages_with_pdf');
    Route::post('/sub_pages_with_pdf/{parent}/create', 'Admin\PageSubWithPdfController@store')->name('sub_pages_with_pdf');
    Route::get('/sub_pages_with_pdf/{parent}/edit/{id}', 'Admin\PageSubWithPdfController@edit')->name('sub_pages_with_pdf');
    Route::post('/sub_pages_with_pdf/{parent}/edit/{id}', 'Admin\PageSubWithPdfController@update')->name('sub_pages_with_pdf');
    Route::get('/sub_pages_with_pdf_delete', 'Admin\PageSubWithPdfController@news_sub_page_pdf_delete')->name('sub_pages_with_pdf');
    Route::post('/sub_pages_with_pdf_delete', 'Admin\PageSubWithPdfController@news_sub_page_pdf_delete')->name('sub_pages_with_pdf');

    Route::post('/sub_pages_with_pdf/plus_pdf', 'Admin\PageSubWithPdfController@plus_pdf')->name('sub_pages_with_pdf');

    Route::get('/sub_pages_with_pdf/upload_pdf', 'Admin\PageSubWithPdfController@upload_pdf')->name('sub_pages_with_pdf');
    Route::post('/sub_pages_with_pdf/upload_pdf', 'Admin\PageSubWithPdfController@upload_pdf')->name('sub_pages_with_pdf');

    Route::get('/sub_pages_with_pdf/delete_sub_pdf', 'Admin\PageSubWithPdfController@delete_sub_pdf');
    Route::post('/sub_pages_with_pdf/delete_sub_pdf', 'Admin\PageSubWithPdfController@delete_sub_pdf');
    Route::get('/sub_pages_with_pdf/edit_sub_pdf_block', 'Admin\PageSubWithPdfController@edit_sub_pdf_block')->name('sub_pages_with_pdf');
    Route::post('/sub_pages_with_pdf/edit_sub_pdf_block', 'Admin\PageSubWithPdfController@edit_sub_pdf_block')->name('sub_pages_with_pdf');
    Route::get('/sub_pages_with_pdf/update_sub_pdf', 'Admin\PageSubWithPdfController@update_sub_pdf')->name('sub_pages_with_pdf');
    Route::post('/sub_pages_with_pdf/update_sub_pdf', 'Admin\PageSubWithPdfController@update_sub_pdf')->name('sub_pages_with_pdf');


    Route::get('/page_gallery/add/{id}', 'Admin\PageGalleryController@add_gallery');
    Route::post('/page_gallery/add/{id}', 'Admin\PageGalleryController@storeGallery');
    Route::get('/page_gallery_delete', 'Admin\PageGalleryController@gallery_delete');
    Route::post('/page_gallery_delete', 'Admin\PageGalleryController@gallery_delete');
    Route::get('/page_gallery/edit/{id}', 'Admin\PageGalleryController@edit_gallery');
    Route::post('/page_gallery/edit/{id}', 'Admin\PageGalleryController@updateGallery');


    //Sub page gallery
    Route::get('/sub_page_gallery/add/{id}', 'Admin\SubPageGalleryController@add_gallery');
    Route::post('/sub_page_gallery/add/{id}', 'Admin\SubPageGalleryController@storeGallery');
    Route::get('/sub_page_gallery_delete', 'Admin\SubPageGalleryController@gallery_delete');
    Route::post('/sub_page_gallery_delete', 'Admin\SubPageGalleryController@gallery_delete');
    Route::get('/sub_page_gallery/edit/{id}', 'Admin\SubPageGalleryController@edit_gallery');
    Route::post('/sub_page_gallery/edit/{id}', 'Admin\SubPageGalleryController@updateGallery');


    Route::get('/news_gallery/add/{id}', 'Admin\NewsGalleryController@add_gallery');
    Route::post('/news_gallery/add/{id}', 'Admin\NewsGalleryController@storeGallery');
    Route::get('/news_gallery_delete', 'Admin\NewsGalleryController@gallery_delete');
    Route::post('/news_gallery_delete', 'Admin\NewsGalleryController@gallery_delete');

    Route::get('/news_gallery/edit/{id}', 'Admin\NewsGalleryController@edit_gallery');
    Route::post('/news_gallery/edit/{id}', 'Admin\NewsGalleryController@updateGallery');



    Route::get('/admin_users', 'Admin\AdminUsersController@index')->name("admin_users");
    Route::get('/admin_users/create', 'Admin\AdminUsersController@create')->name("admin_users");
    Route::post('/admin_users/create', 'Admin\AdminUsersController@store');
    Route::get('/admin_users/edit/{id}', 'Admin\AdminUsersController@edit')->name("admin_users");
    Route::post('/admin_users/edit/{id}', 'Admin\AdminUsersController@update');
    Route::get('/admin_users_delete', 'Admin\AdminUsersController@delete');
    Route::post('/admin_users_delete', 'Admin\AdminUsersController@delete');


    Route::get('/hidden_check', 'Admin\DashboardController@hidden_check');
    Route::post('/hidden_check', 'Admin\DashboardController@hidden_check');
    Route::get('/checked_delete', 'Admin\DashboardController@checked_delete');
    Route::post('/checked_delete', 'Admin\DashboardController@checked_delete');

    Route::get('/orders', 'Admin\DashboardController@donateIrders');
    Route::post('/orders', 'Admin\DashboardController@donateIrders');
    Route::get('/orders-complited', 'Admin\DashboardController@complitedorders');
    Route::get('/orders/complite/{token}', 'Admin\DashboardController@compliteOrder');

});*/
