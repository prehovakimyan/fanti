import { defineConfig } from 'vite';
import laravel from 'laravel-vite-plugin';

export default defineConfig({
    plugins: [
        laravel({
            input: [
                'resources/css/cellar/base.css',
                'resources/js/cellar/html5shiv.min.js',
                'resources/js/cellar/respond.min.js',
                'resources/js/cellar/wow.min.js',
                'resources/js/cellar/common_scripts_min.js',
                'resources/js/cellar/functions.js',
            ],
            refresh: true,
        }),
    ],
});
