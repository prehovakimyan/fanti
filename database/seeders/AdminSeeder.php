<?php

namespace Database\Seeders;

use App\Models\Cellar\Admin;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $admin1 = new Admin();
        $admin1->name = 'Paruyr';
        $admin1->username = 'admin';
        $admin1->password = md5('mypass');
        $admin1->role = 1;
        $admin1->save();
    }
}
