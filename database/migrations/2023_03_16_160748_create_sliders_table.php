<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('sliders', function (Blueprint $table) {
            $table->id();
            $table->string('image');
            $table->integer('item_order');
            $table->string('slide_title_am');
            $table->string('slide_title_ru');
            $table->string('slide_title_en');
            $table->string('slide_sub_title_am')->nullable();
            $table->string('slide_sub_title_ru')->nullable();
            $table->string('slide_sub_title_en')->nullable();
            $table->string('slide_link')->nullable();
            $table->string('new_blank')->default(0);
            $table->string('hidden')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('sliders');
    }
};
