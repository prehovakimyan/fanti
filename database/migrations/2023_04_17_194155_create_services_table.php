<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('services', function (Blueprint $table) {
            $table->id();
            $table->string('title_am');
            $table->string('title_ru');
            $table->string('title_en');
            $table->string('short_desc_am')->nullable();
            $table->string('short_desc_ru')->nullable();
            $table->string('short_desc_en')->nullable();
            $table->text('desc_am')->nullable();
            $table->text('desc_ru')->nullable();
            $table->text('desc_en')->nullable();
            $table->string('image')->nullable();
            $table->string('price')->nullable();
            $table->string('person_1_am')->nullable();
            $table->string('person_1_ru')->nullable();
            $table->string('person_1_en')->nullable();
            $table->string('person_2_am')->nullable();
            $table->string('person_2_ru')->nullable();
            $table->string('person_2_en')->nullable();
            $table->string('person_3_am')->nullable();
            $table->string('person_3_ru')->nullable();
            $table->string('person_3_en')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('services');
    }
};
