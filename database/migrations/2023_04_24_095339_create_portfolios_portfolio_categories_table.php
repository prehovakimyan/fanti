<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('portfolios_portfolio_categories', function (Blueprint $table) {
            $table->unsignedBigInteger('portfolio_id');
            $table->unsignedBigInteger('portfolio_category_id');

            //FOREIGN KEY CONSTRAINTS
            $table->foreign('portfolio_id')->references('id')->on('portfolios')->onDelete('cascade');
            $table->foreign('portfolio_category_id')->references('id')->on('portfolio_categories')->onDelete('cascade');

            //SETTING THE PRIMARY KEYS
            $table->primary(['portfolio_id','portfolio_category_id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('portfolios_portfolio_categories');
    }
};
