<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('title_am');
            $table->string('title_ru');
            $table->string('title_en');
            $table->string('short_desc_am')->nullable();
            $table->string('short_desc_ru')->nullable();
            $table->string('short_desc_en')->nullable();
            $table->string('desc_am')->nullable();
            $table->string('desc_ru')->nullable();
            $table->string('desc_en')->nullable();
            $table->string('image');
            $table->string('pr_code');
            $table->string('price_unit_1');
            $table->string('price_1');
            $table->string('new_price_1')->nullable();
            $table->string('price_unit_2')->nullable();
            $table->string('price_2')->nullable();
            $table->string('new_price_2')->nullable();
            $table->string('video')->nullable();
            $table->tinyInteger('in_stock')->default(1);
            $table->integer('available_count')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('products');
    }
};
