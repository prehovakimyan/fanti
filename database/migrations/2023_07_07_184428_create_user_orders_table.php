<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('user_orders', function (Blueprint $table) {
            $table->id();
            $table->string('user_id')->default(0);
            $table->string('name_surname');
            $table->string('email');
            $table->string('phone');
            $table->text('comment')->nullable();
            $table->text('payment_method');
            $table->text('delivery_type');
            $table->integer('delivery_city')->default(0);
            $table->integer('deliver_address')->nullable();
            $table->tinyInteger('status')->nullable(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('user_orders');
    }
};
