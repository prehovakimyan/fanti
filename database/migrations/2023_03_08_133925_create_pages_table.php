<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->id();
            $table->string('title_am');
            $table->string('title_ru');
            $table->string('title_en');
            $table->string('short_desc_am')->nullable();
            $table->string('short_desc_ru')->nullable();
            $table->string('short_desc_en')->nullable();
            $table->string('description_am')->nullable();
            $table->string('description_ru')->nullable();
            $table->string('description_en')->nullable();
            $table->string('slug');
            $table->string('image')->nullable();
            $table->string('video')->nullable();
            $table->tinyInteger('sub_pages')->default(0);
            $table->tinyInteger('pdf_uploder')->default(0);
            $table->tinyInteger('gallery')->default(0);
            $table->integer('parent')->default(0);
            $table->tinyInteger('hidden')->default(0);
            $table->tinyInteger('hide_in_menu')->default(0);
            $table->string('meta_title_am')->nullable();
            $table->string('meta_title_ru')->nullable();
            $table->string('meta_title_en')->nullable();
            $table->string('meta_desc_am')->nullable();
            $table->string('meta_desc_ru')->nullable();
            $table->string('meta_desc_en')->nullable();
            $table->string('meta_key_am')->nullable();
            $table->string('meta_key_ru')->nullable();
            $table->string('meta_key_en')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('pages');
    }
};
