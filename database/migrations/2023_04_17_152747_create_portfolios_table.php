<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('portfolios', function (Blueprint $table) {
            $table->id();
            $table->string('title_am');
            $table->string('title_ru');
            $table->string('title_en');
            $table->text('short_desc_am');
            $table->text('short_desc_ru');
            $table->text('short_desc_en');
            $table->text('desc_am');
            $table->text('desc_ru');
            $table->text('desc_en');
            $table->timestamp('project_date')->nullable();
            $table->tinyInteger('status')->default(0);
            $table->string('location_am')->nullable();
            $table->string('location_ru')->nullable();
            $table->string('location_en')->nullable();
            $table->text('video_desc_am')->nullable();
            $table->text('video_desc_ru')->nullable();
            $table->text('video_desc_en')->nullable();
            $table->string('video_link')->nullable();
            $table->string('image');
            $table->string('slug');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('portfolios');
    }
};
