<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    use HasFactory;

    public function attributes()
    {
        return $this->belongsToMany(ProductAttribute::class,'product_categories_attributes');
    }
}
