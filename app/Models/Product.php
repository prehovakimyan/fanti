<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    public function images()
    {
        return $this->belongsToMany(ProductImage::class,'products_product_images');
    }

    public function categories()
    {
        return $this->belongsToMany(ProductCategory::class,'products_product_categories');
    }


}
