<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Portfolio extends Model
{
    use HasFactory;

    public function images()
    {
        return $this->belongsToMany(Image::class,'portfolios_images');
    }
    public function planImages()
    {
        return $this->belongsToMany(PlanImage::class,'portfolios_plan_images');
    }

    public function categories()
    {
        return $this->belongsToMany(PortfolioCategory::class,'portfolios_portfolio_categories');
    }

}
