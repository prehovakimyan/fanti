<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductAttribute extends Model
{
    use HasFactory;

    public function categories()
    {
        return $this->belongsToMany(ProductCategory::class,'product_categories_attributes');
    }
}
