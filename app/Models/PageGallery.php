<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PageGallery extends Model
{
    use HasFactory;

    public function images()
    {
        return $this->belongsTo(Gallery::class,'parent_id');
    }

    public function parentPage()
    {
        return $this->belongsTo(Page::class,'page_id');
    }

}
