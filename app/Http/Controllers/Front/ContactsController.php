<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Language;
use Illuminate\Http\Request;

class ContactsController extends Controller
{
    public function index(Request $request){
        $first_lang = Language::where('first', '=', 1)->where('hidden', '=', 0)->first();
        $lang = $first_lang->short;

        return view('front.contacts', compact('lang'));
    }
}
