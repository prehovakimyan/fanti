<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Dictionary;

class DictionaryController extends Controller
{
    public function getTranslate($short_code,$lang){
        $get_glssary = Dictionary::select("value_$lang as translate")->where('short_code','=', $short_code)->first();

        if ($get_glssary) {
        return $get_glssary->translate;
        }
        else {
            return "*_".$short_code."_*";
        }
    }
}
