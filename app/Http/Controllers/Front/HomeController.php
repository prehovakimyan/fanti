<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Language;
use App\Models\Slider;
use Illuminate\Http\Request;
use function Termwind\render;

class HomeController extends Controller
{
    public function index(Request $request){
        $sliderObj = Slider::where('hidden', '=', 0)->orderBy('item_order', 'ASC')->get();
        $first_lang = Language::where('first', '=', 1)->where('hidden', '=', 0)->first();
        $lang = $first_lang->short;

        return view('front.home', compact('sliderObj', 'lang'));
    }
}
