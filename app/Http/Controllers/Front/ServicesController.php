<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Language;
use App\Models\Service;
use Illuminate\Http\Request;

class ServicesController extends Controller
{
    public function index(Request $request){
        $first_lang = Language::where('first', '=', 1)->where('hidden','=', 0)->first();
        $lang = $first_lang->short;
        $services = Service::all();
        return view('front.services', compact("services",'lang'));
    }
}
