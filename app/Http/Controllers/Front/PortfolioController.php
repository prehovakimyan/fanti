<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Language;
use App\Models\Portfolio;
use Illuminate\Http\Request;

class PortfolioController extends Controller
{

    public function index(Request $request) {
        $first_lang = Language::where('first', '=', 1)->where('hidden','=', 0)->first();
        $lang = $first_lang->short;
        $portfolios = Portfolio::all();
        return view('front.portfolio', compact('lang', 'portfolios'));
    }

    public function single(Request $request, $slug) {
        $first_lang = Language::where('first', '=', 1)->where('hidden','=', 0)->first();
        $lang = $first_lang->short;
        $portfolio = Portfolio::where('slug', '=', $slug)->first();
        $portfolio->images;
        $portfolio->planImages;
        return view('front.portfolio-single', compact('lang', 'portfolio'));
    }

}
