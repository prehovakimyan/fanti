<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Blog;
use App\Models\BlogImage;
use App\Models\Language;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function index(Request $request){
        $first_lang = Language::where('first', '=', 1)->where('hidden','=', 0)->first();
        $lang = $first_lang->short;
        $blog_posts = Blog::paginate(10);
        foreach ($blog_posts as $blog) {
            $blog->images = BlogImage::where('blog_id', '=', $blog->id)->get();
        }
        return view('front.blog', compact('lang', 'blog_posts'));
    }

    public function single(Request $request, $slug){
        $first_lang = Language::where('first', '=', 1)->where('hidden','=', 0)->first();
        $lang = $first_lang->short;
        $blog = Blog::where('slug', '=', $slug)->first();
        $blog->views++;
        $blog->save();
        $blog->images = BlogImage::where('blog_id', '=', $blog->id)->get();
        return view('front.blog-single', compact('lang', 'blog'));
    }
}
