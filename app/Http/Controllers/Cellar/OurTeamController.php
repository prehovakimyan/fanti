<?php

namespace App\Http\Controllers\Cellar;

use App\Http\Controllers\Controller;
use App\Models\Language;
use App\Models\OurTeam;
use App\Models\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class OurTeamController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $first_lang = Language::where('first', '=', 1)->where('hidden','=', 0)->first();
        $default_lang = $first_lang->short;
        $teams = OurTeam::select("name_$default_lang as title","short_desc_$default_lang as short_desc", "image", "id")->get();


        return view("cellar.ourTeam.all", compact( 'teams', 'default_lang'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $first_lang = Language::where('first', '=', 1)->where('hidden','=', 0)->first();
        $default_lang = $first_lang->short;
        $languages = Language::where('hidden', '=', 0)->get();
        return view("cellar.ourTeam.create", compact( 'languages', 'default_lang'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'name_am' => 'required|string',
            'name_ru' => 'required|string',
            'name_en' => 'required|string',
            "image"    => "required|file|mimes:jpg,jpeg,png|max:10240"
        ]);

        $team = new OurTeam();
        $team->name_am = $request->name_am;
        $team->name_ru = $request->name_ru;
        $team->name_en = $request->name_en;
        $team->short_desc_am = $request->short_desc_am;
        $team->short_desc_ru = $request->short_desc_ru;
        $team->short_desc_en = $request->short_desc_en;
        $team->position_am = $request->position_am;
        $team->position_ru = $request->position_ru;
        $team->position_en = $request->position_en;

        if($request->file("image")){
            $fileNameToStore = "team_".substr(md5(microtime()),rand(0,26),10);
            Storage::putFileAs('public/team', $request->file("image"), $fileNameToStore);
            $fileUrl = url('/').'/storage/team/'.$fileNameToStore;
            $team->image = $fileUrl;
        }

        $team->save();

        return redirect('admin/our-team');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $team = OurTeam::find($id);
        $first_lang = Language::where('first', '=', 1)->where('hidden','=', 0)->first();
        $default_lang = $first_lang->short;
        $languages = Language::where('hidden', '=', 0)->get();
        return view("cellar.ourTeam.edit", compact( 'team','languages', 'default_lang'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'name_am' => 'required|string',
            'name_ru' => 'required|string',
            'name_en' => 'required|string',
            "image"    => "nullable|file|mimes:jpg,jpeg,png|max:10240"
        ]);

        $team = OurTeam::find($id);
        $team->name_am = $request->name_am;
        $team->name_ru = $request->name_ru;
        $team->name_en = $request->name_en;
        $team->short_desc_am = $request->short_desc_am;
        $team->short_desc_ru = $request->short_desc_ru;
        $team->short_desc_en = $request->short_desc_en;
        $team->position_am = $request->position_am;
        $team->position_ru = $request->position_ru;
        $team->position_en = $request->position_en;

        if($request->file("image")){
            $fileNameToStore = "team_".substr(md5(microtime()),rand(0,26),10);
            Storage::putFileAs('public/team', $request->file("image"), $fileNameToStore);
            $fileUrl = url('/').'/storage/team/'.$fileNameToStore;
            $team->image = $fileUrl;
        } else {
            $team->image = $request->old_image;
        }

        $team->save();

        return redirect('admin/our-team');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $service = OurTeam::findOrFail($id);
        $service->delete();
    }
}
