<?php

namespace App\Http\Controllers\Cellar\Products;

use App\Http\Controllers\Controller;
use App\Models\Image;
use App\Models\Language;
use App\Models\Product;
use App\Models\ProductAttribute;
use App\Models\ProductCategory;
use App\Models\ProductImage;
use App\Models\ProductsProductAttribute;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{

    private function mb_str_split($string)
    {
        # Split at all position not after the start: ^
        # and not before the end: $
        return preg_split('/(?<!^)(?!$)/u', $string);
    }

    private function create_english($title)
    {
        $charlist = $this->mb_str_split($title);
        $array_hayeren = array('.', 'ա', 'բ', 'գ', 'դ', 'ե', 'զ', 'է', 'ը', 'թ', 'ժ', 'ի', 'լ', 'խ', 'ծ', 'կ', 'հ', 'ձ', 'ղ', 'ճ', 'մ', 'յ', 'ն', 'շ', 'ո', 'չ', 'պ', 'ջ', 'ռ', 'ս', 'վ', 'տ', 'ր', 'ց', 'ւ', 'փ', 'ք', 'և', 'օ', 'ֆ', ' ', '  ', '/', '\\', '&', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0');
        $array_english = array('-', 'a', 'b', 'g', 'd', 'e', 'z', 'e', 'y', 't', 'zh', 'i', 'l', 'kh', 'ts', 'k', 'h', 'dz', 'gh', 'ch', 'm', 'y', 'n', 'sh', 'o', 'ch', 'p', 'j', 'r', 's', 'v', 't', 'r', 'c', 'u', 'p', 'q', 'ev', 'o', 'f', '-', '-', '-', '-', '-', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0');
        $new = '';
        if (count($charlist) < 41) {
            $counts = count($charlist);
        } else {
            $counts = 40;
        }
        for ($i = 0; $i < $counts; $i++) {
            $key = array_search(mb_strtolower($charlist[$i], 'UTF-8'), $array_hayeren);
            $new .= $array_english[$key];

        }
        return $new;
    }

    private function create_slug($title, $first_code)
    {

        $slug = $this->create_english($title);
        $article_isset_slug = DB::table("products")->where('slug', '=', $slug)->orderBy('id', 'desc')->count();
        if ($article_isset_slug > 0) {
            $new_slug = $slug . "-" . $article_isset_slug;
        } else {
            $new_slug = $slug;
        }

        return $new_slug;
    }

    private function create_slug_id($title, $first_code, $id)
    {

        $slug = $this->create_english($title);
        $article_isset_slug = DB::table("products")->where('id', '!=', $id)->where('slug', '=', $slug)->orderBy('id', 'desc')->count();
        if ($article_isset_slug > 0) {
            $new_slug = $slug . "-" . $article_isset_slug;
        } else {
            $new_slug = $slug;
        }

        return $new_slug;
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $first_lang = Language::where('first', '=', 1)->where('hidden','=', 0)->first();
        $default_lang = $first_lang->short;
        $products = Product::select("title_$default_lang as title","image","id")->get();
        return view("cellar.products.all", compact('products', 'default_lang'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $first_lang = Language::where('first', '=', 1)->where('hidden','=', 0)->first();
        $default_lang = $first_lang->short;
        $languages = Language::where('hidden', '=', 0)->get();
        $categories = ProductCategory::all();
        return view("cellar.products.create", compact( 'categories', 'languages', 'default_lang'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $first_lang = Language::where('first', '=', 1)->where('hidden','=', 0)->first();
        $default_lang = $first_lang->short;

        $request->validate([
            'title_am'      => 'required|string',
            'title_ru'      => 'required|string',
            'title_en'      => 'required|string',
            'short_desc_am' => 'required|string',
            'short_desc_ru' => 'required|string',
            'short_desc_en' => 'required|string',
            'desc_am'       => 'required|string',
            'desc_ru'       => 'required|string',
            'desc_en'       => 'required|string',
            "image"         => "required|file|mimes:jpg,jpeg,png|max:10240",
            'filename.*'    => 'required|url',
            'filename'      => 'nullable|array',
        ]);

        $product = new Product();
        $product->available_count = $request->available_count;
        $product->title_am = $request->title_am;
        $product->title_ru = $request->title_ru;
        $product->title_en = $request->title_en;
        $product->short_desc_am = $request->short_desc_am;
        $product->short_desc_ru = $request->short_desc_ru;
        $product->short_desc_en = $request->short_desc_en;
        $product->desc_am = $request->desc_am;
        $product->desc_ru = $request->desc_ru;
        $product->desc_en = $request->desc_en;
        $product->pr_code = $request->pr_code;
        $product->price_unit_1 = 1;
        $product->price_1 = $request->price_1;
        $product->new_price_1 = $request->new_price_1;
        $product->price_unit_2 = $request->price_unit_2;
        $product->price_2 = $request->price_2;
        $product->new_price_2 = $request->new_price_2;
        if(isset($request->in_stock)){
            $product->in_stock = 1;
        }else{
            $product->in_stock = 0;
        }
        $product->video = $request->video;

        if($request->file("image")){
            $fileNameToStore = "product_".substr(md5(microtime()),rand(0,26),10);
            Storage::putFileAs('public/products/', $request->file("image"), $fileNameToStore);
            $fileUrl = url('/').'/storage/products/'.$fileNameToStore;
            $product->image = $fileUrl;
        }

        $new_slug = $this->create_slug($request->title_am, $default_lang);
        $product->slug = $new_slug;
        $product->save();

        if (isset($request->filename) && $request->filename != '') {
            foreach ($request->filename as $filename) {

                $galleryItem = new ProductImage();
                $galleryItem->image = $filename;
                $galleryItem->save();
                $product->images()->attach($galleryItem);
            }
        }

        $product->categories()->attach($request->product_cat_id);

        if(!empty($request->pr_attr_name)) {

            foreach ($request->pr_attr_name as $key => $pr_attr) {

                if($request->pr_attr_value_am[$key] != '' && $request->pr_attr_value_ru[$key] != '' && $request->pr_attr_value_en[$key] != '') {
                    $prAttrItem = new ProductsProductAttribute();
                    $prAttrItem->product_id = $product->id;
                    $prAttrItem->product_attribute_id = $pr_attr;
                    $prAttrItem->value_am = $request->pr_attr_value_am[$key];
                    $prAttrItem->value_ru = $request->pr_attr_value_ru[$key];
                    $prAttrItem->value_en = $request->pr_attr_value_en[$key];
                    $prAttrItem->save();
                }

            }

        }


        return redirect('admin/products');

    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $product = Product::findOrFail($id);
        $product->images;
        $product->categories;
        $categoriesID = [];
        $attributesID = [];
        $catId = 0;
        foreach ($product->categories as $category) {
            $catId = $category->id;
            $categoriesID[] = $category->id;
        }

        $product->categoriesID  = $categoriesID;
        $product->attributes = $productAttributes = ProductsProductAttribute::where('product_id', '=', $id)->get();
        foreach ($productAttributes as $productAttribute) {
            $attributesID[] = $productAttribute->product_attribute_id;
        }
        $product->attributesID  = $attributesID;
        /*$product->attributes = ProductsProductAttribute::join('product_attributes', 'products_product_attributes.product_attribute_id', '=', 'product_attributes.id')
            ->get(['products_product_attributes.*', 'product_attributes.title_am', 'product_attributes.title_ru', 'product_attributes.title_en']);*/
        $first_lang = Language::where('first', '=', 1)->where('hidden','=', 0)->first();
        $default_lang = $first_lang->short;
        $languages = Language::where('hidden', '=', 0)->get();
        $categories = ProductCategory::all();
        $productCategory = ProductCategory::find($catId);
        return view("cellar.products.edit", compact( 'productCategory','categories','product','languages', 'default_lang'));

    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $first_lang = Language::where('first', '=', 1)->where('hidden','=', 0)->first();
        $default_lang = $first_lang->short;

        $request->validate([
            'title_am'      => 'required|string',
            'title_ru'      => 'required|string',
            'title_en'      => 'required|string',
            'short_desc_am' => 'required|string',
            'short_desc_ru' => 'required|string',
            'short_desc_en' => 'required|string',
            'desc_am'       => 'required|string',
            'desc_ru'       => 'required|string',
            'desc_en'       => 'required|string',
            "image"         => "nullable|file|mimes:jpg,jpeg,png|max:10240",
            'filename.*'    => 'required|url',
            'filename'      => 'nullable|array',
        ]);

        $product = Product::findOrFail($id);
        $product->available_count = $request->available_count;
        $product->title_am = $request->title_am;
        $product->title_ru = $request->title_ru;
        $product->title_en = $request->title_en;
        $product->short_desc_am = $request->short_desc_am;
        $product->short_desc_ru = $request->short_desc_ru;
        $product->short_desc_en = $request->short_desc_en;
        $product->desc_am = $request->desc_am;
        $product->desc_ru = $request->desc_ru;
        $product->desc_en = $request->desc_en;
        $product->pr_code = $request->pr_code;
        $product->price_unit_1 = 1;
        $product->price_1 = $request->price_1;
        $product->new_price_1 = $request->new_price_1;
        $product->price_unit_2 = $request->price_unit_2;
        $product->price_2 = $request->price_2;
        $product->new_price_2 = $request->new_price_2;
        if(isset($request->in_stock)){
            $product->in_stock = 1;
        }else{
            $product->in_stock = 0;
        }
        $product->video = $request->video;
        $new_slug = $this->create_slug_id($request->title_am, $default_lang, $id);
        $product->slug = $new_slug;

        if($request->file("image")){
            $fileNameToStore = "product_".substr(md5(microtime()),rand(0,26),10);
            Storage::putFileAs('public/products/', $request->file("image"), $fileNameToStore);
            $fileUrl = url('/').'/storage/products/'.$fileNameToStore;
            $product->image = $fileUrl;
        }
        else {
            $product->image = $request->old_image;
        }

        $product->save();

        $product->categories()->sync($request->product_cat_id);

        $imagesArray = array();
        if (isset($request->filename) && $request->filename != '') {
            foreach ($request->filename as $filename) {

                $galleryItem = new ProductImage();
                $galleryItem->image = $filename;
                $galleryItem->save();
                $imagesArray[] = $galleryItem->id;
            }
        }

        $product->images()->sync($imagesArray);

        DB::table('products_product_attributes')->where('product_id','=', $id)->delete();
        if(!empty($request->pr_attr_name)) {

            foreach ($request->pr_attr_name as $key => $pr_attr) {

                if($request->pr_attr_value_am[$key] != '' && $request->pr_attr_value_ru[$key] != '' && $request->pr_attr_value_en[$key] != '') {
                    $prAttrItem = new ProductsProductAttribute();
                    $prAttrItem->product_id = $product->id;
                    $prAttrItem->product_attribute_id = $pr_attr;
                    $prAttrItem->value_am = $request->pr_attr_value_am[$key];
                    $prAttrItem->value_ru = $request->pr_attr_value_ru[$key];
                    $prAttrItem->value_en = $request->pr_attr_value_en[$key];
                    $prAttrItem->save();
                }

            }

        }

        return redirect('admin/products');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $product = Product::findOrFail($id);
        $product->delete();
    }

    public function getAttrByCatId(Request $request) {
        $catId = $request->catId;
        $languages = DB::table('languages')->where('hidden', '=', 0)->get();
        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden', '=', 0)->first();
        $default_lang = $first_lang->short;
        $category = ProductCategory::find($catId);

        ?>
        <div class="pr_attr_box">
            <div class="select_box_attr">
                <select class="form-control" name="pr_attr_name[]" id="">
                    <?php
                    foreach($category->attributes as $product_cat_attribute) {
                        ?>
                        <option value="<?=$product_cat_attribute->id ?>"><?=$product_cat_attribute->{"title_".$default_lang} ?></option>
                        <?php
                    }
                    ?>
                </select>
            </div>
            <?php foreach($languages as $language) { ?>
                <div style="margin: 0" class="form-group lang_field lang_field_<?=$language->short ?> value_box_attr <?=($language->first == 1) ? "active_field" : "hidden_field"?>">
                    <input class="form-control" type="text" name="pr_attr_value_<?=$language->short?>[]" value="">
                </div>
                <?php
            }
            ?>
            <div class="delete_box_attr">
                <a class="del_row"><i class="fa fa-minus" aria-hidden="true"></i></a>
            </div>
        </div>
        <?php
    }

    public function uploadGallery(Request $request) {
        $fileNameToStore = "product_image_".substr(md5(microtime()),rand(0,26),10);
        Storage::putFileAs('public/products/', $request->file("file"), $fileNameToStore);
        $fileUrl = url('/').'/storage/products/'.$fileNameToStore;
        echo $fileUrl;
    }
}
