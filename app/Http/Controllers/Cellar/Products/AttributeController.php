<?php

namespace App\Http\Controllers\Cellar\Products;

use App\Http\Controllers\Controller;
use App\Models\Language;
use App\Models\ProductAttribute;
use App\Models\ProductCategory;
use Illuminate\Http\Request;

class AttributeController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $first_lang = Language::where('first', '=', 1)->where('hidden','=', 0)->first();
        $default_lang = $first_lang->short;
        $attributes = ProductAttribute::all();

        foreach ($attributes as $attribute){
            $attribute->categories;
        }


        return view("cellar.productAttributes.all", compact('attributes', 'default_lang'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $first_lang = Language::where('first', '=', 1)->where('hidden','=', 0)->first();
        $default_lang = $first_lang->short;
        $languages = Language::where('hidden', '=', 0)->get();
        $categories = ProductCategory::all();
        return view("cellar.productAttributes.create", compact( 'categories','languages', 'default_lang'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'title_am' => 'required|string',
            'title_ru' => 'required|string',
            'title_en' => 'required|string',
            'category_id.*' => 'required|integer',
            'category_id' => 'required|array',
        ]);

        $attribute = new ProductAttribute();
        $attribute->title_am = $request->title_am;
        $attribute->title_ru = $request->title_ru;
        $attribute->title_en = $request->title_en;
        $attribute->save();
        $attribute->categories()->sync($request->category_id);

        return redirect('admin/product-attribute');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $attribute = ProductAttribute::findOrFail($id);
        $cats = array();
        foreach ($attribute->categories as $attCategory){
            $cats[] = $attCategory->id;
        }
        $attribute->cats = $cats;
        unset($attribute->categories);
        $first_lang = Language::where('first', '=', 1)->where('hidden','=', 0)->first();
        $default_lang = $first_lang->short;
        $languages = Language::where('hidden', '=', 0)->get();

        $categories = ProductCategory::all();

        return view("cellar.productAttributes.edit", compact( 'categories','attribute','languages', 'default_lang'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'title_am' => 'required|string',
            'title_ru' => 'required|string',
            'title_en' => 'required|string',
            'category_id.*' => 'required|integer',
            'category_id' => 'required|array',
        ]);

        $attribute = ProductAttribute::findOrFail($id);
        $attribute->title_am = $request->title_am;
        $attribute->title_ru = $request->title_ru;
        $attribute->title_en = $request->title_en;
        $attribute->save();
        $attribute->categories()->sync($request->category_id);

        return redirect('admin/product-attribute');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $attribute = ProductAttribute::findOrFail($id);
        $attribute->delete();
    }
}
