<?php

namespace App\Http\Controllers\Cellar;

use App\Http\Controllers\Controller;
use App\Models\Language;
use App\Models\Brand;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class BrandController extends Controller
{

    private function mb_str_split($string)
    {
        # Split at all position not after the start: ^
        # and not before the end: $
        return preg_split('/(?<!^)(?!$)/u', $string);
    }

    private function create_english($title)
    {
        $charlist = $this->mb_str_split($title);
        $array_hayeren = array('.', 'ա', 'բ', 'գ', 'դ', 'ե', 'զ', 'է', 'ը', 'թ', 'ժ', 'ի', 'լ', 'խ', 'ծ', 'կ', 'հ', 'ձ', 'ղ', 'ճ', 'մ', 'յ', 'ն', 'շ', 'ո', 'չ', 'պ', 'ջ', 'ռ', 'ս', 'վ', 'տ', 'ր', 'ց', 'ւ', 'փ', 'ք', 'և', 'օ', 'ֆ', ' ', '  ', '/', '\\', '&', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0');
        $array_english = array('-', 'a', 'b', 'g', 'd', 'e', 'z', 'e', 'y', 't', 'zh', 'i', 'l', 'kh', 'ts', 'k', 'h', 'dz', 'gh', 'ch', 'm', 'y', 'n', 'sh', 'o', 'ch', 'p', 'j', 'r', 's', 'v', 't', 'r', 'c', 'u', 'p', 'q', 'ev', 'o', 'f', '-', '-', '-', '-', '-', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0');
        $new = '';
        if (count($charlist) < 41) {
            $counts = count($charlist);
        } else {
            $counts = 40;
        }
        for ($i = 0; $i < $counts; $i++) {
            $key = array_search(mb_strtolower($charlist[$i], 'UTF-8'), $array_hayeren);
            $new .= $array_english[$key];

        }
        return $new;
    }

    private function create_slug($title, $first_code)
    {

        $slug = $this->create_english($title);
        $article_isset_slug = DB::table("brands")->where('slug', '=', $slug)->orderBy('id', 'desc')->count();
        if ($article_isset_slug > 0) {
            $new_slug = $slug . "-" . $article_isset_slug;
        } else {
            $new_slug = $slug;
        }

        return $new_slug;
    }

    private function create_slug_id($title, $first_code, $id)
    {

        $slug = $this->create_english($title);
        $article_isset_slug = DB::table("brands")->where('id', '!=', $id)->where('slug', '=', $slug)->orderBy('id', 'desc')->count();
        if ($article_isset_slug > 0) {
            $new_slug = $slug . "-" . $article_isset_slug;
        } else {
            $new_slug = $slug;
        }

        return $new_slug;
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $first_lang = Language::where('first', '=', 1)->where('hidden','=', 0)->first();
        $default_lang = $first_lang->short;
        $brands = Brand::select("title_$default_lang as title","id")->get();
        return view("cellar.brands.all", compact('brands', 'default_lang'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $first_lang = Language::where('first', '=', 1)->where('hidden','=', 0)->first();
        $default_lang = $first_lang->short;
        $languages = Language::where('hidden', '=', 0)->get();
        return view("cellar.brands.create", compact( 'languages', 'default_lang'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $first_lang = Language::where('first', '=', 1)->where('hidden','=', 0)->first();
        $default_lang = $first_lang->short;
        $request->validate([
            'title_am' => 'required|string',
            'title_ru' => 'required|string',
            'title_en' => 'required|string',
            "image"    => "nullable|file|mimes:jpg,jpeg,png|max:10240"
        ]);

        $brand = new Brand();
        $brand->title_am = $request->title_am;
        $brand->title_ru = $request->title_ru;
        $brand->title_en = $request->title_en;
        $brand->short_desc_am = $request->short_desc_am;
        $brand->short_desc_ru = $request->short_desc_ru;
        $brand->short_desc_en = $request->short_desc_en;
        $new_slug = $this->create_slug($request->title_am, $default_lang);
        $brand->slug = $new_slug;

        if($request->file("image")){
            $fileNameToStore = "brand_".substr(md5(microtime()),rand(0,26),10);
            Storage::putFileAs('public/brand', $request->file("image"), $fileNameToStore);
            $fileUrl = url('/').'/storage/brand/'.$fileNameToStore;
            $brand->image = $fileUrl;
        }
        $brand->save();

        return redirect('admin/brands');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $brand = Brand::findOrFail($id);
        $first_lang = Language::where('first', '=', 1)->where('hidden','=', 0)->first();
        $default_lang = $first_lang->short;
        $languages = Language::where('hidden', '=', 0)->get();
        return view("cellar.brands.edit", compact( 'brand','languages', 'default_lang'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'title_am' => 'required|string',
            'title_ru' => 'required|string',
            'title_en' => 'required|string',
            "image"    => "nullable|file|mimes:jpg,jpeg,png|max:10240"
        ]);

        $first_lang = Language::where('first', '=', 1)->where('hidden','=', 0)->first();
        $default_lang = $first_lang->short;

        $brand = Brand::findOrFail($id);
        $brand->title_am = $request->title_am;
        $brand->title_ru = $request->title_ru;
        $brand->title_en = $request->title_en;
        $brand->short_desc_am = $request->short_desc_am;
        $brand->short_desc_ru = $request->short_desc_ru;
        $brand->short_desc_en = $request->short_desc_en;
        $new_slug = $this->create_slug_id($request->title_am, $default_lang, $id);
        $brand->slug = $new_slug;

        if($request->file("image")){
            $fileNameToStore = "brand_".substr(md5(microtime()),rand(0,26),10);
            Storage::putFileAs('public/brand', $request->file("image"), $fileNameToStore);
            $fileUrl = url('/').'/storage/brand/'.$fileNameToStore;
            $brand->image = $fileUrl;
        }else {
            $brand->image = $request->old_image;
        }
        $brand->save();

        return redirect('admin/brands');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $brand = Brand::findOrFail($id);
        $brand->delete();
    }
}
