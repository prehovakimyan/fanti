<?php

namespace App\Http\Controllers\Cellar;

use App\GalleryInfo as GalleryInfo;
use App\Http\Controllers\Controller;
use App\Models\Gallery;
use App\Models\Language;
use App\Models\PageGallery;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class PageGalleryController extends Controller
{
    public function index(){

    }

    public function create($parent_id) {
        $first_lang = Language::where('first', '=', 1)->where('hidden','=', 0)->first();
        $default_lang = $first_lang->short;
        $languages = Language::where('hidden', '=', 0)->get();
        return view("cellar.pageGallery.create", compact('languages', 'parent_id', 'default_lang'));
    }

    public function store(Request $request, $parent_id) {

        $request->validate([
            'filename.*' => 'required|url',
            'filename' => 'required|array',
        ]);


            $pageGallery = new PageGallery();
            $pageGallery->page_id = $parent_id;
            $pageGallery->title_am = $request->title_am;
            $pageGallery->title_ru = $request->title_ru;
            $pageGallery->title_en = $request->title_en;
            $pageGallery->title_en = $request->title_en;
            $pageGallery->short_desc_am = $request->short_desc_am;
            $pageGallery->short_desc_ru = $request->short_desc_ru;
            $pageGallery->short_desc_en = $request->short_desc_en;
            $pageGallery->save();


            if (isset($request->filename) && $request->filename != '') {
                foreach ($request->filename as $filename) {

                    $galleryItem = new Gallery();
                    $galleryItem->image = $filename;
                    $galleryItem->parent_id = $pageGallery->id;
                    $galleryItem->save();
                }
            }



        return redirect('admin/pages');
    }

    public function edit($gallery_id) {
        $gallery = PageGallery::find($gallery_id);

        $projectImages = Gallery::where('parent_id', '=', $gallery->id)->get();
        $dropzoneDefaultHidden = '';
        if ($projectImages) {
            $dropzoneDefaultHidden = 'dz-default-hidden';
        }


        $first_lang = Language::where('first', '=', 1)->where('hidden','=', 0)->first();
        $default_lang = $first_lang->short;
        $languages = Language::where('hidden', '=', 0)->get();

        return view("cellar.pageGallery.edit", compact('projectImages','dropzoneDefaultHidden','gallery','languages', 'default_lang'));
    }

    public function update(Request $request, $id) {
        $request->validate([
            'filename.*' => 'required|url',
            'filename' => 'required|array',
        ]);

        $pageGallery = PageGallery::find($id);
        $pageGallery->title_am = $request->title_am;
        $pageGallery->title_ru = $request->title_ru;
        $pageGallery->title_en = $request->title_en;
        $pageGallery->title_en = $request->title_en;
        $pageGallery->short_desc_am = $request->short_desc_am;
        $pageGallery->short_desc_ru = $request->short_desc_ru;
        $pageGallery->short_desc_en = $request->short_desc_en;
        $pageGallery->save();

        DB::table('galleries')->where('parent_id', '=', $id)->delete();
        if (isset($request->filename) && $request->filename != '') {
            foreach ($request->filename as $filename) {

                $galleryItem = new Gallery();
                $galleryItem->image = $filename;
                $galleryItem->parent_id = $pageGallery->id;
                $galleryItem->save();
            }
        }
        return redirect('admin/pages');
    }

    public function upload(Request $request) {
        $fileNameToStore = "gallery_".substr(md5(microtime()),rand(0,26),10);
        Storage::putFileAs('public/gallery', $request->file("file"), $fileNameToStore);
        $fileUrl = url('/').'/storage/gallery/'.$fileNameToStore;
        echo $fileUrl;
    }
}
