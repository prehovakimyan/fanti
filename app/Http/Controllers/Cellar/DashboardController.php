<?php

namespace App\Http\Controllers\Cellar;

use App\Http\Controllers\Controller;
use App\Models\Language;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index(){

        $first_lang = Language::where('first', '=', 1)->where('hidden','=', 0)->first();
        $defaultLang = $first_lang->short;
        return view("cellar.dashboard", compact('defaultLang'));
    }
}
