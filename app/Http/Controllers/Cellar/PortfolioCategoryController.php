<?php

namespace App\Http\Controllers\Cellar;

use App\Http\Controllers\Controller;
use App\Models\Language;
use App\Models\PortfolioCategory;
use Illuminate\Http\Request;

class PortfolioCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(){
        $first_lang = Language::where('first', '=', 1)->where('hidden','=', 0)->first();
        $default_lang = $first_lang->short;
        $categories = PortfolioCategory::select("title_$default_lang as title","id")->get();
        return view("cellar.portfolioCategory.all", compact('categories', 'default_lang'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(){
        $first_lang = Language::where('first', '=', 1)->where('hidden','=', 0)->first();
        $default_lang = $first_lang->short;
        $languages = Language::where('hidden', '=', 0)->get();
        return view("cellar.portfolioCategory.create", compact( 'languages', 'default_lang'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request){
        $request->validate([
            'title_am' => 'required|string',
            'title_ru' => 'required|string',
            'title_en' => 'required|string'
        ]);

        $category = new PortfolioCategory();
        $category->title_am = $request->title_am;
        $category->title_ru = $request->title_ru;
        $category->title_en = $request->title_en;
        $category->save();
        return redirect('admin/portfolio-category');
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id){
        $first_lang = Language::where('first', '=', 1)->where('hidden','=', 0)->first();
        $default_lang = $first_lang->short;
        $languages = Language::where('hidden', '=', 0)->get();
        $category = PortfolioCategory::find($id);
        return view("cellar.portfolioCategory.edit", compact( 'category','languages', 'default_lang'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id){

        $request->validate([
            'title_am' => 'required|string',
            'title_ru' => 'required|string',
            'title_en' => 'required|string'
        ]);

        $category = PortfolioCategory::find($id);
        $category->title_am = $request->title_am;
        $category->title_ru = $request->title_ru;
        $category->title_en = $request->title_en;
        $category->save();
        return redirect('admin/portfolio-category');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $category = PortfolioCategory::findOrFail($id);
        $category->delete();
    }
}
