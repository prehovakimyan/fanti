<?php

namespace App\Http\Controllers\Cellar;

use App\Http\Controllers\Controller;
use App\Models\Language;
use App\Models\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $first_lang = Language::where('first', '=', 1)->where('hidden','=', 0)->first();
        $default_lang = $first_lang->short;
        $services = Service::select("title_$default_lang as title","short_desc_$default_lang as short_desc", "image", "id")->get();


        return view("cellar.services.all", compact( 'services', 'default_lang'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $first_lang = Language::where('first', '=', 1)->where('hidden','=', 0)->first();
        $default_lang = $first_lang->short;
        $languages = Language::where('hidden', '=', 0)->get();
        return view("cellar.services.create", compact( 'languages', 'default_lang'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'title_am' => 'required|string',
            'title_ru' => 'required|string',
            'title_en' => 'required|string',
            'desc_am'  => 'required|string',
            'desc_ru'  => 'required|string',
            'desc_en'  => 'required|string',
            "image"    => "required|file|mimes:jpg,jpeg,png|max:10240"
        ]);

        $service = new Service();
        $service->title_am = $request->title_am;
        $service->title_ru = $request->title_ru;
        $service->title_en = $request->title_en;
        $service->short_desc_am = $request->short_desc_am;
        $service->short_desc_ru = $request->short_desc_ru;
        $service->short_desc_en = $request->short_desc_en;
        $service->desc_am = $request->desc_am;
        $service->desc_ru = $request->desc_ru;
        $service->desc_en = $request->desc_en;
        $service->person_1_am = $request->person_1_am;
        $service->person_1_ru = $request->person_1_ru;
        $service->person_1_en = $request->person_1_en;
        $service->person_2_am = $request->person_2_am;
        $service->person_2_ru = $request->person_2_ru;
        $service->person_2_en = $request->person_2_en;
        $service->person_3_am = $request->person_3_am;
        $service->person_3_ru = $request->person_3_ru;
        $service->person_3_en = $request->person_3_en;
        $service->price = $request->price;

        if($request->file("image")){
            $fileNameToStore = "service_".substr(md5(microtime()),rand(0,26),10);
            Storage::putFileAs('public/service', $request->file("image"), $fileNameToStore);
            $fileUrl = url('/').'/storage/service/'.$fileNameToStore;
            $service->image = $fileUrl;
        }

        $service->save();

        return redirect('admin/services');

    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $service = Service::findOrFail($id);
        $first_lang = Language::where('first', '=', 1)->where('hidden','=', 0)->first();
        $default_lang = $first_lang->short;
        $languages = Language::where('hidden', '=', 0)->get();
        return view("cellar.services.edit", compact( 'service','languages', 'default_lang'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'title_am' => 'required|string',
            'title_ru' => 'required|string',
            'title_en' => 'required|string',
            'desc_am'  => 'required|string',
            'desc_ru'  => 'required|string',
            'desc_en'  => 'required|string',
            "image"    => "nullable|file|mimes:jpg,jpeg,png|max:10240"
        ]);

        $service = Service::find($id);
        $service->title_am = $request->title_am;
        $service->title_ru = $request->title_ru;
        $service->title_en = $request->title_en;
        $service->short_desc_am = $request->short_desc_am;
        $service->short_desc_ru = $request->short_desc_ru;
        $service->short_desc_en = $request->short_desc_en;
        $service->desc_am = $request->desc_am;
        $service->desc_ru = $request->desc_ru;
        $service->desc_en = $request->desc_en;
        $service->person_1_am = $request->person_1_am;
        $service->person_1_ru = $request->person_1_ru;
        $service->person_1_en = $request->person_1_en;
        $service->person_2_am = $request->person_2_am;
        $service->person_2_ru = $request->person_2_ru;
        $service->person_2_en = $request->person_2_en;
        $service->person_3_am = $request->person_3_am;
        $service->person_3_ru = $request->person_3_ru;
        $service->person_3_en = $request->person_3_en;
        $service->price = $request->price;

        if($request->file("image")){
            $fileNameToStore = "service_".substr(md5(microtime()),rand(0,26),10);
            Storage::putFileAs('public/service', $request->file("image"), $fileNameToStore);
            $fileUrl = url('/').'/storage/service/'.$fileNameToStore;
            $service->image = $fileUrl;
        } else {
            $service->image = $request->old_image;
        }

        $service->save();

        return redirect('admin/services');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $service = Service::findOrFail($id);
        $service->delete();
    }
}
