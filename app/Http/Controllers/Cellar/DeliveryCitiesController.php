<?php

namespace App\Http\Controllers\Cellar;

use App\Http\Controllers\Controller;
use App\Models\DeliveryCity;
use App\Models\Language;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DeliveryCitiesController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden','=', 0)->first();
        $default_lang = $first_lang->short;

        $cities = DeliveryCity::select("title_$default_lang as title","id")->get();
        return view("cellar.deliveryCities.all", compact('cities', 'default_lang'));

    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $first_lang = Language::where('first', '=', 1)->where('hidden','=', 0)->first();
        $default_lang = $first_lang->short;
        $languages = Language::where('hidden', '=', 0)->get();
        return view("cellar.deliveryCities.create", compact( 'languages', 'default_lang'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $first_lang = Language::where('first', '=', 1)->where('hidden','=', 0)->first();
        $default_lang = $first_lang->short;
        $request->validate([
            'title_am' => 'required|string',
            'title_ru' => 'required|string',
            'title_en' => 'required|string',
            "image"    => "nullable|file|mimes:jpg,jpeg,png|max:10240"
        ]);

        $city = new DeliveryCity();
        $city->title_am = $request->title_am;
        $city->title_ru = $request->title_ru;
        $city->title_en = $request->title_en;
        $city->cost = $request->cost;
        $city->save();
        return redirect('admin/delivery-cities');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $city = DeliveryCity::findOrFail($id);
        $first_lang = Language::where('first', '=', 1)->where('hidden','=', 0)->first();
        $default_lang = $first_lang->short;
        $languages = Language::where('hidden', '=', 0)->get();
        return view("cellar.deliveryCities.edit", compact( 'city','languages', 'default_lang'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'title_am' => 'required|string',
            'title_ru' => 'required|string',
            'title_en' => 'required|string',
            "image"    => "nullable|file|mimes:jpg,jpeg,png|max:10240"
        ]);

        $first_lang = Language::where('first', '=', 1)->where('hidden','=', 0)->first();
        $default_lang = $first_lang->short;

        $city = DeliveryCity::findOrFail($id);
        $city->title_am = $request->title_am;
        $city->title_ru = $request->title_ru;
        $city->title_en = $request->title_en;
        $city->cost = $request->cost;
        $city->save();
        return redirect('admin/delivery-cities');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
