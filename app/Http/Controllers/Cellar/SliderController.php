<?php

namespace App\Http\Controllers\Cellar;

use App\Models\Language;
use App\Models\Slider;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use App\Models\Page;

class SliderController extends Controller
{
    public function index(Request $request){
        $sliderItems = Slider::orderBy('item_order', "asc")->get();
        $first_lang = Language::where('first', '=', 1)->where('hidden', '=', 0)->first();
        $default_lang = $first_lang->short;
        $languages = Language::where('hidden', '=', 0)->get();
        return view("cellar.slider.all", compact('default_lang','languages', 'sliderItems'));
    }

    public function create(){
        $first_lang = Language::where('first', '=', 1)->where('hidden', '=', 0)->first();
        $default_lang = $first_lang->short;
        $languages = Language::where('hidden', '=', 0)->get();

        return view('cellar.slider.create', compact('default_lang','languages'));
    }

    public function store(Request $request) {

        $request->validate([
            "slide_title_am" => "required|string|max:255",
            "slide_title_ru" => "required|string|max:255",
            "slide_title_en" => "required|string|max:255",
            "item_order" => "required|integer",
            "image"  => "required|file|mimes:jpg,jpeg,png|dimensions:max_width=2200,max_height=1400|max:5120",
        ]);

        $newSlider = new Slider();
        foreach ($request->all() as $key => $val) {
            if ($key != 'new_blank' && $key != '_token' && $key != 'image') {
                $newSlider->$key = $val;
            }
        }
        if($request->new_blank == 1) {
            $newSlider->new_blank = 1;
        } else {
            $newSlider->new_blank = 0;
        }

        if ($request->hasFile("image")) {
            $fileNameToStore = "slide_".time();

            Storage::putFileAs('public/slider', $request->file("image"), $fileNameToStore);

            $fileUrl = url('/').'/storage/slider/'.$fileNameToStore;

            $newSlider->image = $fileUrl;
        }

        $newSlider->save();

        return redirect('/admin/slider');

    }

    public function edit($id)
    {
        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden','=', 0)->first();
        $default_lang = $first_lang->short;

        $languages = DB::table('languages')->where('hidden','=', 0)->get();
        $slide = Slider::find($id);


        return view('cellar.slider.edit', compact('default_lang','languages', 'slide'));
    }

    public function update(Request $request, string $id){
        $request->validate([
            "slide_title_am" => "required|string|max:255",
            "slide_title_ru" => "required|string|max:255",
            "slide_title_en" => "required|string|max:255",
            "item_order" => "required|integer",
            "image"  => "nullable|file|mimes:jpg,jpeg,png|dimensions:max_width=2200,max_height=1400|max:5120",
        ]);

        $newSlider = Slider::find($id);
        foreach ($request->all() as $key => $val) {
            if ($key != 'new_blank' &&  $key != '_token' && $key!='_method' && $key != 'image') {
                $newSlider->$key = $val;
            }
        }
        if($request->new_blank == 1) {
            $newSlider->new_blank = 1;
        } else {
            $newSlider->new_blank = 0;
        }

        if ($request->hasFile("image")) {
            $fileNameToStore = "slide_".time();

            Storage::putFileAs('public/slider', $request->file("image"), $fileNameToStore);

            $fileUrl = url('/').'/storage/slider/'.$fileNameToStore;

            $newSlider->image = $fileUrl;
        }

        $newSlider->save();

        return redirect('/admin/slider');

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $slide = Slider::findOrFail($id);
        $slide->delete();
    }

}
