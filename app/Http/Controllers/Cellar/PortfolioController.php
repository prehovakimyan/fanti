<?php

namespace App\Http\Controllers\Cellar;

use App\Http\Controllers\Controller;
use App\Models\Image;
use App\Models\Language;
use App\Models\PlanImage;
use App\Models\Portfolio;
use App\Models\PortfolioCategory;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class PortfolioController extends Controller
{

    private function mb_str_split($string)
    {
        # Split at all position not after the start: ^
        # and not before the end: $
        return preg_split('/(?<!^)(?!$)/u', $string);
    }

    private function create_english($title)
    {
        $charlist = $this->mb_str_split($title);
        $array_hayeren = array('.', 'ա', 'բ', 'գ', 'դ', 'ե', 'զ', 'է', 'ը', 'թ', 'ժ', 'ի', 'լ', 'խ', 'ծ', 'կ', 'հ', 'ձ', 'ղ', 'ճ', 'մ', 'յ', 'ն', 'շ', 'ո', 'չ', 'պ', 'ջ', 'ռ', 'ս', 'վ', 'տ', 'ր', 'ց', 'ւ', 'փ', 'ք', 'և', 'օ', 'ֆ', ' ', '  ', '/', '\\', '&', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0');
        $array_english = array('-', 'a', 'b', 'g', 'd', 'e', 'z', 'e', 'y', 't', 'zh', 'i', 'l', 'kh', 'ts', 'k', 'h', 'dz', 'gh', 'ch', 'm', 'y', 'n', 'sh', 'o', 'ch', 'p', 'j', 'r', 's', 'v', 't', 'r', 'c', 'u', 'p', 'q', 'ev', 'o', 'f', '-', '-', '-', '-', '-', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0');
        $new = '';
        if (count($charlist) < 41) {
            $counts = count($charlist);
        } else {
            $counts = 40;
        }
        for ($i = 0; $i < $counts; $i++) {
            $key = array_search(mb_strtolower($charlist[$i], 'UTF-8'), $array_hayeren);
            $new .= $array_english[$key];

        }
        return $new;
    }

    private function create_slug($title, $first_code)
    {

        $slug = $this->create_english($title);
        $article_isset_slug = DB::table("portfolios")->where('slug', '=', $slug)->orderBy('id', 'desc')->count();
        if ($article_isset_slug > 0) {
            $new_slug = $slug . "-" . $article_isset_slug;
        } else {
            $new_slug = $slug;
        }

        return $new_slug;
    }

    private function create_slug_id($title, $first_code, $id)
    {

        $slug = $this->create_english($title);
        $article_isset_slug = DB::table("portfolios")->where('id', '!=', $id)->where('slug', '=', $slug)->orderBy('id', 'desc')->count();
        if ($article_isset_slug > 0) {
            $new_slug = $slug . "-" . $article_isset_slug;
        } else {
            $new_slug = $slug;
        }

        return $new_slug;
    }

    public function index() {
        $first_lang = Language::where('first', '=', 1)->where('hidden','=', 0)->first();
        $default_lang = $first_lang->short;
        $portfolios = Portfolio::select("title_$default_lang as title","short_desc_$default_lang as short_desc", "image", "id")->get();
        foreach ($portfolios as $portfolio) {
            $portfolio->planImages;
            $portfolio->images;
        }

        return view("cellar.portfolio.all", compact('portfolios', 'default_lang'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $first_lang = Language::where('first', '=', 1)->where('hidden','=', 0)->first();
        $default_lang = $first_lang->short;
        $languages = DB::table('languages')->where('hidden','=', 0)->get();
        $categories = PortfolioCategory::all();
        return view('cellar.portfolio.create', compact('default_lang', 'languages', 'categories'));
    }

    public function store(Request $request) {
        $first_lang = Language::where('first', '=', 1)->where('hidden','=', 0)->first();
        $default_lang = $first_lang->short;
        $request->validate([
            'title_am'      => 'required|string',
            'title_ru'      => 'required|string',
            'title_en'      => 'required|string',
            'short_desc_am' => 'required|string',
            'short_desc_ru' => 'required|string',
            'short_desc_en' => 'required|string',
            'desc_am'       => 'required|string',
            'desc_ru'       => 'required|string',
            'desc_en'       => 'required|string',
            "image"         => "required|file|mimes:jpg,jpeg,png|max:10240",
            'filename.*'    => 'required|url',
            'filename'      => 'required|array',
            'planImages.*'  => 'required|url',
            'planImages'    => 'required|array',
        ]);


        $portfolio = new Portfolio();
        $portfolio->title_am = $request->title_am;
        $portfolio->title_ru = $request->title_ru;
        $portfolio->title_en = $request->title_en;
        $portfolio->short_desc_am = $request->short_desc_am;
        $portfolio->short_desc_ru = $request->short_desc_ru;
        $portfolio->short_desc_en = $request->short_desc_en;
        $portfolio->desc_am = $request->desc_am;
        $portfolio->desc_ru = $request->desc_ru;
        $portfolio->desc_en = $request->desc_en;
        $portfolio->project_date = Carbon::parse($request->project_date)->format('Y-m-d H:i:s');
        $portfolio->status = 3;
        $portfolio->location_am = $request->location_am;
        $portfolio->location_ru = $request->location_ru;
        $portfolio->location_en = $request->location_en;
        $portfolio->video_desc_am = $request->video_desc_am;
        $portfolio->video_desc_ru = $request->video_desc_ru;
        $portfolio->video_desc_en = $request->video_desc_en;
        $portfolio->video_link = $request->video_link;
        $new_slug = $this->create_slug($request->title_am, $default_lang);
        $portfolio->slug = $new_slug;

        if($request->file("image")){
            $fileNameToStore = "portfolio_main_".substr(md5(microtime()),rand(0,26),10);
            Storage::putFileAs('public/portfolio', $request->file("image"), $fileNameToStore);
            $fileUrl = url('/').'/storage/portfolio/'.$fileNameToStore;
            $portfolio->image = $fileUrl;
        }

        $portfolio->save();

        $portfolio->categories()->sync($request->category);

        if (isset($request->filename) && $request->filename != '') {
            foreach ($request->filename as $filename) {

                $galleryItem = new Image();
                $galleryItem->image = $filename;
                $galleryItem->parent_id = $portfolio->id;
                $galleryItem->save();
                $portfolio->images()->attach($galleryItem);
            }
        }

        if (isset($request->planImages) && $request->planImages != '') {
            foreach ($request->planImages as $filename) {

                $planImage = new PlanImage();
                $planImage->image = $filename;
                $planImage->parent_id = $portfolio->id;
                $planImage->save();
                $portfolio->planImages()->attach($planImage);
            }
        }

        return redirect('admin/portfolio');

    }

    public function edit($id) {
        $portfolio = Portfolio::find($id);
        $portfolio->images;
        $portfolio->categories;
        $dropzoneDefaultHidden = '';
        if ($portfolio->images) {
            $dropzoneDefaultHidden = 'dz-default-hidden';
        }
        $portfolio->planImages;
        $dropzonePlanHidden = '';
        if ($portfolio->planImages) {
            $dropzonePlanHidden = 'dz-default-hidden';
        }

        $first_lang = Language::where('first', '=', 1)->where('hidden','=', 0)->first();
        $default_lang = $first_lang->short;
        $languages = Language::where('hidden', '=', 0)->get();
        $categories = PortfolioCategory::all();
        $portfolioCategoryIds = array();

        foreach ($portfolio->categories as $category){
            $portfolioCategoryIds[] = $category->id;
        }
        return view("cellar.portfolio.edit", compact( 'portfolioCategoryIds','categories','dropzoneDefaultHidden','dropzonePlanHidden','portfolio','languages', 'default_lang'));
    }

    public function update(Request $request, string $id) {
        $first_lang = Language::where('first', '=', 1)->where('hidden','=', 0)->first();
        $default_lang = $first_lang->short;
        $request->validate([
            'title_am'      => 'required|string',
            'title_ru'      => 'required|string',
            'title_en'      => 'required|string',
            'short_desc_am' => 'required|string',
            'short_desc_ru' => 'required|string',
            'short_desc_en' => 'required|string',
            'desc_am'       => 'required|string',
            'desc_ru'       => 'required|string',
            'desc_en'       => 'required|string',
            "image"         => "nullable|file|mimes:jpg,jpeg,png|max:10240",
            'filename.*'    => 'required|url',
            'filename'      => 'required|array',
            'planImages.*'  => 'required|url',
            'planImages'    => 'required|array',
        ]);


        $portfolio = Portfolio::findOrFail($id);
        $portfolio->title_am = $request->title_am;
        $portfolio->title_ru = $request->title_ru;
        $portfolio->title_en = $request->title_en;
        $portfolio->short_desc_am = $request->short_desc_am;
        $portfolio->short_desc_ru = $request->short_desc_ru;
        $portfolio->short_desc_en = $request->short_desc_en;
        $portfolio->desc_am = $request->desc_am;
        $portfolio->desc_ru = $request->desc_ru;
        $portfolio->desc_en = $request->desc_en;
        $portfolio->project_date = Carbon::parse($request->project_date)->format('Y-m-d H:i:s');
        $portfolio->status = 3;
        $portfolio->location_am = $request->location_am;
        $portfolio->location_ru = $request->location_ru;
        $portfolio->location_en = $request->location_en;
        $portfolio->video_desc_am = $request->video_desc_am;
        $portfolio->video_desc_ru = $request->video_desc_ru;
        $portfolio->video_desc_en = $request->video_desc_en;
        $portfolio->video_link = $request->video_link;
        $new_slug = $this->create_slug_id($request->title_am, $default_lang, $id);
        $portfolio->slug = $new_slug;

        if($request->file("image")){
            $fileNameToStore = "portfolio_main_".substr(md5(microtime()),rand(0,26),10);
            Storage::putFileAs('public/portfolio', $request->file("image"), $fileNameToStore);
            $fileUrl = url('/').'/storage/portfolio/'.$fileNameToStore;
            $portfolio->image = $fileUrl;
        } else {
            $portfolio->image = $request->old_image;
        }

        $portfolio->save();
        $portfolio->categories()->sync($request->category);

        $imagesArray = array();
        DB::table('images')->where('parent_id','=', $id)->delete();
        if (isset($request->filename) && $request->filename != '') {
            foreach ($request->filename as $filename) {

                $galleryItem = new Image();
                $galleryItem->image = $filename;
                $galleryItem->parent_id = $portfolio->id;
                $galleryItem->save();
                $imagesArray[] = $galleryItem->id;
            }
        }

        $portfolio->images()->sync($imagesArray);

        $planImagesArray = array();
        DB::table('plan_images')->where('parent_id','=', $id)->delete();
        if (isset($request->planImages) && $request->planImages != '') {
            foreach ($request->planImages as $filename) {

                $planImage = new PlanImage();
                $planImage->image = $filename;
                $planImage->parent_id = $portfolio->id;
                $planImage->save();
                $planImagesArray[] = $planImage->id;
            }
        }

        $portfolio->planImages()->sync($planImagesArray);

        return redirect('admin/portfolio');

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $portfolio = Portfolio::findOrFail($id);
        $portfolio->delete();
    }

    public function uploadGallery(Request $request) {
        $fileNameToStore = "portfolio_".substr(md5(microtime()),rand(0,26),10);
        Storage::putFileAs('public/portfolio/gallery/', $request->file("file"), $fileNameToStore);
        $fileUrl = url('/').'/storage/portfolio/gallery/'.$fileNameToStore;
        echo $fileUrl;
    }
    public function uploadPlan(Request $request) {
        $fileNameToStore = "plan_image_".substr(md5(microtime()),rand(0,26),10);
        Storage::putFileAs('public/portfolio/plan/', $request->file("file"), $fileNameToStore);
        $fileUrl = url('/').'/storage/portfolio/plan/'.$fileNameToStore;
        echo $fileUrl;
    }
}
