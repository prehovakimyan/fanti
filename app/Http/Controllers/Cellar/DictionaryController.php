<?php

namespace App\Http\Controllers\Cellar;

use App\Http\Controllers\Controller;
use App\Models\Dictionary;
use App\Models\Language;
use Illuminate\Http\Request;

class DictionaryController extends Controller
{
    public function index(Request $request)
    {



        if(isset($request->id) && $request->id !=''){
            $id = $request->id;
            $edit_dictionary = Dictionary::find($id);
        }else{
            $edit_dictionary = '';
        }

        $languages = Language::where('hidden', '=', 0)->get();
        $dictionaries = Dictionary::orderBy('id', 'desc')->get();
        return view("cellar.dictionary.all", compact('languages','dictionaries','edit_dictionary'));
    }


    public function store(Request $request)
    {

        $request->validate([
            "short_code" => "required|string|max:255",
            "value_am" => "required|string|max:255",
            "value_ru" => "required|string|max:255",
            "value_en" => "required|string|max:255",
            "dictionary_id" => "nullable|integer|max:255",
        ]);


        if($request->dictionary_id != 0){

            $dictionary = Dictionary::find($request->dictionary_id);
        }else{
            $dictionary = new Dictionary();
        }

        $dictionary->short_code = $request->short_code;
        $dictionary->value_am = $request->value_am;
        $dictionary->value_ru = $request->value_ru;
        $dictionary->value_en = $request->value_en;
        $dictionary->save();
        return redirect('admin/dictionary');

    }

    public function destroy($id)
    {
        Dictionary::destroy($id);
    }
}
