<?php

namespace App\Http\Controllers\Cellar;

use App\Http\Controllers\Controller;
use App\Models\Blog;
use App\Models\BlogImage;
use App\Models\Language;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class BlogController extends Controller
{
    private function mb_str_split($string)
    {
        # Split at all position not after the start: ^
        # and not before the end: $
        return preg_split('/(?<!^)(?!$)/u', $string);
    }

    private function create_english($title)
    {
        $charlist = $this->mb_str_split($title);
        $array_hayeren = array('.', 'ա', 'բ', 'գ', 'դ', 'ե', 'զ', 'է', 'ը', 'թ', 'ժ', 'ի', 'լ', 'խ', 'ծ', 'կ', 'հ', 'ձ', 'ղ', 'ճ', 'մ', 'յ', 'ն', 'շ', 'ո', 'չ', 'պ', 'ջ', 'ռ', 'ս', 'վ', 'տ', 'ր', 'ց', 'ւ', 'փ', 'ք', 'և', 'օ', 'ֆ', ' ', '  ', '/', '\\', '&', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0');
        $array_english = array('-', 'a', 'b', 'g', 'd', 'e', 'z', 'e', 'y', 't', 'zh', 'i', 'l', 'kh', 'ts', 'k', 'h', 'dz', 'gh', 'ch', 'm', 'y', 'n', 'sh', 'o', 'ch', 'p', 'j', 'r', 's', 'v', 't', 'r', 'c', 'u', 'p', 'q', 'ev', 'o', 'f', '-', '-', '-', '-', '-', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0');
        $new = '';
        if (count($charlist) < 41) {
            $counts = count($charlist);
        } else {
            $counts = 40;
        }
        for ($i = 0; $i < $counts; $i++) {
            $key = array_search(mb_strtolower($charlist[$i], 'UTF-8'), $array_hayeren);
            $new .= $array_english[$key];

        }
        return $new;
    }

    private function create_slug($title, $first_code)
    {

        $slug = $this->create_english($title);
        $article_isset_slug = DB::table("blogs")->where('slug', '=', $slug)->orderBy('id', 'desc')->count();
        if ($article_isset_slug > 0) {
            $new_slug = $slug . "-" . $article_isset_slug;
        } else {
            $new_slug = $slug;
        }

        return $new_slug;
    }

    private function create_slug_id($title, $first_code, $id)
    {

        $slug = $this->create_english($title);
        $article_isset_slug = DB::table("pages")->where('id', '!=', $id)->where('slug', '=', $slug)->orderBy('id', 'desc')->count();
        if ($article_isset_slug > 0) {
            $new_slug = $slug . "-" . $article_isset_slug;
        } else {
            $new_slug = $slug;
        }

        return $new_slug;
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $first_lang = Language::where('first', '=', 1)->where('hidden','=', 0)->first();
        $default_lang = $first_lang->short;
        $blogs = Blog::select("title_$default_lang as title","short_desc_$default_lang as short_desc", "id")->get();

        foreach ($blogs as $blog) {
            $blog->image = BlogImage::where('blog_id', '=', $blog->id)->first();
        }

        return view("cellar.blog.all", compact( 'blogs', 'default_lang'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $first_lang = Language::where('first', '=', 1)->where('hidden','=', 0)->first();
        $default_lang = $first_lang->short;
        $languages = Language::where('hidden', '=', 0)->get();
        return view("cellar.blog.create", compact( 'languages', 'default_lang'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $first_lang = Language::where('first', '=', 1)->where('hidden','=', 0)->first();
        $default_lang = $first_lang->short;
        $request->validate([
            'title_am' => 'required|string',
            'title_ru' => 'required|string',
            'title_en' => 'required|string',
            'filename.*' => 'required|url',
            'filename' => 'required|array',
        ]);

        $blog = new Blog();
        $blog->title_am = $request->title_am;
        $blog->title_ru = $request->title_ru;
        $blog->title_en = $request->title_en;
        $blog->short_desc_am = $request->short_desc_am;
        $blog->short_desc_ru = $request->short_desc_ru;
        $blog->short_desc_en = $request->short_desc_en;
        $blog->desc_am = $request->desc_am;
        $blog->desc_ru = $request->desc_ru;
        $blog->desc_en = $request->desc_en;
        $new_slug = $this->create_slug($request->title_am, $default_lang);
        $blog->slug = $new_slug;
        $blog->save();
        if (isset($request->filename) && $request->filename != '') {
            foreach ($request->filename as $filename) {
                $galleryItem = new BlogImage();
                $galleryItem->image = $filename;
                $galleryItem->blog_id = $blog->id;
                $galleryItem->save();
            }
        }

        return redirect('admin/blog');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $first_lang = Language::where('first', '=', 1)->where('hidden','=', 0)->first();
        $default_lang = $first_lang->short;
        $languages = Language::where('hidden', '=', 0)->get();

        $blog = Blog::find($id);
        $projectImages = BlogImage::where('blog_id', '=', $id)->get();

        $dropzoneDefaultHidden = '';
        if ($projectImages) {
            $dropzoneDefaultHidden = 'dz-default-hidden';
        }

        return view("cellar.blog.edit", compact( 'dropzoneDefaultHidden','projectImages','blog', 'languages', 'default_lang'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $first_lang = Language::where('first', '=', 1)->where('hidden','=', 0)->first();
        $default_lang = $first_lang->short;
        $request->validate([
            'title_am' => 'required|string',
            'title_ru' => 'required|string',
            'title_en' => 'required|string',
            'filename.*' => 'required|url',
            'filename' => 'required|array',
        ]);

        $blog = Blog::find($id);
        $blog->title_am = $request->title_am;
        $blog->title_ru = $request->title_ru;
        $blog->title_en = $request->title_en;
        $blog->short_desc_am = $request->short_desc_am;
        $blog->short_desc_ru = $request->short_desc_ru;
        $blog->short_desc_en = $request->short_desc_en;
        $blog->desc_am = $request->desc_am;
        $blog->desc_ru = $request->desc_ru;
        $blog->desc_en = $request->desc_en;
        $new_slug = $this->create_slug($request->title_am, $default_lang);
        $blog->slug = $new_slug;
        $blog->save();

        DB::table('blog_images')->where('blog_id', '=', $id)->delete();
        if (isset($request->filename) && $request->filename != '') {
            foreach ($request->filename as $filename) {
                $galleryItem = new BlogImage();
                $galleryItem->image = $filename;
                $galleryItem->blog_id = $blog->id;
                $galleryItem->save();
            }
        }

        return redirect('admin/blog');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        DB::table('blog_images')->where('blog_id', '=', $id)->delete();
        DB::table('blogs')->where('id', '=', $id)->delete();
    }

    public function upload(Request $request) {
        $fileNameToStore = "blog_".substr(md5(microtime()),rand(0,26),10);
        Storage::putFileAs('public/blog', $request->file("file"), $fileNameToStore);
        $fileUrl = url('/').'/storage/blog/'.$fileNameToStore;
        echo $fileUrl;
    }
}
