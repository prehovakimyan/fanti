<?php

namespace App\Http\Controllers\Cellar;

use App\Models\PageGallery;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use App\Models\Page;

class PagesController extends Controller
{


    private function mb_str_split($string)
    {
        # Split at all position not after the start: ^
        # and not before the end: $
        return preg_split('/(?<!^)(?!$)/u', $string);
    }

    private function create_english($title)
    {
        $charlist = $this->mb_str_split($title);
        $array_hayeren = array('.', 'ա', 'բ', 'գ', 'դ', 'ե', 'զ', 'է', 'ը', 'թ', 'ժ', 'ի', 'լ', 'խ', 'ծ', 'կ', 'հ', 'ձ', 'ղ', 'ճ', 'մ', 'յ', 'ն', 'շ', 'ո', 'չ', 'պ', 'ջ', 'ռ', 'ս', 'վ', 'տ', 'ր', 'ց', 'ւ', 'փ', 'ք', 'և', 'օ', 'ֆ', ' ', '  ', '/', '\\', '&', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0');
        $array_english = array('-', 'a', 'b', 'g', 'd', 'e', 'z', 'e', 'y', 't', 'zh', 'i', 'l', 'kh', 'ts', 'k', 'h', 'dz', 'gh', 'ch', 'm', 'y', 'n', 'sh', 'o', 'ch', 'p', 'j', 'r', 's', 'v', 't', 'r', 'c', 'u', 'p', 'q', 'ev', 'o', 'f', '-', '-', '-', '-', '-', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0');
        $new = '';
        if (count($charlist) < 41) {
            $counts = count($charlist);
        } else {
            $counts = 40;
        }
        for ($i = 0; $i < $counts; $i++) {
            $key = array_search(mb_strtolower($charlist[$i], 'UTF-8'), $array_hayeren);
            $new .= $array_english[$key];

        }
        return $new;
    }

    private function create_slug($title, $first_code)
    {

        $slug = $this->create_english($title);
        $article_isset_slug = DB::table("pages")->where('slug', '=', $slug)->orderBy('id', 'desc')->count();
        if ($article_isset_slug > 0) {
            $new_slug = $slug . "-" . $article_isset_slug;
        } else {
            $new_slug = $slug;
        }

        return $new_slug;
    }

    private function create_slug_id($title, $first_code, $id)
    {

        $slug = $this->create_english($title);
        $article_isset_slug = DB::table("pages")->where('id', '!=', $id)->where('slug', '=', $slug)->orderBy('id', 'desc')->count();
        if ($article_isset_slug > 0) {
            $new_slug = $slug . "-" . $article_isset_slug;
        } else {
            $new_slug = $slug;
        }

        return $new_slug;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden','=', 0)->first();
        $default_lang = $first_lang->short;

        $pages = Page::select("gallery", "pdf_uploder", "sub_pages", "title_$default_lang as title", "id")->get();

        foreach ($pages as $page) {
            $page->pageGallery = PageGallery::where('page_id', '=', $page->id)->first();
        }

        return view("cellar.pages.all", compact('pages', 'default_lang'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $languages = DB::table('languages')->where('hidden','=', 0)->get();
        return view('cellar.pages.create', compact('languages'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (count($request->all()) == 0) {
            return response()->json([
                'status'  => false,
                'message' => 'Request data is corrupted'
            ], 422);
        }

        if (isset($request->add_page) && $request->add_page == "ok") {

            $validateUser = Validator::make($request->all(),
                [
                    "title_am" => "required|string|max:255",
                    "title_ru" => "required|string|max:255",
                    "title_en" => "required|string|max:255",
                ]);

            if ($validateUser->fails()) {
                return response()->json([
                    'status' => false,
                    'message' => 'validation error',
                    'errors' => $validateUser->errors()
                ], 401);
            }



            $languages = DB::table('languages')->where('hidden','=', 0)->get();

            $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden','=', 0)->first();
            $default_lang = $first_lang->short;
            $first_title = $request->input("title_".$default_lang);

            //Page Configs

            if (isset($request->sub_page) && $request->sub_page == '1') {
                $sub_page = $request->sub_page;
            } else {
                $sub_page = 0;
            }
            if (isset($request->gallery) && $request->gallery == '1') {
                $gallery = $request->gallery;
            } else {
                $gallery = 0;
            }
            if (isset($request->pdf_uploader) && $request->pdf_uploader == '1') {
                $pdf_uploader = $request->pdf_uploader;
            } else {
                $pdf_uploader = 0;
            }

            if (isset($request->parent) && $request->parent != '') {
                $parent_id = $request->parent;
            } else {
                $parent_id = 0;
            }


            $page = new Page();
            $new_slug = $this->create_slug($first_title, $default_lang);
            $page->slug = $new_slug;
            $page->sub_pages = $sub_page;
            $page->gallery = $gallery;
            $page->pdf_uploder = $pdf_uploader;

            foreach($request->all() as $key => $val){
                if($key !='add_page' && $key !='_token' && $key !='image'){
                    $page->$key = $val;
                }
            }

            if ($request->file("image")) {

                $fileNameToStore = "image_".time();

                Storage::putFileAs('public/images', $request->file("image"), $fileNameToStore);

                $fileUrl = url('/').'/storage/images/'.$fileNameToStore;

                $page->image = $fileUrl;

            }

            $page->save();
            if ($parent_id != 0) {
                return redirect('admin/pages/' . $parent_id);
            } else {
                return redirect('admin/pages');
            }


        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if($id == 21){
            return redirect('admin/pages/21/edit');
        }else{

            $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden','=', 0)->first();
            $default_lang = $first_lang->short;
            $page = DB::table("pages")
                ->select(
                    "title_$default_lang as title",
                    "description_$default_lang as description",
                    "short_desc_$default_lang as short_desc",
                    "meta_title_$default_lang as meta_title",
                    "meta_desc_$default_lang as meta_desc",
                    "meta_key_$default_lang as meta_key",
                    "gallery",
                    "sub_pages",
                    "pdf_uploder",
                    "parent",
                    "hidden",
                    "slug",
                    "id",
                    "config_data1",
                    "config_data2",
                    "config_cat",
                    "config_tag",
                    "config_image",
                    "config_project",
                    "page_with_pdf"
                )->where('id', $id)->first();
            $page->child_pages = '';
            if ($page->sub_pages == '1') {
                $child_pages = DB::table("pages")->select(
                    "title_$default_lang as title",
                    "description_$default_lang as description",
                    "short_desc_$default_lang as short_desc",
                    "meta_title_$default_lang as meta_title",
                    "meta_desc_$default_lang as meta_desc",
                    "meta_key_$default_lang as meta_key",
                    "gallery",
                    "sub_pages",
                    "pdf_uploder",
                    "parent",
                    "hidden",
                    "slug",
                    "id",
                    "config_data1",
                    "config_data2",
                    "config_cat",
                    "config_tag",
                    "config_image",
                    "page_with_pdf"
                )
                    ->where('parent', $id)
                    ->orderBy('id', 'desc')->get();
                if (!empty($child_pages)) {
                    foreach ($child_pages as $child_page) {
                        $child_page->child_page_galleries = '';
                        $child_page->child_page_pdfs = '';
                        $child_page->page_gallery_count = 0;
                        if($child_page->pdf_uploder == 1){
                            $child_page_pdfs = DB::table("page_pdfs")->select("pdf_name_$default_lang as title", "id", "hidden")
                                ->where('page_id',$child_page->id)->orderBy('id', 'desc')->get();
                            $child_page->child_page_pdfs = $child_page_pdfs;
                        }
                        if($child_page->gallery == 1){
                            $page_gallery = DB::table("gallery_infos")->select("hidden","id","title_$default_lang as title")->where('parent_id',$child_page->id)->orderBy('id', 'desc')->get();
                            $page_gal_count = DB::table("gallery_infos")->where('parent_id',$child_page->id)->orderBy('id', 'desc')->count();
                            if($page_gal_count > 0){
                                foreach ($page_gallery as $page_gallery_one) {

                                    $page_gallery_count = DB::table('gallery')->where('parent_id',$page_gallery_one->id)->count();
                                    $child_page->child_page_galleries = $page_gallery;
                                }
                                $child_page->page_gallery_count = $page_gallery_count;
                            }

                        }
                        $child_page->child_sub_pages_with_pdf = '';
                        if ($child_page->page_with_pdf == 1){
                            $child_sub_pages_with_pdfs = DB::table("sub_pages_with_pdfs")
                                ->select("title_$default_lang as title","id","hidden")
                                ->where('parent_id',$child_page->id)
                                ->where('folder_parent',0)
                                ->get();
                            if (!empty($child_sub_pages_with_pdfs)){
                                $child_page->child_sub_pages_with_pdf = $child_sub_pages_with_pdfs;
                            }
                        }
                        if(isset($child_sub_pages_with_pdfs) && !empty($child_sub_pages_with_pdfs)){
                            foreach ($child_sub_pages_with_pdfs as $child_sub_pages_with_pdf) {
                                $child_sub_pages_with_pdf->pdf_folders = '';
                                $child_sub_pdfs_folder = DB::table("sub_pages_with_pdfs")
                                    ->select("title_$default_lang as title","description_$default_lang as description","id","hidden")
                                    ->where('folder_parent',$child_sub_pages_with_pdf->id)
                                    ->get();
                                if (!empty($child_sub_pdfs_folder)){
                                    $child_sub_pages_with_pdf->pdf_folders = $child_sub_pdfs_folder;
                                }


                                $child_sub_pages_with_pdf->gallery_folders = '';

                                $page_gallery = DB::table("gallery_infos")->select("hidden","id","title_$default_lang as title")->where('sub_parent_id',$child_sub_pages_with_pdf->id)->orderBy('id', 'desc')->get();
                                $page_gal_count = DB::table("gallery_infos")->where('sub_parent_id',$child_sub_pages_with_pdf->id)->orderBy('id', 'desc')->count();
                                if($page_gal_count > 0){
                                    foreach ($page_gallery as $page_gallery_one) {

                                        $page_gallery_count = DB::table('gallery')->where('sub_parent_id',$page_gallery_one->id)->count();
                                        $child_sub_pages_with_pdf->gallery_folders = $page_gallery;
                                    }
                                    $child_sub_pages_with_pdf->page_gallery_count = $page_gallery_count;
                                }

                            }
                        }


                    }
                    $page->child_pages = $child_pages;
                }
            }

            $page->sub_pages_with_pdf = '';
            if ($page->page_with_pdf == '1'){
                $sub_pages_with_pdfs = DB::table("sub_pages_with_pdfs")->select("title_$default_lang as title","id","hidden")->where('parent_id',$id)->get();
                if (!empty($sub_pages_with_pdfs)){
                    $page->sub_pages_with_pdf = $sub_pages_with_pdfs;
                }
            }

            if($page->pdf_uploder == 1){
                $parebt_page_pdfs = DB::table("page_pdfs")->select("pdf_name_$default_lang as title", "id")
                    ->where('page_id',$id)
                    ->orderBy('id', 'desc')->get();
                $page->parent_page_pdfs = $parebt_page_pdfs;
            }
            $page->sub_pages_with_projects = '';
            if($page->config_project == 1){
                $sub_pages_with_projects = DB::table("sub_pages_with_pdfs")->select("title_$default_lang as title","id","hidden")->where('parent_id',$id)->get();
                if (!empty($sub_pages_with_projects)){
                    $page->sub_pages_with_projects = $sub_pages_with_projects;
                }
            }



            $page->newses = '';
            $page->news_pdf = '';
            $page->news_sub_page = '';
            if($page->config_cat == 1){

                $page_news = DB::table("news")->select("cat_id","page_pdf","gallery_config","pdf_config","title_$default_lang as title","id","created_at","image","hidden")
                    ->where("parent_id", $id)->orderby('id', "DESC")
                    ->orderBy('id', 'desc')->get();


                if(!empty($page_news)){
                    foreach ($page_news as $page_new) {
                        $page_new->count_news_sub_page_pdf = 0;

                        $page_new->news_gallery_count = 0;
                        $page_news_pdfs = DB::table("news_pdfs")->select("pdf_name_$default_lang as title", "id","hidden")
                            ->where('news_id',$page_new->id)
                            ->orderBy('id', 'desc')->get();
                        if(!empty($page_news_pdfs)){
                            $page_new->pdf = $page_news_pdfs;
                        }


                        $news_galleries = DB::table("news_gallery_infos")->where('parent_id',$page_new->id)
                            ->orderBy('id', 'desc')->get();
                        if(!empty($news_galleries)){
                            $page_new->galleries =  $news_galleries;
                            foreach ($news_galleries as $news_gallery){
                                $news_gallery_count = DB::table('news_gallery')->where('parent_id',$news_gallery->id)->count();
                                $news_gallery->count_news_sub_page_pdf = $news_gallery_count;
                            }
                        }

                        $page_news_sub_page = DB::table("news_sub_page")->select("title_$default_lang as title","hidden", "id") ->where('news_id',$page_new->id)
                            ->orderBy('id', 'desc')->get();
                        //return $page_new->id;
                        foreach ($page_news_sub_page as $page_news_sub_page_one){
                            $count_news_sub_page_pdf = DB::table("news_sub_page_pdf")->where('sub_page_id',$page_news_sub_page_one->id)
                                ->count();
                            $page_news_sub_page_one->count_news_sub_page_pdf = $count_news_sub_page_pdf;
                        }

                        if(!empty($page_news_sub_page)){
                            $page_new->news_sub_page = $page_news_sub_page;
                        }
                        //return $page_new->count_news_sub_page_pdf;
                    }

                    $page->newses = $page_news;

                }

            }

            $page_gallery = DB::table("gallery_infos")->select("id")->where('parent_id',$id)->first();

            return view('admin.pages.show', compact('page_gallery','default_lang','page'));
        }


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden','=', 0)->first();
        $default_lang = $first_lang->short;

        $languages = DB::table('languages')->where('hidden','=', 0)->get();
        $page = Page::find($id);


        return view('cellar.pages.edit', compact('default_lang','languages','page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (isset($request->edit_page) && $request->edit_page == "ok") {

            $validateUser = Validator::make($request->all(),
                [
                    "title_am" => "required|string|max:255",
                    "title_ru" => "required|string|max:255",
                    "title_en" => "required|string|max:255",
                ]);

            if ($validateUser->fails()) {
                return response()->json([
                    'status' => false,
                    'message' => 'validation error',
                    'errors' => $validateUser->errors()
                ], 401);
            }



            $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden','=', 0)->first();
            $default_lang = $first_lang->short;
            $first_title = $request->input("title_".$default_lang);

            $new_slug = $this->create_slug_id($first_title, $default_lang, $id);


            if ($request->file("image")) {

                $image = Input::file('image');
                $generatedString = time();
                $extension = $request->file("image")->getClientOriginalExtension();
                $newFile = public_path('uploads/page/' . $generatedString . "." . $extension);
                Image::make($image->getRealPath())->orientate()->save($newFile);
                $newFileThumb = public_path('uploads/page/thumbs/' . $generatedString . "." . $extension);
                $filepath = $generatedString . "." . $extension;
                Image::make($image->getRealPath())->resize(520, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->orientate()->save($newFileThumb);

            } else {
                $filepath = $request->old_image;
            }
            if (isset($request->sub_page) && $request->sub_page == '1') {
                $sub_page = $request->sub_page;
            } else {
                $sub_page = 0;
            }
            if (isset($request->gallery) && $request->gallery == '1') {
                $gallery = $request->gallery;
            } else {
                $gallery = 0;
            }
            if (isset($request->pdf_uploader) && $request->pdf_uploader == '1') {
                $pdf_uploader = $request->pdf_uploader;
            } else {
                $pdf_uploader = 0;
            }

            if (isset($request->parent) && $request->parent != '') {
                $parent_id = $request->parent;
            } else {
                $parent_id = 0;
            }

            $page = Page::find($id);


            $page->slug = $new_slug;
            $page->sub_pages = $sub_page;
            $page->gallery = $gallery;
            $page->pdf_uploder = $pdf_uploader;

            foreach($request->all() as $key => $val){

                if($key !='_token' && $key !='edit_page' && $key!='_method' && $key!='menu_item_hidden'&& $key!='old_image'){
                    $page->$key = $val;
                }

            }
            $page->image = $filepath;

            $page->save();
        }


        return redirect('/admin/pages/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('pages')->where('id', '=', $id)->delete();
    }

}
