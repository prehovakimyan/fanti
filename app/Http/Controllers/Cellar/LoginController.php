<?php

namespace App\Http\Controllers\Cellar;

use App\Http\Controllers\Controller;
use App\Models\Cellar\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LoginController extends Controller
{
    protected $redirectTo = '/admin/dashboard';
    protected $logoutRedirectTo = '/admin';

    public function index(Request $request)
    {
        if (session('admin')) {
            return redirect($this->redirectTo);
        } else {
            return view('cellar.auth');
        }
    }

    public function signIn(Request $request)
    {
        $this->validateLogin($request);

        $username = $request->username;
        $password = $request->password;


        $login_admin = Admin::where('username', '=', $username)
            ->where('password', '=', md5($password))
            ->first();

        if ($login_admin) {
            if ($login_admin->role == 2) {
                session(['admin' => 'yes']);
                session(['super_admin' => 'yes']);
            } else {
                session(['admin' => 'yes']);
            }
            return redirect($this->redirectTo);
        } else {
            $request->session()->flash('error_login', 'Invalid user login or password');
            return view('cellar.auth');
        }


    }


    protected function validateLogin(Request $request)
    {
        $this->validate($request, [
            $this->username() => 'required|string',
            'password' => 'required|string',
        ]);
    }

    public function username()
    {
        return 'username';
    }

    public function logout(Request $request)
    {
        $request->session()->forget('admin');
        $request->session()->forget('super_admin');
        return redirect($this->logoutRedirectTo);
    }
}
