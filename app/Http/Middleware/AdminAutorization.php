<?php

namespace App\Http\Middleware;

use Closure;

class AdminAutorization
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(session('admin')){

            return $next($request);
        }else{
            return redirect('admin');
        }
    }
}
